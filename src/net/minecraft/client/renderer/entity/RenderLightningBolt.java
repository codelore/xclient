/*     */ package net.minecraft.client.renderer.entity;
/*     */ 
/*     */ import com.mumfrey.liteloader.core.LiteLoader;
/*     */ import java.util.Random;
/*     */ import net.minecraft.client.renderer.Tessellator;
/*     */ import net.minecraft.entity.Entity;
/*     */ import net.minecraft.entity.effect.EntityLightningBolt;
/*     */ import org.lwjgl.opengl.GL11;
/*     */ 
/*     */ public class RenderLightningBolt extends Render
/*     */ {
/*     */   public void doRenderLightningBolt(EntityLightningBolt par1EntityLightningBolt, double par2, double par4, double par6, float par8, float par9)
/*     */   {
/*  22 */     Tessellator var10 = Tessellator.instance;
/*  23 */     GL11.glDisable(3553);
/*  24 */     GL11.glDisable(2896);
/*  25 */     GL11.glEnable(3042);
/*  26 */     GL11.glBlendFunc(770, 1);
/*  27 */     double[] var11 = new double[8];
/*  28 */     double[] var12 = new double[8];
/*  29 */     double var13 = 0.0D;
/*  30 */     double var15 = 0.0D;
/*  31 */     Random var17 = new Random(par1EntityLightningBolt.boltVertex);
/*     */ 
/*  33 */     for (int var18 = 7; var18 >= 0; var18--)
/*     */     {
/*  35 */       var11[var18] = var13;
/*  36 */       var12[var18] = var15;
/*  37 */       var13 += var17.nextInt(11) - 5;
/*  38 */       var15 += var17.nextInt(11) - 5;
/*     */     }
/*     */ 
/*  41 */     for (int var45 = 0; var45 < 4; var45++)
/*     */     {
/*  43 */       Random var46 = new Random(par1EntityLightningBolt.boltVertex);
/*     */ 
/*  45 */       for (int var19 = 0; var19 < 3; var19++)
/*     */       {
/*  47 */         int var20 = 7;
/*  48 */         int var21 = 0;
/*     */ 
/*  50 */         if (var19 > 0)
/*     */         {
/*  52 */           var20 = 7 - var19;
/*     */         }
/*     */ 
/*  55 */         if (var19 > 0)
/*     */         {
/*  57 */           var21 = var20 - 2;
/*     */         }
/*     */ 
/*  60 */         double var22 = var11[var20] - var13;
/*  61 */         double var24 = var12[var20] - var15;
/*     */ 
/*  63 */         for (int var26 = var20; var26 >= var21; var26--)
/*     */         {
/*  65 */           double var27 = var22;
/*  66 */           double var29 = var24;
/*     */ 
/*  68 */           if (var19 == 0)
/*     */           {
/*  70 */             var22 += var46.nextInt(11) - 5;
/*  71 */             var24 += var46.nextInt(11) - 5;
/*     */           }
/*     */           else
/*     */           {
/*  75 */             var22 += var46.nextInt(31) - 15;
/*  76 */             var24 += var46.nextInt(31) - 15;
/*     */           }
/*     */ 
/*  79 */           var10.startDrawing(5);
/*  80 */           float var31 = 0.5F;
/*  81 */           var10.setColorRGBA_F(0.9F * var31, 0.9F * var31, 1.0F * var31, 0.3F);
/*  82 */           double var32 = 0.1D + var45 * 0.2D;
/*     */ 
/*  84 */           if (var19 == 0)
/*     */           {
/*  86 */             var32 *= (var26 * 0.1D + 1.0D);
/*     */           }
/*     */ 
/*  89 */           double var34 = 0.1D + var45 * 0.2D;
/*     */ 
/*  91 */           if (var19 == 0)
/*     */           {
/*  93 */             var34 *= ((var26 - 1) * 0.1D + 1.0D);
/*     */           }
/*     */ 
/*  96 */           for (int var36 = 0; var36 < 5; var36++)
/*     */           {
/*  98 */             double var37 = par2 + 0.5D - var32;
/*  99 */             double var39 = par6 + 0.5D - var32;
/*     */ 
/* 101 */             if ((var36 == 1) || (var36 == 2))
/*     */             {
/* 103 */               var37 += var32 * 2.0D;
/*     */             }
/*     */ 
/* 106 */             if ((var36 == 2) || (var36 == 3))
/*     */             {
/* 108 */               var39 += var32 * 2.0D;
/*     */             }
/*     */ 
/* 111 */             double var41 = par2 + 0.5D - var34;
/* 112 */             double var43 = par6 + 0.5D - var34;
/*     */ 
/* 114 */             if ((var36 == 1) || (var36 == 2))
/*     */             {
/* 116 */               var41 += var34 * 2.0D;
/*     */             }
/*     */ 
/* 119 */             if ((var36 == 2) || (var36 == 3))
/*     */             {
/* 121 */               var43 += var34 * 2.0D;
/*     */             }
/*     */ 
/* 124 */             var10.addVertex(var41 + var22, par4 + var26 * 16, var43 + var24);
/* 125 */             var10.addVertex(var37 + var27, par4 + (var26 + 1) * 16, var39 + var29);
/*     */           }
/*     */ 
/* 128 */           var10.draw();
/*     */         }
/*     */       }
/*     */     }
/*     */ 
/* 133 */     GL11.glDisable(3042);
/* 134 */     GL11.glEnable(2896);
/* 135 */     GL11.glEnable(3553);
/*     */   }
/*     */ 
/*     */   public void doRender(Entity par1Entity, double par2, double par4, double par6, float par8, float par9)
/*     */   {
/* 147 */     doRenderLightningBolt((EntityLightningBolt)par1Entity, par2, par4, par6, par8, par9);
/*     */   }
/*     */ 
/*     */   static
/*     */   {
/*  13 */     LiteLoader.getInstance();
/*     */   }
/*     */ }

/* Location:           /home/arthur/dev/jirai/xClient-mcp/mcp/build/liteloader_1.5.2_deobf.zip
 * Qualified Name:     net.minecraft.client.renderer.entity.RenderLightningBolt
 * JD-Core Version:    0.6.0
 */