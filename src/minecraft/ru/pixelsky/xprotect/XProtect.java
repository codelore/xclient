package ru.pixelsky.xprotect;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.network.*;
import net.minecraft.client.Minecraft;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.NetLoginHandler;
import net.minecraft.network.packet.NetHandler;
import net.minecraft.network.packet.Packet1Login;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumOS;
import net.minecraftforge.common.MinecraftForge;

import java.io.File;
import java.net.URL;
import java.net.URLDecoder;

/**
 * WARNING! YOU _MUST_ OBFUSCATE CLASSES IN THIS PACKAGE
 */
@Mod(name = "pixelsky-base", modid = "pixelsky-base", version = "1.0")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class XProtect {
    private Thread scanThread;
    private volatile boolean scanning;
    private Filelist localFilelist;
    
    public native static String getHwid();
    public native static String[] checkedDirs();
    
    @Mod.Init
    public void init(FMLInitializationEvent event)
    {
        computeHashes();
        MinecraftForge.EVENT_BUS.register(this);
        NetworkRegistry.instance().registerConnectionHandler(loginHandler);
    }

    public void sendFilelist(Filelist fl)
    {
        PacketDispatcher.sendPacketToServer(
                new Packet250CustomPayload("xProtect", fl.toBytes())
        );
    }

    public void sendHwid(String s)
    {
        s = s.replaceAll("\\{", "").replaceAll("\\}", "");
        PacketDispatcher.sendPacketToServer(
                new Packet250CustomPayload("xProtect_hwid", s.getBytes())
        );
    }
    public void sendHashes()
    {
        if (localFilelist == null && scanning) {
            while (scanning) {
                // Ждем, пока вычислятся хэши
                try {
                    scanThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        sendFilelist(localFilelist);
    }

    public void computeHashes() {
        Runnable scanRunnable = new Runnable() {
            @Override
            public void run() {
                // Minecraft запущен из .jar
                File jarFile = getMinecraftJarFile();

                /* Not exit if running in IDE */
                if (!jarFile.getName().endsWith(".class") && (!jarFile.getParentFile().getName().equals("bin") || !jarFile.getParentFile().getParentFile().equals(Minecraft.getMinecraftDir())))
                    System.exit(0);

                scanning = true;
                File minecraftRoot = Minecraft.getMinecraftDir();
                String[] checkedDirs = {"bin","mods","coremods"};//checkedDirs();
                localFilelist = Filelist.fromFilesystem(minecraftRoot, checkedDirs);
                scanning = false;
            }
        };
        scanThread = new Thread(scanRunnable);
        scanThread.start();
    }
    public void computeHwid()
    {
        
    }

    private File getMinecraftJarFile() {
        URL url = Minecraft.getMinecraft().getClass().getProtectionDomain().getCodeSource().getLocation();
        String path = URLDecoder.decode(url.toString());

        /* РџРѕСЂСЏРґРѕРє РЅРµРёР·РІРµСЃС‚РµРЅ, РїРѕСЌС‚РѕРјСѓ СѓРґР°Р»СЏРµРј РґРІР° СЂР°Р·Р° */
        path = removePrefix(path, "jar:");
        path = removePrefix(path, "file:");
        path = removePrefix(path, "jar:");

        /* РњРѕР¶РµС‚ РѕСЃС‚Р°С‚СЊСЃСЏ Р»РёС€РЅРёР№ / РѕС‚ РїСЂРѕС‚РѕРєРѕР»Р°, СѓР±РёСЂР°РµРј РµРіРѕ */
        while (path.startsWith("//"))
            path = path.substring(1);

        /* РЈР±РёСЂР°РµРј / РІ РЅР°С‡Р°Р»Рµ РїСѓС‚Рё РІ Windows (/ РѕСЃС‚Р°Р»СЃСЏ РѕС‚ РїСЂРѕС‚РѕРєРѕР»Р°) */
        if (Minecraft.getOs() == EnumOS.WINDOWS)
            if (path.startsWith("/"))
                path = path.substring(1);

        /* '!' СЂР°Р·РґРµР»СЏРµС‚ РїСѓС‚СЊ Рє jar Рё РЅР°Р·РІР°РЅРёРµ РєР»Р°СЃСЃР° */
        int pathAndClassNameDelimIndex = path.indexOf('!');
        if (pathAndClassNameDelimIndex >= 0 ) {
            path = path.substring(0, pathAndClassNameDelimIndex);
        }

        return new File(path);
    }

    private String removePrefix(String string, String prefix) {
        if (string.startsWith(prefix))
            return string.substring(prefix.length());
        else
            return string;
    }

    private IConnectionHandler loginHandler = new IConnectionHandler() {
        @Override
        public void playerLoggedIn(Player player, NetHandler netHandler, INetworkManager manager) {

        }
        @Override
        public String connectionReceived(NetLoginHandler netHandler, INetworkManager manager) {
            // Server side

            return null;
        }
        @Override
        public void connectionOpened(NetHandler netClientHandler, String server, int port, INetworkManager manager) {

        }
        @Override
        public void connectionOpened(NetHandler netClientHandler, MinecraftServer server, INetworkManager manager) {

        }
        @Override
        public void connectionClosed(INetworkManager manager) {

        }
        @Override

        public void clientLoggedIn(NetHandler clientHandler, INetworkManager manager, Packet1Login login) {
            sendHashes();
           // sendHwid(getHwid());
        }
    };
}
