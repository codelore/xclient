package net.minecraft.src;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.client.gui.GuiSmallButton;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.settings.EnumOptions;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.StringTranslate;

public class GuiAnimationSettingsOF extends GuiScreen {

   private GuiScreen prevScreen;
   protected String title = "Анимация";
   private GameSettings settings;
   private static EnumOptions[] enumOptions = new EnumOptions[]{EnumOptions.ANIMATED_WATER, EnumOptions.ANIMATED_LAVA, EnumOptions.ANIMATED_FIRE, EnumOptions.ANIMATED_PORTAL, EnumOptions.ANIMATED_REDSTONE, EnumOptions.ANIMATED_EXPLOSION, EnumOptions.ANIMATED_FLAME, EnumOptions.ANIMATED_SMOKE, EnumOptions.VOID_PARTICLES, EnumOptions.WATER_PARTICLES, EnumOptions.RAIN_SPLASH, EnumOptions.PORTAL_PARTICLES, EnumOptions.POTION_PARTICLES, EnumOptions.DRIPPING_WATER_LAVA, EnumOptions.ANIMATED_TERRAIN, EnumOptions.ANIMATED_ITEMS, EnumOptions.ANIMATED_TEXTURES, EnumOptions.PARTICLES};


   public GuiAnimationSettingsOF(GuiScreen guiscreen, GameSettings gamesettings) {
      this.prevScreen = guiscreen;
      this.settings = gamesettings;
   }

   public void initGui() {
      StringTranslate stringtranslate = StringTranslate.getInstance();
      int i = 0;
      EnumOptions[] aenumoptions = enumOptions;
      int j = aenumoptions.length;

      for(int k = 0; k < j; ++k) {
         EnumOptions enumoptions = aenumoptions[k];
         int x = super.width / 2 - 155 + i % 2 * 160;
         int y = super.height / 6 + 21 * (i / 2) - 10;
         if(!enumoptions.getEnumFloat()) {
            super.buttonList.add(new GuiSmallButton(enumoptions.returnEnumOrdinal(), x, y, enumoptions, this.settings.getKeyBinding(enumoptions)));
         } else {
            super.buttonList.add(new GuiSlider(enumoptions.returnEnumOrdinal(), x, y, enumoptions, this.settings.getKeyBinding(enumoptions), this.settings.getOptionFloatValue(enumoptions)));
         }

         ++i;
      }

      super.buttonList.add(new GuiButton(210, super.width / 2 - 155, super.height / 6 + 168 + 11, 70, 20, "Вкл. всё"));
      super.buttonList.add(new GuiButton(211, super.width / 2 - 155 + 80, super.height / 6 + 168 + 11, 70, 20, "Выкл. всё"));
      super.buttonList.add(new GuiSmallButton(200, super.width / 2 + 5, super.height / 6 + 168 + 11, stringtranslate.translateKey("gui.done")));
   }

   protected void actionPerformed(GuiButton guibutton) {
      if(guibutton.enabled) {
         if(guibutton.id < 100 && guibutton instanceof GuiSmallButton) {
            this.settings.setOptionValue(((GuiSmallButton)guibutton).returnEnumOptions(), 1);
            guibutton.displayString = this.settings.getKeyBinding(EnumOptions.getEnumOptions(guibutton.id));
         }

         if(guibutton.id == 200) {
            super.mc.gameSettings.saveOptions();
            super.mc.displayGuiScreen(this.prevScreen);
         }

         if(guibutton.id == 210) {
            super.mc.gameSettings.setAllAnimations(true);
         }

         if(guibutton.id == 211) {
            super.mc.gameSettings.setAllAnimations(false);
         }

         if(guibutton.id != EnumOptions.CLOUD_HEIGHT.ordinal()) {
            ScaledResolution scaledresolution = new ScaledResolution(super.mc.gameSettings, super.mc.displayWidth, super.mc.displayHeight);
            int i = scaledresolution.getScaledWidth();
            int j = scaledresolution.getScaledHeight();
            this.setWorldAndResolution(super.mc, i, j);
         }

      }
   }

   public void drawScreen(int i, int j, float f) {
      this.drawDefaultBackground();
      this.drawCenteredString(super.fontRenderer, this.title, super.width / 2, 20, 16777215);
      super.drawScreen(i, j, f);
   }

}
