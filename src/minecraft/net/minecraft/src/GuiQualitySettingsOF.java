package net.minecraft.src;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.client.gui.GuiSmallButton;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.settings.EnumOptions;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.StringTranslate;

public class GuiQualitySettingsOF extends GuiScreen {

   private GuiScreen prevScreen;
   protected String title = "Качество";
   private GameSettings settings;
   private static EnumOptions[] enumOptions = new EnumOptions[]{EnumOptions.MIPMAP_LEVEL, EnumOptions.MIPMAP_TYPE, EnumOptions.AF_LEVEL, EnumOptions.AA_LEVEL, EnumOptions.CLEAR_WATER, EnumOptions.RANDOM_MOBS, EnumOptions.BETTER_GRASS, EnumOptions.BETTER_SNOW, EnumOptions.CUSTOM_FONTS, EnumOptions.CUSTOM_COLORS, EnumOptions.SWAMP_COLORS, EnumOptions.SMOOTH_BIOMES, EnumOptions.CONNECTED_TEXTURES, EnumOptions.NATURAL_TEXTURES, EnumOptions.CUSTOM_SKY};
   private int lastMouseX = 0;
   private int lastMouseY = 0;
   private long mouseStillTime = 0L;


   public GuiQualitySettingsOF(GuiScreen guiscreen, GameSettings gamesettings) {
      this.prevScreen = guiscreen;
      this.settings = gamesettings;
   }

   public void initGui() {
      StringTranslate stringtranslate = StringTranslate.getInstance();
      int i = 0;
      EnumOptions[] aenumoptions = enumOptions;
      int j = aenumoptions.length;

      for(int k = 0; k < j; ++k) {
         EnumOptions enumoptions = aenumoptions[k];
         int x = super.width / 2 - 155 + i % 2 * 160;
         int y = super.height / 6 + 21 * (i / 2) - 10;
         if(!enumoptions.getEnumFloat()) {
            super.buttonList.add(new GuiSmallButton(enumoptions.returnEnumOrdinal(), x, y, enumoptions, this.settings.getKeyBinding(enumoptions)));
         } else {
            super.buttonList.add(new GuiSlider(enumoptions.returnEnumOrdinal(), x, y, enumoptions, this.settings.getKeyBinding(enumoptions), this.settings.getOptionFloatValue(enumoptions)));
         }

         ++i;
      }

      super.buttonList.add(new GuiButton(200, super.width / 2 - 100, super.height / 6 + 168 + 11, stringtranslate.translateKey("gui.done")));
   }

   protected void actionPerformed(GuiButton guibutton) {
      if(guibutton.enabled) {
         if(guibutton.id < 100 && guibutton instanceof GuiSmallButton) {
            this.settings.setOptionValue(((GuiSmallButton)guibutton).returnEnumOptions(), 1);
            guibutton.displayString = this.settings.getKeyBinding(EnumOptions.getEnumOptions(guibutton.id));
         }

         if(guibutton.id == 200) {
            super.mc.gameSettings.saveOptions();
            super.mc.displayGuiScreen(this.prevScreen);
         }

         if(guibutton.id != EnumOptions.CLOUD_HEIGHT.ordinal()) {
            ScaledResolution scaledresolution = new ScaledResolution(super.mc.gameSettings, super.mc.displayWidth, super.mc.displayHeight);
            int i = scaledresolution.getScaledWidth();
            int j = scaledresolution.getScaledHeight();
            this.setWorldAndResolution(super.mc, i, j);
         }

      }
   }

   public void drawScreen(int x, int y, float f) {
      this.drawDefaultBackground();
      this.drawCenteredString(super.fontRenderer, this.title, super.width / 2, 20, 16777215);
      super.drawScreen(x, y, f);
      if(Math.abs(x - this.lastMouseX) <= 5 && Math.abs(y - this.lastMouseY) <= 5) {
         short activateDelay = 700;
         if(System.currentTimeMillis() >= this.mouseStillTime + (long)activateDelay) {
            int x1 = super.width / 2 - 150;
            int y1 = super.height / 6 - 5;
            if(y <= y1 + 98) {
               y1 += 105;
            }

            int x2 = x1 + 150 + 150;
            int y2 = y1 + 84 + 10;
            GuiButton btn = this.getSelectedButton(x, y);
            if(btn != null) {
               String s = this.getButtonName(btn.displayString);
               String[] lines = this.getTooltipLines(s);
               if(lines == null) {
                  return;
               }

               this.drawGradientRect(x1, y1, x2, y2, -536870912, -536870912);

               for(int i = 0; i < lines.length; ++i) {
                  String line = lines[i];
                  int col = 14540253;
                  if(line.endsWith("!")) {
                     col = 16719904;
                  }

                  super.fontRenderer.drawStringWithShadow(line, x1 + 5, y1 + 5 + i * 11, col);
               }
            }

         }
      } else {
         this.lastMouseX = x;
         this.lastMouseY = y;
         this.mouseStillTime = System.currentTimeMillis();
      }
   }

   private String[] getTooltipLines(String btnName) {
	      return btnName.equals("Mipmap Level")?new String[]{"Визуальные эффекты делающие удаленные объекты лучше", "путем сглаживания деталей текстуры", "  Откл. - нет сглаживания", "  1 - минимальное сглаживание", "  4 - максимальное сглаживание", "Эта опция обычно не влияет на производительность."}:(btnName.equals("Mipmap Type")?new String[]{"Визуальные эффекты делающие удаленные объекты лучше", "путем сглаживания деталей текстуры", "  Ближайший - грубое сглаживаение", "  Линейный - прекрасное сглаживание", "Эта опция обычно не влияет на производительность."}:(btnName.equals("Anisotropic Filtering")?new String[]{"Анизатропная фильтрация", " Откл. - (по умолчанию) стандартная детализация текстуры (быстро)", " 2-16 - мелкие детали в mip-текстуре (медленно)", "Анизатропная фильтрация восстанавливает детали в mip-текстуре", "Текстуры. Высокое значение параметра может уменьшить FPS."}:(btnName.equals("Antialiasing")?new String[]{"Сглаживание", " Откл. - (по умолчанию) без сглаживания (быстро)", " 2-16 - сглаживание линий и краев (медленно)", "Эта опция сглаживает неровные линии и", "резкие переходы цвета.", "Высокое значение параметра может существенно снизить FPS.", "Не все уровни могут поддерживаться различными видеокартами.", "Начинает работать после перезагрузки!"}:(btnName.equals("Clear Water")?new String[]{"Прозрачная вода", "  Вкл. - чистая, прозрачная вода", "  Выкл. - стандартная вода"}:(btnName.equals("Better Grass")?new String[]{"Улучшенная трава", "  Выкл. - стандартная текстура травы, быстро", "  Быстро - полноразмерная текстура травы, медленно", "  Красиво - динамическая текстура трамы, медленно"}:(btnName.equals("Better Snow")?new String[]{"Улучшенный снег", "  Откл. - стандартный снег, быстро", "  Вкл. - улучшенный снег, медленно.", "показывает снег под прозрачными блоками (забор, высокая трава)", "которые граничат со снежными блоками"}:(btnName.equals("Random Mobs")?new String[]{"Случайные монстры", "  Откл. - нет случайных монстров, быстро", "  Вкл. - случайные монстры, медленно", "Случайные монстры используют случайную текстуру из игровой библиотеки.", "Необходимы специальные текстурпаки с несколькими вариациями текстур монстров."}:(btnName.equals("Swamp Colors")?new String[]{"Болотный цвет", "  Вкл. - используются болотные цвета (по умолчанию), медленно", "  Выкл. - не используются болотные цвета, быстро", "Болотные цвета влияют на траву, листья, лозу и воду."}:(btnName.equals("Smooth Biomes")?new String[]{"Гладкие биомы", "  Вкл. - сглаживание границ биомов (по умолчанию), медленно", "  Выкл. - нет сглаживания границ биомов, быстро", "Сглаживание границ биомов производится путем отбора", "и усреднения цвета всех откружающих блоков.", "Затрагивается трава, листья, лоза и вода."}:(btnName.equals("Custom Fonts")?new String[]{"Пользовательские шрифты", "  Вкл. - используются пользовательские шрифты (по умолчанию), быстро", "  Выкл. - используются стандартные шрифты, быстро", "Пользовательские шрифты используемые в текущем", "текстур паке"}:(btnName.equals("Custom Colors")?new String[]{"Пользовательские цвета", "  Вкл. - используются пользовательские цвета  (по умолчанию), медленно", "  Выкл. - используются стандартные цвета, быстро", "Пользовательские цвета используемые в текущем", "текстур паке"}:(btnName.equals("Show Capes")?new String[]{"Показать мыс", "  Вкл. - показывает мыс игрока (по умолчанию)", "  Выкл. - скрывает мыс игрока."}:(btnName.equals("Connected Textures")?new String[]{"Связанная текстура", "  Выкл. - нет связанных текстур (по умолчанию)", "  Быстро - быстрые связанные текстуры", "  Красиво - красивые связанные текстуры", "Связанные текстуры присоединяют текстуры стекла,", "песчаника и книжных полок в месте досягаемости", "друг друга. Связанные текстуры берутся", "из установленного текстур пака."}:(btnName.equals("Far View")?new String[]{"Дистанция взгляда", " Выкл. - (по умолчанию) стандартная дистанция взгляда", " Вкл. - 3x дистанция взгляда", "Этот параметр очень требователен к ресурсам!", "3x дистанция взгляда => 9x загруженных чанков => FPS / 9", "Стандартная дистанция взгляда: 32, 64, 128, 256", "Дистанция взгляда: 96, 192, 384, 512"}:(btnName.equals("Natural Textures")?new String[]{"Природные текстуры", "  Выкл. - без природных текстур (по умолчанию)", "  Вкл. - используются природные текстуры", "Природные текстуры удаляют сетчатый узор", "создающийся из повторящихся блоков одного и того же типа.", "Параметр вращает и переворачивает", "текстуру блока. Конфигурация для природных", "текстур берется из установленного текстур пака"}:(btnName.equals("Custom Sky")?new String[]{"Пользовательское небо", "  Вкл. - пользовательская текстура неба (по умолчанию), медленно", "  Выкл. - стандартное небо, быстро", "Пользовательская текстура неба берется из текущего", "текстур пака"}:null))))))))))))))));   }

   private String getButtonName(String displayString) {
      int pos = displayString.indexOf(58);
      return pos < 0?displayString:displayString.substring(0, pos);
   }

   private GuiButton getSelectedButton(int i, int j) {
      for(int k = 0; k < super.buttonList.size(); ++k) {
         GuiButton btn = (GuiButton)super.buttonList.get(k);
         boolean flag = i >= btn.xPosition && j >= btn.yPosition && i < btn.xPosition + btn.width && j < btn.yPosition + btn.height;
         if(flag) {
            return btn;
         }
      }

      return null;
   }

}
