package net.minecraft.src;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.client.gui.GuiSmallButton;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.settings.EnumOptions;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.StringTranslate;

public class GuiDetailSettingsOF extends GuiScreen {

    private GuiScreen prevScreen;
    protected String title = "Детализация";
    private GameSettings settings;
    private static EnumOptions[] enumOptions = new EnumOptions[]{EnumOptions.CLOUDS, EnumOptions.CLOUD_HEIGHT, EnumOptions.TREES, EnumOptions.GRASS, EnumOptions.WATER, EnumOptions.RAIN, EnumOptions.SKY, EnumOptions.STARS, EnumOptions.SUN_MOON, EnumOptions.SHOW_CAPES, EnumOptions.DEPTH_FOG, EnumOptions.HELD_ITEM_TOOLTIPS, EnumOptions.DROPPED_ITEMS};
    private int lastMouseX = 0;
    private int lastMouseY = 0;
    private long mouseStillTime = 0L;

    public GuiDetailSettingsOF(GuiScreen guiscreen, GameSettings gamesettings) {
        this.prevScreen = guiscreen;
        this.settings = gamesettings;
    }

    public void initGui() {
        StringTranslate stringtranslate = StringTranslate.getInstance();
        int i = 0;
        EnumOptions[] aenumoptions = enumOptions;
        int j = aenumoptions.length;

        for (int k = 0; k < j; ++k) {
            EnumOptions enumoptions = aenumoptions[k];
            int x = super.width / 2 - 155 + i % 2 * 160;
            int y = super.height / 6 + 21 * (i / 2) - 10;
            if (!enumoptions.getEnumFloat()) {
                super.buttonList.add(new GuiSmallButton(enumoptions.returnEnumOrdinal(), x, y, enumoptions, this.settings.getKeyBinding(enumoptions)));
            } else {
                super.buttonList.add(new GuiSlider(enumoptions.returnEnumOrdinal(), x, y, enumoptions, this.settings.getKeyBinding(enumoptions), this.settings.getOptionFloatValue(enumoptions)));
            }

            ++i;
        }

        super.buttonList.add(new GuiButton(200, super.width / 2 - 100, super.height / 6 + 168 + 11, stringtranslate.translateKey("gui.done")));
    }

    protected void actionPerformed(GuiButton guibutton) {
        if (guibutton.enabled) {
            if (guibutton.id < 100 && guibutton instanceof GuiSmallButton) {
                this.settings.setOptionValue(((GuiSmallButton) guibutton).returnEnumOptions(), 1);
                guibutton.displayString = this.settings.getKeyBinding(EnumOptions.getEnumOptions(guibutton.id));
            }

            if (guibutton.id == 200) {
                super.mc.gameSettings.saveOptions();
                super.mc.displayGuiScreen(this.prevScreen);
            }

            if (guibutton.id != EnumOptions.CLOUD_HEIGHT.ordinal()) {
                ScaledResolution scaledresolution = new ScaledResolution(super.mc.gameSettings, super.mc.displayWidth, super.mc.displayHeight);
                int i = scaledresolution.getScaledWidth();
                int j = scaledresolution.getScaledHeight();
                this.setWorldAndResolution(super.mc, i, j);
            }

        }
    }

    public void drawScreen(int x, int y, float f) {
        this.drawDefaultBackground();
        this.drawCenteredString(super.fontRenderer, this.title, super.width / 2, 20, 16777215);
        super.drawScreen(x, y, f);
        if (Math.abs(x - this.lastMouseX) <= 5 && Math.abs(y - this.lastMouseY) <= 5) {
            short activateDelay = 700;
            if (System.currentTimeMillis() >= this.mouseStillTime + (long) activateDelay) {
                int x1 = super.width / 2 - 150;
                int y1 = super.height / 6 - 5;
                if (y <= y1 + 98) {
                    y1 += 105;
                }

                int x2 = x1 + 150 + 150;
                int y2 = y1 + 84 + 10;
                GuiButton btn = this.getSelectedButton(x, y);
                if (btn != null) {
                    String s = this.getButtonName(btn.displayString);
                    String[] lines = this.getTooltipLines(s);
                    if (lines == null) {
                        return;
                    }

                    this.drawGradientRect(x1, y1, x2, y2, -536870912, -536870912);

                    for (int i = 0; i < lines.length; ++i) {
                        String line = lines[i];
                        super.fontRenderer.drawStringWithShadow(line, x1 + 5, y1 + 5 + i * 11, 14540253);
                    }
                }

            }
        } else {
            this.lastMouseX = x;
            this.lastMouseY = y;
            this.mouseStillTime = System.currentTimeMillis();
        }
    }

    private String[] getTooltipLines(String btnName) {
    	  return btnName.equals("Clouds")?new String[]{"Облака", "  По умолчанию - стандартные настройки", "  Быстро - плохое качество", "  Красиво - высокое качество", "  Выкл. - отключить облака", "Облака с низким качеством изображаются в 2D.", "Облака с высоким качеством изображаются в 3D."}:(btnName.equals("Cloud Height")?new String[]{"Высота облаков", "  Вкл. – высота по умолчанию", "  100% - Предел высоты"}:(btnName.equals("Trees")?new String[]{"Деревья", "  По умолчанию - стандартные настройки", "  Быстро - низкое качество", "  Красиво - высокое качество", "При низком качестве листва непрозрачная.", "При высоком качестве листва деревьев имеет полупрозрачную текстуру."}:(btnName.equals("Grass")?new String[]{"Трава", "  По умолчанию - стандартные настройки", "  Быстро - плохое качество", "  Красиво - высокое качество", "У блоков травы при низком качестве текстура стандартная.", "При высоком качестве используются текстуры биома."}:(btnName.equals("Dropped Items")?new String[]{"Брошенные предметы", "  По умолчанию - стандартные настройки", "  Быстро – предметы изображаются в 2D", "  Красиво - 3D эффект"}:(btnName.equals("Water")?new String[]{"Вода", "  По умолчанию - стандартные настройки", "  Быстро  - плохое качество", "  Красиво - высокое качество", "Вода при низком качестве не имеет визуальных эффектов", "При высоком качестве включаются все эффекты течения"}:(btnName.equals("Rain & Snow")?new String[]{"Дождь и снег", "  По умолчанию - стандартные настройки", "  Быстро  - небольшой дождь/снег", "  Красиво – сильный дождь/снег", "  Выкл. – выключить дождь/снег", "Когда дождь/снег отключен, частицы и звук", "не выключаются."}:(btnName.equals("Sky")?new String[]{"Небо", "  Вкл. – видимое небо", "  Выкл.  - откл. небо", "При отключении неба луна и солнце все равно видны."}:(btnName.equals("Stars")?new String[]{"Звезды", "  Вкл. – звезды видны на небе", "  Выкл.  - ночью нет звезд"}:(btnName.equals("Туман")?new String[]{"Depth Fog", "  Вкл. – туман только у бедрока (По умолчанию)", "  Выкл. – отключает туман"}:(btnName.equals("Show Capes")?new String[]{"Показывать плащи", "  Вкл. – видны плащи игроков (По умолчанию)", "  Выкл. – плащи отключены"}:null))))))))));    }

    private String getButtonName(String displayString) {
        int pos = displayString.indexOf(58);
        return pos < 0 ? displayString : displayString.substring(0, pos);
    }

    private GuiButton getSelectedButton(int i, int j) {
        for (int k = 0; k < super.buttonList.size(); ++k) {
            GuiButton btn = (GuiButton) super.buttonList.get(k);
            boolean flag = i >= btn.xPosition && j >= btn.yPosition && i < btn.xPosition + btn.width && j < btn.yPosition + btn.height;
            if (flag) {
                return btn;
            }
        }

        return null;
    }
}
