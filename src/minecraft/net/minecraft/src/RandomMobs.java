package net.minecraft.src;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import net.minecraft.client.renderer.RenderEngine;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.src.Config;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public class RandomMobs {

   private static Map textureVariantsMap = new HashMap();
   private static RenderGlobal renderGlobal = null;
   private static boolean initialized = false;
   private static Random random = new Random();
   private static boolean working = false;


   public static void entityLoaded(Entity entity) {
      if(entity instanceof EntityLiving) {
         if(!(entity instanceof EntityPlayer)) {
            EntityLiving el = (EntityLiving)entity;
            WorldServer ws = Config.getWorldServer();
            if(ws != null) {
               Entity es = ws.getEntityByID(entity.entityId);
               if(es instanceof EntityLiving) {
                  EntityLiving els = (EntityLiving)es;
                  int randomId = els.persistentId;
                  el.persistentId = randomId;
               }
            }
         }
      }
   }

   public static void worldChanged(World oldWorld, World newWorld) {
      if(newWorld != null) {
         List entityList = newWorld.getLoadedEntityList();

         for(int e = 0; e < entityList.size(); ++e) {
            Entity entity = (Entity)entityList.get(e);
            entityLoaded(entity);
         }
      }

   }

   public static String getTexture(String texture) {
      if(working) {
         return texture;
      } else {
         String entityLiving;
         try {
            working = true;
            if(!initialized) {
               initialize();
            }

            if(renderGlobal == null) {
               String entity1 = texture;
               return entity1;
            }

            Entity entity = renderGlobal.renderedEntity;
            if(entity == null) {
               entityLiving = texture;
               return entityLiving;
            }

            if(entity instanceof EntityLiving) {
               EntityLiving entityLiving1 = (EntityLiving)entity;
               String var3;
               if(!texture.startsWith("/mob/")) {
                  var3 = texture;
                  return var3;
               }

               var3 = getTexture(texture, entityLiving1.persistentId);
               return var3;
            }

            entityLiving = texture;
         } finally {
            working = false;
         }

         return entityLiving;
      }
   }

   private static String getTexture(String texture, int randomId) {
      if(randomId <= 0) {
         return texture;
      } else {
         String[] texs = (String[])((String[])textureVariantsMap.get(texture));
         if(texs == null) {
            texs = getTextureVariants(texture);
            textureVariantsMap.put(texture, texs);
         }

         if(texs != null && texs.length > 0) {
            int index = randomId % texs.length;
            String tex = texs[index];
            return tex;
         } else {
            return texture;
         }
      }
   }

   private static String[] getTextureVariants(String texture) {
      RenderEngine renderEngine = Config.getRenderEngine();
      renderEngine.getTexture(texture);
      String[] texs = new String[0];
      int pointPos = texture.lastIndexOf(46);
      if(pointPos < 0) {
         return texs;
      } else {
         String prefix = texture.substring(0, pointPos);
         String suffix = texture.substring(pointPos);
         int countVariants = getCountTextureVariants(texture, prefix, suffix);
         if(countVariants <= 1) {
            return texs;
         } else {
            texs = new String[countVariants];
            texs[0] = texture;

            for(int i = 1; i < texs.length; ++i) {
               int texNum = i + 1;
               String texName = prefix + texNum + suffix;
               texs[i] = texName;
               renderEngine.getTexture(texName);
            }

            Config.dbg("RandomMobs: " + texture + ", variants: " + texs.length);
            return texs;
         }
      }
   }

   private static int getCountTextureVariants(String texture, String prefix, String suffix) {
      RenderEngine renderEngine = Config.getRenderEngine();
      short maxNum = 1000;

      for(int num = 2; num < maxNum; ++num) {
         String variant = prefix + num + suffix;

         try {
            InputStream e = renderEngine.texturePack.getSelectedTexturePack().getResourceAsStream(variant);
            if(e == null) {
               return num - 1;
            }

            e.close();
         } catch (IOException var8) {
            return num - 1;
         }
      }

      return maxNum;
   }

   public static void resetTextures() {
      textureVariantsMap.clear();
      if(Config.isRandomMobs()) {
         initialize();
      }

   }

   private static void initialize() {
      renderGlobal = Config.getRenderGlobal();
      if(renderGlobal != null) {
         initialized = true;
         ArrayList list = new ArrayList();
         list.add("bat");
         list.add("cat_black");
         list.add("cat_red");
         list.add("cat_siamese");
         list.add("cavespider");
         list.add("chicken");
         list.add("cow");
         list.add("creeper");
         list.add("enderman");
         list.add("enderman_eyes");
         list.add("fire");
         list.add("ghast");
         list.add("ghast_fire");
         list.add("lava");
         list.add("ozelot");
         list.add("pig");
         list.add("pigman");
         list.add("pigzombie");
         list.add("redcow");
         list.add("saddle");
         list.add("sheep");
         list.add("sheep_fur");
         list.add("silverfish");
         list.add("skeleton");
         list.add("skeleton_wither");
         list.add("slime");
         list.add("snowman");
         list.add("spider");
         list.add("spider_eyes");
         list.add("squid");
         list.add("villager");
         list.add("villager_golem");
         list.add("wither");
         list.add("wither_invul");
         list.add("wolf");
         list.add("wolf_angry");
         list.add("wolf_collar");
         list.add("wolf_tame");
         list.add("zombie");
         list.add("zombie_villager");

         for(int i = 0; i < list.size(); ++i) {
            String name = (String)list.get(i);
            String tex = "/mob/" + name + ".png";
            getTexture(tex, 100);
         }

      }
   }

}
