package net.minecraft.src;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import net.minecraft.logging.ILogAgent;
import net.minecraft.network.packet.Packet70GameEvent;
import net.minecraft.profiler.Profiler;
import net.minecraft.server.MinecraftServer;
import net.minecraft.src.Config;
import net.minecraft.src.NextTickHashSet;
import net.minecraft.world.ChunkCoordIntPair;
import net.minecraft.world.NextTickListEntry;
import net.minecraft.world.WorldServer;
import net.minecraft.world.WorldSettings;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.storage.ISaveHandler;

public class WorldServerOF extends WorldServer {

   private NextTickHashSet nextTickHashSet = null;
   private TreeSet pendingTickList = null;


   public WorldServerOF(MinecraftServer par1MinecraftServer, ISaveHandler par2iSaveHandler, String par3Str, int par4, WorldSettings par5WorldSettings, Profiler par6Profiler, ILogAgent par7ILogAgent) {
      super(par1MinecraftServer, par2iSaveHandler, par3Str, par4, par5WorldSettings, par6Profiler, par7ILogAgent);
      this.fixSetNextTicks();
   }

   private void fixSetNextTicks() {
      try {
         Field[] e = WorldServer.class.getDeclaredFields();
         if(e.length > 5) {
            Field f = e[3];
            f.setAccessible(true);
            if(f.getType() == Set.class) {
               Set oldSet = (Set)f.get(this);
               NextTickHashSet newSet = new NextTickHashSet(oldSet);
               f.set(this, newSet);
               Field f2 = e[4];
               f2.setAccessible(true);
               this.pendingTickList = (TreeSet)f2.get(this);
               this.nextTickHashSet = newSet;
            }
         }
      } catch (Exception var6) {
         Config.dbg("Error setting WorldServer.nextTickSet: " + var6.getMessage());
      }

   }

   public List getPendingBlockUpdates(Chunk par1Chunk, boolean par2) {
      if(this.nextTickHashSet != null && this.pendingTickList != null) {
         ArrayList var3 = null;
         ChunkCoordIntPair var4 = par1Chunk.getChunkCoordIntPair();
         int var5 = var4.chunkXPos << 4;
         int var6 = var5 + 16;
         int var7 = var4.chunkZPos << 4;
         int var8 = var7 + 16;
         Iterator var9 = this.nextTickHashSet.getNextTickEntries(var4.chunkXPos, var4.chunkZPos);

         while(var9.hasNext()) {
            NextTickListEntry var10 = (NextTickListEntry)var9.next();
            if(var10.xCoord >= var5 && var10.xCoord < var6 && var10.zCoord >= var7 && var10.zCoord < var8) {
               if(par2) {
                  this.pendingTickList.remove(var10);
                  var9.remove();
               }

               if(var3 == null) {
                  var3 = new ArrayList();
               }

               var3.add(var10);
            } else {
               Config.dbg("Not matching: " + var5 + "," + var7);
            }
         }

         return var3;
      } else {
         return super.getPendingBlockUpdates(par1Chunk, par2);
      }
   }

   protected void updateWeather() {
      if(Config.isWeatherEnabled()) {
         super.updateWeather();
      } else {
         this.fixWorldWeather();
      }

      if(!Config.isTimeDefault()) {
         this.fixWorldTime();
      }

   }

   private void fixWorldWeather() {
      if(super.worldInfo.isRaining() || super.worldInfo.isThundering()) {
         super.worldInfo.setRainTime(0);
         super.worldInfo.setRaining(false);
         this.setRainStrength(0.0F);
         super.worldInfo.setThunderTime(0);
         super.worldInfo.setThundering(false);
         this.getMinecraftServer().getConfigurationManager().sendPacketToAllPlayers(new Packet70GameEvent(2, 0));
      }

   }

   private void fixWorldTime() {
      if(super.worldInfo.getGameType().getID() == 1) {
         long time = this.getWorldTime();
         long timeOfDay = time % 24000L;
         if(Config.isTimeDayOnly()) {
            if(timeOfDay <= 1000L) {
               this.setWorldTime(time - timeOfDay + 1001L);
            }

            if(timeOfDay >= 11000L) {
               this.setWorldTime(time - timeOfDay + 24001L);
            }
         }

         if(Config.isTimeNightOnly()) {
            if(timeOfDay <= 14000L) {
               this.setWorldTime(time - timeOfDay + 14001L);
            }

            if(timeOfDay >= 22000L) {
               this.setWorldTime(time - timeOfDay + 24000L + 14001L);
            }
         }

      }
   }
}
