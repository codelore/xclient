package net.minecraft.src;

import java.util.HashSet;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.src.IWrUpdateListener;
import net.minecraft.src.Reflector;
import net.minecraft.src.WrUpdateControl;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.ChunkCache;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import org.lwjgl.opengl.GL11;

public class WorldRendererThreaded extends WorldRenderer {

   private int glRenderListStable;
   private int glRenderListBoundingBox;


   public WorldRendererThreaded(World par1World, List par2List, int par3, int par4, int par5, int par6) {
      super(par1World, par2List, par3, par4, par5, par6);
      this.glRenderListStable = super.glRenderList + 393216;
      this.glRenderListBoundingBox = super.glRenderList + 2;
   }

   public void updateRenderer() {
      if(super.worldObj != null) {
         this.updateRenderer((IWrUpdateListener)null);
         this.finishUpdate();
      }
   }

   public void updateRenderer(IWrUpdateListener updateListener) {
      if(super.worldObj != null) {
         super.needsUpdate = false;
         int xMin = super.posX;
         int yMin = super.posY;
         int zMin = super.posZ;
         int xMax = super.posX + 16;
         int yMax = super.posY + 16;
         int zMax = super.posZ + 16;
         boolean[] tempSkipRenderPass = new boolean[2];

         for(int hashset = 0; hashset < tempSkipRenderPass.length; ++hashset) {
            tempSkipRenderPass[hashset] = true;
         }

         if(Reflector.LightCache.exists()) {
            Object var28 = Reflector.getFieldValue(Reflector.LightCache_cache);
            Reflector.callVoid(var28, Reflector.LightCache_clear, new Object[0]);
            Reflector.callVoid(Reflector.BlockCoord_resetPool, new Object[0]);
         }

         Chunk.isLit = false;
         HashSet var27 = new HashSet();
         var27.addAll(super.tileEntityRenderers);
         super.tileEntityRenderers.clear();
         byte one = 1;
         ChunkCache chunkcache = new ChunkCache(super.worldObj, xMin - one, yMin - one, zMin - one, xMax + one, yMax + one, zMax + one, one);
         if(!chunkcache.extendedLevelsInChunkCache()) {
            ++WorldRenderer.chunksUpdated;
            RenderBlocks hashset1 = new RenderBlocks(chunkcache);
            super.bytesDrawn = 0;
            Tessellator tessellator = Tessellator.instance;
            boolean hasForge = Reflector.ForgeHooksClient.exists();
            WrUpdateControl uc = new WrUpdateControl();

            for(int renderPass = 0; renderPass < 2; ++renderPass) {
               uc.setRenderPass(renderPass);
               boolean renderNextPass = false;
               boolean hasRenderedBlocks = false;
               boolean hasGlList = false;

               for(int y = yMin; y < yMax; ++y) {
                  if(hasRenderedBlocks && updateListener != null) {
                     updateListener.updating(uc);
                  }

                  for(int z = zMin; z < zMax; ++z) {
                     for(int x = xMin; x < xMax; ++x) {
                        int i3 = chunkcache.getBlockId(x, y, z);
                        if(i3 > 0) {
                           if(!hasGlList) {
                              hasGlList = true;
                              GL11.glNewList(super.glRenderList + renderPass, 4864);
                              tessellator.setRenderingChunk(true);
                              tessellator.startDrawingQuads();
                              tessellator.setTranslation((double)(-globalChunkOffsetX), 0.0D, (double)(-globalChunkOffsetZ));
                           }

                           Block block = Block.blocksList[i3];
                           if(renderPass == 0 && block.hasTileEntity()) {
                              TileEntity blockPass = chunkcache.getBlockTileEntity(x, y, z);
                              if(TileEntityRenderer.instance.hasSpecialRenderer(blockPass)) {
                                 super.tileEntityRenderers.add(blockPass);
                              }
                           }

                           int var31 = block.getRenderBlockPass();
                           boolean canRender = true;
                           if(var31 != renderPass) {
                              renderNextPass = true;
                              canRender = false;
                           }

                           if(hasForge) {
                              canRender = Reflector.callBoolean(block, Reflector.ForgeBlock_canRenderInPass, new Object[]{Integer.valueOf(renderPass)});
                           }

                           if(canRender) {
                              hasRenderedBlocks |= hashset1.renderBlockByRenderType(block, x, y, z);
                           }
                        }
                     }
                  }
               }

               if(hasGlList) {
                  if(updateListener != null) {
                     updateListener.updating(uc);
                  }

                  super.bytesDrawn += tessellator.draw();
                  GL11.glEndList();
                  tessellator.setRenderingChunk(false);
                  tessellator.setTranslation(0.0D, 0.0D, 0.0D);
               } else {
                  hasRenderedBlocks = false;
               }

               if(hasRenderedBlocks) {
                  tempSkipRenderPass[renderPass] = false;
               }

               if(!renderNextPass) {
                  break;
               }
            }
         }

         for(int var29 = 0; var29 < 2; ++var29) {
            super.skipRenderPass[var29] = tempSkipRenderPass[var29];
         }

         HashSet var30 = new HashSet();
         var30.addAll(super.tileEntityRenderers);
         var30.removeAll(var27);
         super.tileEntities.addAll(var30);
         var27.removeAll(super.tileEntityRenderers);
         super.tileEntities.removeAll(var27);
         super.isChunkLit = Chunk.isLit;
         super.isInitialized = true;
         super.isVisible = true;
         this.isVisibleFromPosition = false;
      }
   }

   public void finishUpdate() {
      int temp = super.glRenderList;
      super.glRenderList = this.glRenderListStable;
      this.glRenderListStable = temp;

      for(int f = 0; f < 2; ++f) {
         if(!super.skipRenderPass[f]) {
            GL11.glNewList(super.glRenderList + f, 4864);
            GL11.glEndList();
         }
      }

      if(this.needsBoxUpdate && !this.skipAllRenderPasses()) {
         float var3 = 0.0F;
         GL11.glNewList(this.glRenderListBoundingBox, 4864);
         RenderItem.renderAABB(AxisAlignedBB.getAABBPool().getAABB((double)((float)super.posXClip - var3), (double)((float)super.posYClip - var3), (double)((float)super.posZClip - var3), (double)((float)(super.posXClip + 16) + var3), (double)((float)(super.posYClip + 16) + var3), (double)((float)(super.posZClip + 16) + var3)));
         GL11.glEndList();
         this.needsBoxUpdate = false;
      }

   }

   public int getGLCallListForPass(int par1) {
      return !super.isInFrustum?-1:(!super.skipRenderPass[par1]?this.glRenderListStable + par1:-1);
   }

   public void callOcclusionQueryList() {
      GL11.glCallList(this.glRenderListBoundingBox);
   }
}
