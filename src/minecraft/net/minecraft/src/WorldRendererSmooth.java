package net.minecraft.src;

import java.util.HashSet;
import java.util.List;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.src.Reflector;
import net.minecraft.src.WrUpdateState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.world.ChunkCache;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import org.lwjgl.opengl.GL11;

public class WorldRendererSmooth extends WorldRenderer {

   private WrUpdateState updateState = new WrUpdateState();
   public int activeSet = 0;
   public int[] activeListIndex = new int[]{0, 0};
   public int[][][] glWorkLists = new int[2][2][16];
   public boolean[] tempSkipRenderPass = new boolean[2];


   public WorldRendererSmooth(World par1World, List par2List, int par3, int par4, int par5, int par6) {
      super(par1World, par2List, par3, par4, par5, par6);
      int glWorkBase = 393216 + 64 * (super.glRenderList / 3);

      for(int set = 0; set < 2; ++set) {
         int setBase = glWorkBase + set * 2 * 16;

         for(int pass = 0; pass < 2; ++pass) {
            int passBase = setBase + pass * 16;

            for(int t = 0; t < 16; ++t) {
               this.glWorkLists[set][pass][t] = passBase + t;
            }
         }
      }

   }

   public void setPosition(int px, int py, int pz) {
      if(this.isUpdating) {
         this.updateRenderer();
      }

      super.setPosition(px, py, pz);
   }

   public void updateRenderer() {
      if(super.worldObj != null) {
         this.updateRenderer(0L);
         this.finishUpdate();
      }
   }

   public boolean updateRenderer(long finishTime) {
      if(super.worldObj == null) {
         return true;
      } else {
         super.needsUpdate = false;
         if(!this.isUpdating) {
            if(this.needsBoxUpdate) {
               float xMin = 0.0F;
               GL11.glNewList(super.glRenderList + 2, 4864);
               RenderItem.renderAABB(AxisAlignedBB.getAABBPool().getAABB((double)((float)super.posXClip - xMin), (double)((float)super.posYClip - xMin), (double)((float)super.posZClip - xMin), (double)((float)(super.posXClip + 16) + xMin), (double)((float)(super.posYClip + 16) + xMin), (double)((float)(super.posZClip + 16) + xMin)));
               GL11.glEndList();
               this.needsBoxUpdate = false;
            }

            if(Reflector.LightCache.exists()) {
               Object var25 = Reflector.getFieldValue(Reflector.LightCache_cache);
               Reflector.callVoid(var25, Reflector.LightCache_clear, new Object[0]);
               Reflector.callVoid(Reflector.BlockCoord_resetPool, new Object[0]);
            }

            Chunk.isLit = false;
         }

         int var26 = super.posX;
         int yMin = super.posY;
         int zMin = super.posZ;
         int xMax = super.posX + 16;
         int yMax = super.posY + 16;
         int zMax = super.posZ + 16;
         ChunkCache chunkcache = null;
         RenderBlocks renderblocks = null;
         HashSet setOldEntityRenders = null;
         if(!this.isUpdating) {
            for(int setNewEntityRenderers = 0; setNewEntityRenderers < 2; ++setNewEntityRenderers) {
               this.tempSkipRenderPass[setNewEntityRenderers] = true;
            }

            byte var27 = 1;
            chunkcache = new ChunkCache(super.worldObj, var26 - var27, yMin - var27, zMin - var27, xMax + var27, yMax + var27, zMax + var27, var27);
            renderblocks = new RenderBlocks(chunkcache);
            setOldEntityRenders = new HashSet();
            setOldEntityRenders.addAll(super.tileEntityRenderers);
            super.tileEntityRenderers.clear();
         }

         if(this.isUpdating || !chunkcache.extendedLevelsInChunkCache()) {
            super.bytesDrawn = 0;
            Tessellator var28 = Tessellator.instance;
            boolean hasForge = Reflector.ForgeHooksClient.exists();

            for(int renderPass = 0; renderPass < 2; ++renderPass) {
               boolean renderNextPass = false;
               boolean hasRenderedBlocks = false;
               boolean hasGlList = false;

               for(int y = yMin; y < yMax; ++y) {
                  if(this.isUpdating) {
                     this.isUpdating = false;
                     chunkcache = this.updateState.chunkcache;
                     renderblocks = this.updateState.renderblocks;
                     setOldEntityRenders = this.updateState.setOldEntityRenders;
                     renderPass = this.updateState.renderPass;
                     y = this.updateState.y;
                     renderNextPass = this.updateState.flag;
                     hasRenderedBlocks = this.updateState.hasRenderedBlocks;
                     hasGlList = this.updateState.hasGlList;
                     if(hasGlList) {
                        GL11.glNewList(this.glWorkLists[this.activeSet][renderPass][this.activeListIndex[renderPass]], 4864);
                        var28.setRenderingChunk(true);
                        var28.startDrawingQuads();
                        var28.setTranslation((double)(-globalChunkOffsetX), 0.0D, (double)(-globalChunkOffsetZ));
                     }
                  } else if(hasGlList && finishTime != 0L && System.nanoTime() - finishTime > 0L && this.activeListIndex[renderPass] < 15) {
                     var28.draw();
                     GL11.glEndList();
                     var28.setRenderingChunk(false);
                     var28.setTranslation(0.0D, 0.0D, 0.0D);
                     ++this.activeListIndex[renderPass];
                     this.updateState.chunkcache = chunkcache;
                     this.updateState.renderblocks = renderblocks;
                     this.updateState.setOldEntityRenders = setOldEntityRenders;
                     this.updateState.renderPass = renderPass;
                     this.updateState.y = y;
                     this.updateState.flag = renderNextPass;
                     this.updateState.hasRenderedBlocks = hasRenderedBlocks;
                     this.updateState.hasGlList = hasGlList;
                     this.isUpdating = true;
                     return false;
                  }

                  for(int z = zMin; z < zMax; ++z) {
                     for(int x = var26; x < xMax; ++x) {
                        int i3 = chunkcache.getBlockId(x, y, z);
                        if(i3 > 0) {
                           if(!hasGlList) {
                              hasGlList = true;
                              GL11.glNewList(this.glWorkLists[this.activeSet][renderPass][this.activeListIndex[renderPass]], 4864);
                              var28.setRenderingChunk(true);
                              var28.startDrawingQuads();
                              var28.setTranslation((double)(-globalChunkOffsetX), 0.0D, (double)(-globalChunkOffsetZ));
                           }

                           Block block = Block.blocksList[i3];
                           if(renderPass == 0 && block.hasTileEntity()) {
                              TileEntity blockPass = chunkcache.getBlockTileEntity(x, y, z);
                              if(TileEntityRenderer.instance.hasSpecialRenderer(blockPass)) {
                                 super.tileEntityRenderers.add(blockPass);
                              }
                           }

                           int var30 = block.getRenderBlockPass();
                           boolean canRender = true;
                           if(var30 != renderPass) {
                              renderNextPass = true;
                              canRender = false;
                           }

                           if(hasForge) {
                              canRender = Reflector.callBoolean(block, Reflector.ForgeBlock_canRenderInPass, new Object[]{Integer.valueOf(renderPass)});
                           }

                           if(canRender) {
                              hasRenderedBlocks |= renderblocks.renderBlockByRenderType(block, x, y, z);
                           }
                        }
                     }
                  }
               }

               if(hasGlList) {
                  super.bytesDrawn += var28.draw();
                  GL11.glEndList();
                  var28.setRenderingChunk(false);
                  var28.setTranslation(0.0D, 0.0D, 0.0D);
               } else {
                  hasRenderedBlocks = false;
               }

               if(hasRenderedBlocks) {
                  this.tempSkipRenderPass[renderPass] = false;
               }

               if(!renderNextPass) {
                  break;
               }
            }
         }

         HashSet var29 = new HashSet();
         var29.addAll(super.tileEntityRenderers);
         var29.removeAll(setOldEntityRenders);
         super.tileEntities.addAll(var29);
         setOldEntityRenders.removeAll(super.tileEntityRenderers);
         super.tileEntities.removeAll(setOldEntityRenders);
         super.isChunkLit = Chunk.isLit;
         super.isInitialized = true;
         ++WorldRenderer.chunksUpdated;
         super.isVisible = true;
         this.isVisibleFromPosition = false;
         super.skipRenderPass[0] = this.tempSkipRenderPass[0];
         super.skipRenderPass[1] = this.tempSkipRenderPass[1];
         this.isUpdating = false;
         return true;
      }
   }

   public void finishUpdate() {
      int pass;
      int i;
      int list;
      for(pass = 0; pass < 2; ++pass) {
         if(!super.skipRenderPass[pass]) {
            GL11.glNewList(super.glRenderList + pass, 4864);

            for(i = 0; i <= this.activeListIndex[pass]; ++i) {
               list = this.glWorkLists[this.activeSet][pass][i];
               GL11.glCallList(list);
            }

            GL11.glEndList();
         }
      }

      if(this.activeSet == 0) {
         this.activeSet = 1;
      } else {
         this.activeSet = 0;
      }

      for(pass = 0; pass < 2; ++pass) {
         if(!super.skipRenderPass[pass]) {
            for(i = 0; i <= this.activeListIndex[pass]; ++i) {
               list = this.glWorkLists[this.activeSet][pass][i];
               GL11.glNewList(list, 4864);
               GL11.glEndList();
            }
         }
      }

      for(pass = 0; pass < 2; ++pass) {
         this.activeListIndex[pass] = 0;
      }

   }
}
