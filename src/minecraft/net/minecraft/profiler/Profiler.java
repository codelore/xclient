package net.minecraft.profiler;

import net.minecraft.src.Config;

import java.util.*;

public class Profiler {
   public static List<String> errors = new ArrayList<String>();

   private final List sectionList = new ArrayList();
   private final List timestampList = new ArrayList();

   public boolean profilingEnabled = true;
   private String profilingSection = "";
   private final Map profilingMap = new HashMap();
   public boolean profilerGlobalEnabled = true;
   private boolean profilerLocalEnabled;
   private long startTickNano;
   public long timeTickNano;
   private long startUpdateChunksNano;
   public long timeUpdateChunksNano;


   public Profiler() {
      this.profilerLocalEnabled = this.profilerGlobalEnabled;
      this.startTickNano = 0L;
      this.timeTickNano = 0L;
      this.startUpdateChunksNano = 0L;
      this.timeUpdateChunksNano = 0L;
   }

   public void clearProfiling() {
      this.profilingMap.clear();
      this.profilingSection = "";
      this.sectionList.clear();
      this.profilerLocalEnabled = this.profilerGlobalEnabled;
   }

   public void startSection(String par1Str) {
      if(Config.getGameSettings().showDebugInfo) {
         if(this.startTickNano == 0L && par1Str.equals("tick")) {
            this.startTickNano = System.nanoTime();
         }

         if(this.startTickNano != 0L && par1Str.equals("preRenderErrors")) {
            this.timeTickNano = System.nanoTime() - this.startTickNano;
            this.startTickNano = 0L;
         }

         if(this.startUpdateChunksNano == 0L && par1Str.equals("updatechunks")) {
            this.startUpdateChunksNano = System.nanoTime();
         }

         if(this.startUpdateChunksNano != 0L && par1Str.equals("terrain")) {
            this.timeUpdateChunksNano = System.nanoTime() - this.startUpdateChunksNano;
            this.startUpdateChunksNano = 0L;
         }
      }

      if(this.profilerLocalEnabled) {
         if(this.profilingEnabled) {
            if(this.profilingSection.length() > 0) {
               this.profilingSection = this.profilingSection + ".";
            }

            this.profilingSection = this.profilingSection + par1Str;
            this.sectionList.add(this.profilingSection);
            this.timestampList.add(Long.valueOf(System.nanoTime()));
         }

      }
   }

   public void endSection() {
       try {
           if(this.profilerLocalEnabled) {
               if(this.profilingEnabled) {
                   if (timestampList.size() <= 0)
                       return;

                   long var1 = System.nanoTime();
                   long var3 = ((Long)this.timestampList.remove(this.timestampList.size() - 1)).longValue();
                   this.sectionList.remove(this.sectionList.size() - 1);
                   long var5 = var1 - var3;
                   if(this.profilingMap.containsKey(this.profilingSection)) {
                       this.profilingMap.put(this.profilingSection, Long.valueOf(((Long)this.profilingMap.get(this.profilingSection)).longValue() + var5));
                   } else {
                       this.profilingMap.put(this.profilingSection, Long.valueOf(var5));
                   }

                   if(var5 > 100000000L) {
                       System.out.println("Something\'s taking too long! \'" + this.profilingSection + "\' took aprox " + (double)var5 / 1000000.0D + " ms");
                   }

                   this.profilingSection = !this.sectionList.isEmpty()?(String)this.sectionList.get(this.sectionList.size() - 1):"";
               }
           }
       } catch (NoSuchElementException ex) {
            errors.add("No such element exception: " + ex.getMessage() + ": " + Arrays.toString(ex.getStackTrace()));
       }
   }

   public List getProfilingData(String par1Str) {
      this.profilerLocalEnabled = this.profilerGlobalEnabled;
      if(!this.profilerLocalEnabled) {
         return new ArrayList(Arrays.asList(new ProfilerResult[]{new ProfilerResult("root", 0.0D, 0.0D)}));
      } else if(!this.profilingEnabled) {
         return null;
      } else {
         long var3 = this.profilingMap.containsKey("root")?((Long)this.profilingMap.get("root")).longValue():0L;
         long var5 = this.profilingMap.containsKey(par1Str)?((Long)this.profilingMap.get(par1Str)).longValue():-1L;
         ArrayList var7 = new ArrayList();
         if(par1Str.length() > 0) {
            par1Str = par1Str + ".";
         }

         long var8 = 0L;
         Iterator var10 = this.profilingMap.keySet().iterator();

         while(var10.hasNext()) {
            String var21 = (String)var10.next();
            if(var21.length() > par1Str.length() && var21.startsWith(par1Str) && var21.indexOf(".", par1Str.length() + 1) < 0) {
               var8 += ((Long)this.profilingMap.get(var21)).longValue();
            }
         }

         float var211 = (float)var8;
         if(var8 < var5) {
            var8 = var5;
         }

         if(var3 < var8) {
            var3 = var8;
         }

         Iterator var20 = this.profilingMap.keySet().iterator();

         String var12;
         while(var20.hasNext()) {
            var12 = (String)var20.next();
            if(var12.length() > par1Str.length() && var12.startsWith(par1Str) && var12.indexOf(".", par1Str.length() + 1) < 0) {
               long var13 = ((Long)this.profilingMap.get(var12)).longValue();
               double var15 = (double)var13 * 100.0D / (double)var8;
               double var17 = (double)var13 * 100.0D / (double)var3;
               String var19 = var12.substring(par1Str.length());
               var7.add(new ProfilerResult(var19, var15, var17));
            }
         }

         var20 = this.profilingMap.keySet().iterator();

         while(var20.hasNext()) {
            var12 = (String)var20.next();
            this.profilingMap.put(var12, Long.valueOf(((Long)this.profilingMap.get(var12)).longValue() * 999L / 1000L));
         }

         if((float)var8 > var211) {
            var7.add(new ProfilerResult("unspecified", (double)((float)var8 - var211) * 100.0D / (double)var8, (double)((float)var8 - var211) * 100.0D / (double)var3));
         }

         Collections.sort(var7);
         var7.add(0, new ProfilerResult(par1Str, 100.0D, (double)var8 * 100.0D / (double)var3));
         return var7;
      }
   }

   public void endStartSection(String par1Str) {
      if(this.profilerLocalEnabled) {
         this.endSection();
         this.startSection(par1Str);
      }
   }

   public String getNameOfLastSection() {
      return this.sectionList.size() == 0?"[UNKNOWN]":(String)this.sectionList.get(this.sectionList.size() - 1);
   }
}
