package net.minecraft.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.StepSound;
import net.minecraft.block.material.Material;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.CallableEffectAmplifier;
import net.minecraft.entity.CallableEffectDuration;
import net.minecraft.entity.CallableEffectID;
import net.minecraft.entity.CallableEffectIsAmbient;
import net.minecraft.entity.CallableEffectIsSplash;
import net.minecraft.entity.CallableEffectName;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityBodyHelper;
import net.minecraft.entity.EntityTracker;
import net.minecraft.entity.EnumCreatureAttribute;
import net.minecraft.entity.ai.EntityAITasks;
import net.minecraft.entity.ai.EntityJumpHelper;
import net.minecraft.entity.ai.EntityLookHelper;
import net.minecraft.entity.ai.EntityMoveHelper;
import net.minecraft.entity.ai.EntitySenses;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.item.EntityXPOrb;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.monster.EntityGhast;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagFloat;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.network.packet.Packet18Animation;
import net.minecraft.network.packet.Packet22Collect;
import net.minecraft.network.packet.Packet5PlayerInventory;
import net.minecraft.pathfinding.PathNavigate;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionHelper;
import net.minecraft.src.Config;
import net.minecraft.src.Reflector;
import net.minecraft.util.ChunkCoordinates;
import net.minecraft.util.CombatTracker;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.ReportedException;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;

public abstract class EntityLiving extends Entity {

   private static final float[] enchantmentProbability = new float[]{0.0F, 0.0F, 0.1F, 0.2F};
   private static final float[] armorEnchantmentProbability = new float[]{0.0F, 0.0F, 0.25F, 0.5F};
   private static final float[] armorProbability = new float[]{0.0F, 0.0F, 0.05F, 0.07F};
   public static final float[] pickUpLootProability = new float[]{0.0F, 0.1F, 0.15F, 0.45F};
   public int maxHurtResistantTime = 20;
   public float field_70769_ao;
   public float field_70770_ap;
   public float renderYawOffset = 0.0F;
   public float prevRenderYawOffset = 0.0F;
   public float rotationYawHead = 0.0F;
   public float prevRotationYawHead = 0.0F;
   protected float field_70768_au;
   protected float field_70766_av;
   protected float field_70764_aw;
   protected float field_70763_ax;
   protected boolean field_70753_ay = true;
   protected String texture = "/mob/char.png";
   protected boolean field_70740_aA = true;
   protected float field_70741_aB = 0.0F;
   protected String entityType = null;
   protected float field_70743_aD = 1.0F;
   protected int scoreValue = 0;
   protected float field_70745_aF = 0.0F;
   public float landMovementFactor = 0.1F;
   public float jumpMovementFactor = 0.02F;
   public float prevSwingProgress;
   public float swingProgress;
   protected int health = this.getMaxHealth();
   public int prevHealth;
   public int carryoverDamage;
   public int livingSoundTime;
   public int hurtTime;
   public int maxHurtTime;
   public float attackedAtYaw = 0.0F;
   public int deathTime = 0;
   public int attackTime = 0;
   public float prevCameraPitch;
   public float cameraPitch;
   protected boolean dead = false;
   public int experienceValue;
   public int field_70731_aW = -1;
   public float field_70730_aX = (float)(Math.random() * 0.8999999761581421D + 0.10000000149011612D);
   public float prevLimbYaw;
   public float limbYaw;
   public float limbSwing;
   protected EntityPlayer attackingPlayer = null;
   protected int recentlyHit = 0;
   private EntityLiving entityLivingToAttack = null;
   private int revengeTimer = 0;
   private EntityLiving lastAttackingEntity = null;
   public int arrowHitTimer = 0;
   protected HashMap activePotionsMap = new HashMap();
   private boolean potionsNeedUpdate = true;
   private int field_70748_f;
   private EntityLookHelper lookHelper;
   private EntityMoveHelper moveHelper;
   private EntityJumpHelper jumpHelper;
   private EntityBodyHelper bodyHelper;
   private PathNavigate navigator;
   public final EntityAITasks tasks;
   public final EntityAITasks targetTasks;
   private EntityLiving attackTarget;
   private EntitySenses senses;
   private float AIMoveSpeed;
   private ChunkCoordinates homePosition = new ChunkCoordinates(0, 0, 0);
   private float maximumHomeDistance = -1.0F;
   private ItemStack[] equipment = new ItemStack[5];
   protected float[] equipmentDropChances = new float[5];
   private ItemStack[] previousEquipment = new ItemStack[5];
   public boolean isSwingInProgress = false;
   public int swingProgressInt = 0;
   private boolean canPickUpLoot = false;
   private boolean persistenceRequired = false;
   public final CombatTracker field_94063_bt = new CombatTracker(this);
   protected int newPosRotationIncrements;
   protected double newPosX;
   protected double newPosY;
   protected double newPosZ;
   protected double newRotationYaw;
   protected double newRotationPitch;
   float field_70706_bo = 0.0F;
   protected int lastDamage = 0;
   protected int entityAge = 0;
   protected float moveStrafing;
   protected float moveForward;
   protected float randomYawVelocity;
   public boolean isJumping = false;
   protected float defaultPitch = 0.0F;
   protected float moveSpeed = 0.7F;
   private int jumpTicks = 0;
   private Entity currentTarget;
   protected int numTicksToChaseTarget = 0;
   public int persistentId;


   public EntityLiving(World par1World) {
      super(par1World);
      this.persistentId = super.rand.nextInt(Integer.MAX_VALUE);
      super.preventEntitySpawning = true;
      this.tasks = new EntityAITasks(par1World != null && par1World.theProfiler != null?par1World.theProfiler:null);
      this.targetTasks = new EntityAITasks(par1World != null && par1World.theProfiler != null?par1World.theProfiler:null);
      this.lookHelper = new EntityLookHelper(this);
      this.moveHelper = new EntityMoveHelper(this);
      this.jumpHelper = new EntityJumpHelper(this);
      this.bodyHelper = new EntityBodyHelper(this);
      this.navigator = new PathNavigate(this, par1World, (float)this.func_96121_ay());
      this.senses = new EntitySenses(this);
      this.field_70770_ap = (float)(Math.random() + 1.0D) * 0.01F;
      this.setPosition(super.posX, super.posY, super.posZ);
      this.field_70769_ao = (float)Math.random() * 12398.0F;
      super.rotationYaw = (float)(Math.random() * 3.141592653589793D * 2.0D);
      this.rotationYawHead = super.rotationYaw;

      for(int var2 = 0; var2 < this.equipmentDropChances.length; ++var2) {
         this.equipmentDropChances[var2] = 0.085F;
      }

      super.stepHeight = 0.5F;
   }

   protected int func_96121_ay() {
      return 16;
   }

   public EntityLookHelper getLookHelper() {
      return this.lookHelper;
   }

   public EntityMoveHelper getMoveHelper() {
      return this.moveHelper;
   }

   public EntityJumpHelper getJumpHelper() {
      return this.jumpHelper;
   }

   public PathNavigate getNavigator() {
      return this.navigator;
   }

   public EntitySenses getEntitySenses() {
      return this.senses;
   }

   public Random getRNG() {
      return super.rand;
   }

   public EntityLiving getAITarget() {
      return this.entityLivingToAttack;
   }

   public EntityLiving getLastAttackingEntity() {
      return this.lastAttackingEntity;
   }

   public void setLastAttackingEntity(Entity par1Entity) {
      if(par1Entity instanceof EntityLiving) {
         this.lastAttackingEntity = (EntityLiving)par1Entity;
      }

   }

   public int getAge() {
      return this.entityAge;
   }

   public float getRotationYawHead() {
      return this.rotationYawHead;
   }

   public void setRotationYawHead(float par1) {
      this.rotationYawHead = par1;
   }

   public float getAIMoveSpeed() {
      return this.AIMoveSpeed;
   }

   public void setAIMoveSpeed(float par1) {
      this.AIMoveSpeed = par1;
      this.setMoveForward(par1);
   }

   public boolean attackEntityAsMob(Entity par1Entity) {
      this.setLastAttackingEntity(par1Entity);
      return false;
   }

   public EntityLiving getAttackTarget() {
      return this.attackTarget;
   }

   public void setAttackTarget(EntityLiving par1EntityLiving) {
      this.attackTarget = par1EntityLiving;
      if(Reflector.ForgeHooks_onLivingSetAttackTarget.exists()) {
         Reflector.callVoid(Reflector.ForgeHooks_onLivingSetAttackTarget, new Object[]{this, par1EntityLiving});
      }

   }

   public boolean canAttackClass(Class par1Class) {
      return EntityCreeper.class != par1Class && EntityGhast.class != par1Class;
   }

   public void eatGrassBonus() {}

   protected void updateFallState(double par1, boolean par3) {
      if(!this.isInWater()) {
         this.handleWaterMovement();
      }

      if(par3 && super.fallDistance > 0.0F) {
         int var4 = MathHelper.floor_double(super.posX);
         int var5 = MathHelper.floor_double(super.posY - 0.20000000298023224D - (double)super.yOffset);
         int var6 = MathHelper.floor_double(super.posZ);
         int var7 = super.worldObj.getBlockId(var4, var5, var6);
         if(var7 == 0) {
            int var8 = super.worldObj.blockGetRenderType(var4, var5 - 1, var6);
            if(var8 == 11 || var8 == 32 || var8 == 21) {
               var7 = super.worldObj.getBlockId(var4, var5 - 1, var6);
            }
         }

         if(var7 > 0) {
            Block.blocksList[var7].onFallenUpon(super.worldObj, var4, var5, var6, this, super.fallDistance);
         }
      }

      super.updateFallState(par1, par3);
   }

   public boolean isWithinHomeDistanceCurrentPosition() {
      return this.isWithinHomeDistance(MathHelper.floor_double(super.posX), MathHelper.floor_double(super.posY), MathHelper.floor_double(super.posZ));
   }

   public boolean isWithinHomeDistance(int par1, int par2, int par3) {
      return this.maximumHomeDistance == -1.0F?true:this.homePosition.getDistanceSquared(par1, par2, par3) < this.maximumHomeDistance * this.maximumHomeDistance;
   }

   public void setHomeArea(int par1, int par2, int par3, int par4) {
      this.homePosition.set(par1, par2, par3);
      this.maximumHomeDistance = (float)par4;
   }

   public ChunkCoordinates getHomePosition() {
      return this.homePosition;
   }

   public float getMaximumHomeDistance() {
      return this.maximumHomeDistance;
   }

   public void detachHome() {
      this.maximumHomeDistance = -1.0F;
   }

   public boolean hasHome() {
      return this.maximumHomeDistance != -1.0F;
   }

   public void setRevengeTarget(EntityLiving par1EntityLiving) {
      this.entityLivingToAttack = par1EntityLiving;
      this.revengeTimer = this.entityLivingToAttack != null?100:0;
      if(Reflector.ForgeHooks_onLivingSetAttackTarget.exists()) {
         Reflector.callVoid(Reflector.ForgeHooks_onLivingSetAttackTarget, new Object[]{this, par1EntityLiving});
      }

   }

   protected void entityInit() {
      super.dataWatcher.addObject(8, Integer.valueOf(this.field_70748_f));
      super.dataWatcher.addObject(9, Byte.valueOf((byte)0));
      super.dataWatcher.addObject(10, Byte.valueOf((byte)0));
      super.dataWatcher.addObject(6, Byte.valueOf((byte)0));
      super.dataWatcher.addObject(5, "");
   }

   public boolean canEntityBeSeen(Entity par1Entity) {
      return super.worldObj.rayTraceBlocks(super.worldObj.getWorldVec3Pool().getVecFromPool(super.posX, super.posY + (double)this.getEyeHeight(), super.posZ), super.worldObj.getWorldVec3Pool().getVecFromPool(par1Entity.posX, par1Entity.posY + (double)par1Entity.getEyeHeight(), par1Entity.posZ)) == null;
   }

   public String getTexture() {
      return this.texture;
   }

   public boolean canBeCollidedWith() {
      return !super.isDead;
   }

   public boolean canBePushed() {
      return !super.isDead;
   }

   public float getEyeHeight() {
      return super.height * 0.85F;
   }

   public int getTalkInterval() {
      return 80;
   }

   public void playLivingSound() {
      String var1 = this.getLivingSound();
      if(var1 != null) {
         this.playSound(var1, this.getSoundVolume(), this.getSoundPitch());
      }

   }

   public void onEntityUpdate() {
      this.prevSwingProgress = this.swingProgress;
      super.onEntityUpdate();
      super.worldObj.theProfiler.startSection("mobBaseTick");
      if(this.isEntityAlive() && super.rand.nextInt(1000) < this.livingSoundTime++) {
         this.livingSoundTime = -this.getTalkInterval();
         this.playLivingSound();
      }

      if(this.isEntityAlive() && this.isEntityInsideOpaqueBlock()) {
         this.attackEntityFrom(DamageSource.inWall, 1);
      }

      if(this.isImmuneToFire() || super.worldObj.isRemote) {
         this.extinguish();
      }

      boolean var1 = this instanceof EntityPlayer && ((EntityPlayer)this).capabilities.disableDamage;
      if(this.isEntityAlive() && this.isInsideOfMaterial(Material.water) && !this.canBreatheUnderwater() && !this.activePotionsMap.containsKey(Integer.valueOf(Potion.waterBreathing.id)) && !var1) {
         this.setAir(this.decreaseAirSupply(this.getAir()));
         if(this.getAir() == -20) {
            this.setAir(0);

            for(int var2 = 0; var2 < 8; ++var2) {
               float var3 = super.rand.nextFloat() - super.rand.nextFloat();
               float var4 = super.rand.nextFloat() - super.rand.nextFloat();
               float var5 = super.rand.nextFloat() - super.rand.nextFloat();
               super.worldObj.spawnParticle("bubble", super.posX + (double)var3, super.posY + (double)var4, super.posZ + (double)var5, super.motionX, super.motionY, super.motionZ);
            }

            this.attackEntityFrom(DamageSource.drown, 2);
         }

         this.extinguish();
      } else {
         this.setAir(300);
      }

      this.prevCameraPitch = this.cameraPitch;
      if(this.attackTime > 0) {
         --this.attackTime;
      }

      if(this.hurtTime > 0) {
         --this.hurtTime;
      }

      if(super.hurtResistantTime > 0) {
         --super.hurtResistantTime;
      }

      if(this.health <= 0) {
         this.onDeathUpdate();
      }

      if(this.recentlyHit > 0) {
         --this.recentlyHit;
      } else {
         this.attackingPlayer = null;
      }

      if(this.lastAttackingEntity != null && !this.lastAttackingEntity.isEntityAlive()) {
         this.lastAttackingEntity = null;
      }

      if(this.entityLivingToAttack != null) {
         if(!this.entityLivingToAttack.isEntityAlive()) {
            this.setRevengeTarget((EntityLiving)null);
         } else if(this.revengeTimer > 0) {
            --this.revengeTimer;
         } else {
            this.setRevengeTarget((EntityLiving)null);
         }
      }

      this.updatePotionEffects();
      this.field_70763_ax = this.field_70764_aw;
      this.prevRenderYawOffset = this.renderYawOffset;
      this.prevRotationYawHead = this.rotationYawHead;
      super.prevRotationYaw = super.rotationYaw;
      super.prevRotationPitch = super.rotationPitch;
      super.worldObj.theProfiler.endSection();
   }

   protected void onDeathUpdate() {
      ++this.deathTime;
      if(this.deathTime == 20) {
         int var1;
         if(!super.worldObj.isRemote && (this.recentlyHit > 0 || this.isPlayer()) && !this.isChild() && super.worldObj.getGameRules().getGameRuleBooleanValue("doMobLoot")) {
            var1 = this.getExperiencePoints(this.attackingPlayer);

            while(var1 > 0) {
               int var8 = EntityXPOrb.getXPSplit(var1);
               var1 -= var8;
               super.worldObj.spawnEntityInWorld(new EntityXPOrb(super.worldObj, super.posX, super.posY, super.posZ, var8));
            }
         }

         this.setDead();

         for(var1 = 0; var1 < 20; ++var1) {
            double var81 = super.rand.nextGaussian() * 0.02D;
            double var4 = super.rand.nextGaussian() * 0.02D;
            double var6 = super.rand.nextGaussian() * 0.02D;
            super.worldObj.spawnParticle("explode", super.posX + (double)(super.rand.nextFloat() * super.width * 2.0F) - (double)super.width, super.posY + (double)(super.rand.nextFloat() * super.height), super.posZ + (double)(super.rand.nextFloat() * super.width * 2.0F) - (double)super.width, var81, var4, var6);
         }
      }

   }

   protected int decreaseAirSupply(int par1) {
      int var2 = EnchantmentHelper.getRespiration(this);
      return var2 > 0 && super.rand.nextInt(var2 + 1) > 0?par1:par1 - 1;
   }

   protected int getExperiencePoints(EntityPlayer par1EntityPlayer) {
      if(this.experienceValue > 0) {
         int var2 = this.experienceValue;
         ItemStack[] var3 = this.getLastActiveItems();

         for(int var4 = 0; var4 < var3.length; ++var4) {
            if(var3[var4] != null && this.equipmentDropChances[var4] <= 1.0F) {
               var2 += 1 + super.rand.nextInt(3);
            }
         }

         return var2;
      } else {
         return this.experienceValue;
      }
   }

   protected boolean isPlayer() {
      return false;
   }

   public void spawnExplosionParticle() {
      for(int var1 = 0; var1 < 20; ++var1) {
         double var2 = super.rand.nextGaussian() * 0.02D;
         double var4 = super.rand.nextGaussian() * 0.02D;
         double var6 = super.rand.nextGaussian() * 0.02D;
         double var8 = 10.0D;
         super.worldObj.spawnParticle("explode", super.posX + (double)(super.rand.nextFloat() * super.width * 2.0F) - (double)super.width - var2 * var8, super.posY + (double)(super.rand.nextFloat() * super.height) - var4 * var8, super.posZ + (double)(super.rand.nextFloat() * super.width * 2.0F) - (double)super.width - var6 * var8, var2, var4, var6);
      }

   }

   public void updateRidden() {
      super.updateRidden();
      this.field_70768_au = this.field_70766_av;
      this.field_70766_av = 0.0F;
      super.fallDistance = 0.0F;
   }

   public void setPositionAndRotation2(double par1, double par3, double par5, float par7, float par8, int par9) {
      super.yOffset = 0.0F;
      this.newPosX = par1;
      this.newPosY = par3;
      this.newPosZ = par5;
      this.newRotationYaw = (double)par7;
      this.newRotationPitch = (double)par8;
      this.newPosRotationIncrements = par9;
   }

   public void onUpdate() {
      if(!Reflector.ForgeHooks_onLivingUpdate.exists() || !Reflector.callBoolean(Reflector.ForgeHooks_onLivingUpdate, new Object[]{this})) {
         super.onUpdate();
         if(!super.worldObj.isRemote) {
            int var12;
            for(var12 = 0; var12 < 5; ++var12) {
               ItemStack var2 = this.getCurrentItemOrArmor(var12);
               if(!ItemStack.areItemStacksEqual(var2, this.previousEquipment[var12])) {
                  ((WorldServer)super.worldObj).getEntityTracker().sendPacketToAllPlayersTrackingEntity(this, new Packet5PlayerInventory(super.entityId, var12, var2));
                  this.previousEquipment[var12] = var2 == null?null:var2.copy();
               }
            }

            var12 = this.getArrowCountInEntity();
            if(var12 > 0) {
               if(this.arrowHitTimer <= 0) {
                  this.arrowHitTimer = 20 * (30 - var12);
               }

               --this.arrowHitTimer;
               if(this.arrowHitTimer <= 0) {
                  this.setArrowCountInEntity(var12 - 1);
               }
            }
         }

         this.onLivingUpdate();
         double var121 = super.posX - super.prevPosX;
         double var3 = super.posZ - super.prevPosZ;
         float var5 = (float)(var121 * var121 + var3 * var3);
         float var6 = this.renderYawOffset;
         float var7 = 0.0F;
         this.field_70768_au = this.field_70766_av;
         float var8 = 0.0F;
         if(var5 > 0.0025000002F) {
            var8 = 1.0F;
            var7 = (float)Math.sqrt((double)var5) * 3.0F;
            var6 = (float)Math.atan2(var3, var121) * 180.0F / 3.1415927F - 90.0F;
         }

         if(this.swingProgress > 0.0F) {
            var6 = super.rotationYaw;
         }

         if(!super.onGround) {
            var8 = 0.0F;
         }

         this.field_70766_av += (var8 - this.field_70766_av) * 0.3F;
         super.worldObj.theProfiler.startSection("headTurn");
         if(this.isAIEnabled()) {
            this.bodyHelper.func_75664_a();
         } else {
            float var9 = MathHelper.wrapAngleTo180_float(var6 - this.renderYawOffset);
            this.renderYawOffset += var9 * 0.3F;
            float var10 = MathHelper.wrapAngleTo180_float(super.rotationYaw - this.renderYawOffset);
            boolean var11 = var10 < -90.0F || var10 >= 90.0F;
            if(var10 < -75.0F) {
               var10 = -75.0F;
            }

            if(var10 >= 75.0F) {
               var10 = 75.0F;
            }

            this.renderYawOffset = super.rotationYaw - var10;
            if(var10 * var10 > 2500.0F) {
               this.renderYawOffset += var10 * 0.2F;
            }

            if(var11) {
               var7 *= -1.0F;
            }
         }

         super.worldObj.theProfiler.endSection();
         super.worldObj.theProfiler.startSection("rangeChecks");

         while(super.rotationYaw - super.prevRotationYaw < -180.0F) {
            super.prevRotationYaw -= 360.0F;
         }

         while(super.rotationYaw - super.prevRotationYaw >= 180.0F) {
            super.prevRotationYaw += 360.0F;
         }

         while(this.renderYawOffset - this.prevRenderYawOffset < -180.0F) {
            this.prevRenderYawOffset -= 360.0F;
         }

         while(this.renderYawOffset - this.prevRenderYawOffset >= 180.0F) {
            this.prevRenderYawOffset += 360.0F;
         }

         while(super.rotationPitch - super.prevRotationPitch < -180.0F) {
            super.prevRotationPitch -= 360.0F;
         }

         while(super.rotationPitch - super.prevRotationPitch >= 180.0F) {
            super.prevRotationPitch += 360.0F;
         }

         while(this.rotationYawHead - this.prevRotationYawHead < -180.0F) {
            this.prevRotationYawHead -= 360.0F;
         }

         while(this.rotationYawHead - this.prevRotationYawHead >= 180.0F) {
            this.prevRotationYawHead += 360.0F;
         }

         super.worldObj.theProfiler.endSection();
         this.field_70764_aw += var7;
         if(Config.isSmoothWorld() && !Config.isMinecraftThread()) {
            Thread.yield();
         }

      }
   }

   public void heal(int par1) {
      if(this.health > 0) {
         this.setEntityHealth(this.getHealth() + par1);
         if(this.health > this.getMaxHealth()) {
            this.setEntityHealth(this.getMaxHealth());
         }

         super.hurtResistantTime = this.maxHurtResistantTime / 2;
      }

   }

   public abstract int getMaxHealth();

   public int getHealth() {
      return this.health;
   }

   public void setEntityHealth(int par1) {
      this.health = par1;
      if(par1 > this.getMaxHealth()) {
         par1 = this.getMaxHealth();
      }

   }

   public boolean attackEntityFrom(DamageSource par1DamageSource, int par2) {
      if(Reflector.ForgeHooks_onLivingAttack.exists() && Reflector.callBoolean(Reflector.ForgeHooks_onLivingAttack, new Object[]{this, par1DamageSource, Integer.valueOf(par2)})) {
         return false;
      } else if(this.isEntityInvulnerable()) {
         return false;
      } else if(super.worldObj.isRemote) {
         return false;
      } else {
         this.entityAge = 0;
         if(this.health <= 0) {
            return false;
         } else if(par1DamageSource.isFireDamage() && this.isPotionActive(Potion.fireResistance)) {
            return false;
         } else {
            if((par1DamageSource == DamageSource.anvil || par1DamageSource == DamageSource.fallingBlock) && this.getCurrentItemOrArmor(4) != null) {
               this.getCurrentItemOrArmor(4).damageItem(par2 * 4 + super.rand.nextInt(par2 * 2), this);
               par2 = (int)((float)par2 * 0.75F);
            }

            this.limbYaw = 1.5F;
            boolean var3 = true;
            if((float)super.hurtResistantTime > (float)this.maxHurtResistantTime / 2.0F) {
               if(par2 <= this.lastDamage) {
                  return false;
               }

               this.damageEntity(par1DamageSource, par2 - this.lastDamage);
               this.lastDamage = par2;
               var3 = false;
            } else {
               this.lastDamage = par2;
               this.prevHealth = this.health;
               super.hurtResistantTime = this.maxHurtResistantTime;
               this.damageEntity(par1DamageSource, par2);
               this.hurtTime = this.maxHurtTime = 10;
            }

            this.attackedAtYaw = 0.0F;
            Entity var4 = par1DamageSource.getEntity();
            if(var4 != null) {
               if(var4 instanceof EntityLiving) {
                  this.setRevengeTarget((EntityLiving)var4);
               }

               if(var4 instanceof EntityPlayer) {
                  this.recentlyHit = 100;
                  this.attackingPlayer = (EntityPlayer)var4;
               } else if(var4 instanceof EntityWolf) {
                  EntityWolf var9 = (EntityWolf)var4;
                  if(var9.isTamed()) {
                     this.recentlyHit = 100;
                     this.attackingPlayer = null;
                  }
               }
            }

            if(var3) {
               super.worldObj.setEntityState(this, (byte)2);
               if(par1DamageSource != DamageSource.drown) {
                  this.setBeenAttacked();
               }

               if(var4 != null) {
                  double var91 = var4.posX - super.posX;

                  double var7;
                  for(var7 = var4.posZ - super.posZ; var91 * var91 + var7 * var7 < 1.0E-4D; var7 = (Math.random() - Math.random()) * 0.01D) {
                     var91 = (Math.random() - Math.random()) * 0.01D;
                  }

                  this.attackedAtYaw = (float)(Math.atan2(var7, var91) * 180.0D / 3.141592653589793D) - super.rotationYaw;
                  this.knockBack(var4, par2, var91, var7);
               } else {
                  this.attackedAtYaw = (float)((int)(Math.random() * 2.0D) * 180);
               }
            }

            if(this.health <= 0) {
               if(var3) {
                  this.playSound(this.getDeathSound(), this.getSoundVolume(), this.getSoundPitch());
               }

               this.onDeath(par1DamageSource);
            } else if(var3) {
               this.playSound(this.getHurtSound(), this.getSoundVolume(), this.getSoundPitch());
            }

            return true;
         }
      }
   }

   protected float getSoundPitch() {
      return this.isChild()?(super.rand.nextFloat() - super.rand.nextFloat()) * 0.2F + 1.5F:(super.rand.nextFloat() - super.rand.nextFloat()) * 0.2F + 1.0F;
   }

   public void performHurtAnimation() {
      this.hurtTime = this.maxHurtTime = 10;
      this.attackedAtYaw = 0.0F;
   }

   public int getTotalArmorValue() {
      int var1 = 0;
      ItemStack[] var2 = this.getLastActiveItems();
      int var3 = var2.length;

      for(int var4 = 0; var4 < var3; ++var4) {
         ItemStack var5 = var2[var4];
         if(var5 != null && var5.getItem() instanceof ItemArmor) {
            int var6 = ((ItemArmor)var5.getItem()).damageReduceAmount;
            var1 += var6;
         }
      }

      return var1;
   }

   protected void damageArmor(int par1) {}

   protected int applyArmorCalculations(DamageSource par1DamageSource, int par2) {
      if(!par1DamageSource.isUnblockable()) {
         int var3 = 25 - this.getTotalArmorValue();
         int var4 = par2 * var3 + this.carryoverDamage;
         this.damageArmor(par2);
         par2 = var4 / 25;
         this.carryoverDamage = var4 % 25;
      }

      return par2;
   }

   protected int applyPotionDamageCalculations(DamageSource par1DamageSource, int par2) {
      int var3;
      int var4;
      int var5;
      if(this.isPotionActive(Potion.resistance)) {
         var3 = (this.getActivePotionEffect(Potion.resistance).getAmplifier() + 1) * 5;
         var4 = 25 - var3;
         var5 = par2 * var4 + this.carryoverDamage;
         par2 = var5 / 25;
         this.carryoverDamage = var5 % 25;
      }

      if(par2 <= 0) {
         return 0;
      } else {
         var3 = EnchantmentHelper.getEnchantmentModifierDamage(this.getLastActiveItems(), par1DamageSource);
         if(var3 > 20) {
            var3 = 20;
         }

         if(var3 > 0 && var3 <= 20) {
            var4 = 25 - var3;
            var5 = par2 * var4 + this.carryoverDamage;
            par2 = var5 / 25;
            this.carryoverDamage = var5 % 25;
         }

         return par2;
      }
   }

   protected void damageEntity(DamageSource par1DamageSource, int par2) {
      if(!this.isEntityInvulnerable()) {
         if(Reflector.ForgeHooks_onLivingHurt.exists()) {
            par2 = Reflector.callInt(Reflector.ForgeHooks_onLivingHurt, new Object[]{this, par1DamageSource, Integer.valueOf(par2)});
            if(par2 <= 0) {
               return;
            }
         }

         par2 = this.applyArmorCalculations(par1DamageSource, par2);
         par2 = this.applyPotionDamageCalculations(par1DamageSource, par2);
         int var3 = this.getHealth();
         this.health -= par2;
         this.field_94063_bt.func_94547_a(par1DamageSource, var3, par2);
      }

   }

   protected float getSoundVolume() {
      return 1.0F;
   }

   protected String getLivingSound() {
      return null;
   }

   protected String getHurtSound() {
      return "damage.hit";
   }

   protected String getDeathSound() {
      return "damage.hit";
   }

   public void knockBack(Entity par1Entity, int par2, double par3, double par5) {
      super.isAirBorne = true;
      float var7 = MathHelper.sqrt_double(par3 * par3 + par5 * par5);
      float var8 = 0.4F;
      super.motionX /= 2.0D;
      super.motionY /= 2.0D;
      super.motionZ /= 2.0D;
      super.motionX -= par3 / (double)var7 * (double)var8;
      super.motionY += (double)var8;
      super.motionZ -= par5 / (double)var7 * (double)var8;
      if(super.motionY > 0.4000000059604645D) {
         super.motionY = 0.4000000059604645D;
      }

   }

   public void onDeath(DamageSource par1DamageSource) {
      if(!Reflector.ForgeHooks_onLivingDeath.exists() || !Reflector.callBoolean(Reflector.ForgeHooks_onLivingDeath, new Object[]{this, par1DamageSource})) {
         Entity var2 = par1DamageSource.getEntity();
         EntityLiving var3 = this.func_94060_bK();
         if(this.scoreValue >= 0 && var3 != null) {
            var3.addToPlayerScore(this, this.scoreValue);
         }

         if(var2 != null) {
            var2.onKillEntity(this);
         }

         this.dead = true;
         if(!super.worldObj.isRemote) {
            int var4 = 0;
            if(var2 instanceof EntityPlayer) {
               var4 = EnchantmentHelper.getLootingModifier((EntityLiving)var2);
            }

            ArrayList listCapturedDrops = null;
            if(Reflector.ForgeEntity_captureDrops.exists()) {
               Reflector.setFieldValue(this, Reflector.ForgeEntity_captureDrops, Boolean.TRUE);
               listCapturedDrops = (ArrayList)Reflector.getFieldValue(this, Reflector.ForgeEntity_capturedDrops);
               listCapturedDrops.clear();
            }

            int var5 = 0;
            if(!this.isChild() && super.worldObj.getGameRules().getGameRuleBooleanValue("doMobLoot")) {
               this.dropFewItems(this.recentlyHit > 0, var4);
               this.dropEquipment(this.recentlyHit > 0, var4);
               if(this.recentlyHit > 0) {
                  var5 = super.rand.nextInt(200) - var4;
                  if(var5 < 5) {
                     this.dropRareDrop(var5 <= 0?1:0);
                  }
               }
            }

            if(Reflector.ForgeEntity_captureDrops.exists()) {
               Reflector.setFieldValue(this, Reflector.ForgeEntity_captureDrops, Boolean.FALSE);
               if(!Reflector.callBoolean(Reflector.ForgeHooks_onLivingDrops, new Object[]{this, par1DamageSource, listCapturedDrops, Integer.valueOf(var4), Boolean.valueOf(this.recentlyHit > 0), Integer.valueOf(var5)})) {
                  Iterator iter = listCapturedDrops.iterator();

                  while(iter.hasNext()) {
                     EntityItem item = (EntityItem)iter.next();
                     super.worldObj.spawnEntityInWorld(item);
                  }
               }
            }
         }

         super.worldObj.setEntityState(this, (byte)3);
      }
   }

   protected void dropRareDrop(int par1) {}

   protected void dropFewItems(boolean par1, int par2) {
      int var3 = this.getDropItemId();
      if(var3 > 0) {
         int var4 = super.rand.nextInt(3);
         if(par2 > 0) {
            var4 += super.rand.nextInt(par2 + 1);
         }

         for(int var5 = 0; var5 < var4; ++var5) {
            this.dropItem(var3, 1);
         }
      }

   }

   protected int getDropItemId() {
      return 0;
   }

   protected void fall(float par1) {
      if(Reflector.ForgeHooks_onLivingFall.exists()) {
         par1 = Reflector.callFloat(Reflector.ForgeHooks_onLivingFall, new Object[]{this, Float.valueOf(par1)});
         if(par1 <= 0.0F) {
            return;
         }
      }

      super.fall(par1);
      int var2 = MathHelper.ceiling_float_int(par1 - 3.0F);
      if(var2 > 0) {
         if(var2 > 4) {
            this.playSound("damage.fallbig", 1.0F, 1.0F);
         } else {
            this.playSound("damage.fallsmall", 1.0F, 1.0F);
         }

         this.attackEntityFrom(DamageSource.fall, var2);
         int var3 = super.worldObj.getBlockId(MathHelper.floor_double(super.posX), MathHelper.floor_double(super.posY - 0.20000000298023224D - (double)super.yOffset), MathHelper.floor_double(super.posZ));
         if(var3 > 0) {
            StepSound var4 = Block.blocksList[var3].stepSound;
            this.playSound(var4.getStepSound(), var4.getVolume() * 0.5F, var4.getPitch() * 0.75F);
         }
      }

   }

   public void moveEntityWithHeading(float par1, float par2) {
      double var9;
      float var11;
      if(this.isInWater() && (!(this instanceof EntityPlayer) || !((EntityPlayer)this).capabilities.isFlying)) {
         var9 = super.posY;
         this.moveFlying(par1, par2, this.isAIEnabled()?0.04F:0.02F);
         this.moveEntity(super.motionX, super.motionY, super.motionZ);
         super.motionX *= 0.800000011920929D;
         super.motionY *= 0.800000011920929D;
         super.motionZ *= 0.800000011920929D;
         super.motionY -= 0.02D;
         if(super.isCollidedHorizontally && this.isOffsetPositionInLiquid(super.motionX, super.motionY + 0.6000000238418579D - super.posY + var9, super.motionZ)) {
            super.motionY = 0.30000001192092896D;
         }
      } else if(this.handleLavaMovement() && (!(this instanceof EntityPlayer) || !((EntityPlayer)this).capabilities.isFlying)) {
         var9 = super.posY;
         this.moveFlying(par1, par2, 0.02F);
         this.moveEntity(super.motionX, super.motionY, super.motionZ);
         super.motionX *= 0.5D;
         super.motionY *= 0.5D;
         super.motionZ *= 0.5D;
         super.motionY -= 0.02D;
         if(super.isCollidedHorizontally && this.isOffsetPositionInLiquid(super.motionX, super.motionY + 0.6000000238418579D - super.posY + var9, super.motionZ)) {
            super.motionY = 0.30000001192092896D;
         }
      } else {
         float var12 = 0.91F;
         if(super.onGround) {
            var12 = 0.54600006F;
            int var8 = super.worldObj.getBlockId(MathHelper.floor_double(super.posX), MathHelper.floor_double(super.boundingBox.minY) - 1, MathHelper.floor_double(super.posZ));
            if(var8 > 0) {
               var12 = Block.blocksList[var8].slipperiness * 0.91F;
            }
         }

         float var81 = 0.16277136F / (var12 * var12 * var12);
         if(super.onGround) {
            if(this.isAIEnabled()) {
               var11 = this.getAIMoveSpeed();
            } else {
               var11 = this.landMovementFactor;
            }

            var11 *= var81;
         } else {
            var11 = this.jumpMovementFactor;
         }

         this.moveFlying(par1, par2, var11);
         var12 = 0.91F;
         if(super.onGround) {
            var12 = 0.54600006F;
            int var10 = super.worldObj.getBlockId(MathHelper.floor_double(super.posX), MathHelper.floor_double(super.boundingBox.minY) - 1, MathHelper.floor_double(super.posZ));
            if(var10 > 0) {
               var12 = Block.blocksList[var10].slipperiness * 0.91F;
            }
         }

         if(this.isOnLadder()) {
            float var101 = 0.15F;
            if(super.motionX < (double)(-var101)) {
               super.motionX = (double)(-var101);
            }

            if(super.motionX > (double)var101) {
               super.motionX = (double)var101;
            }

            if(super.motionZ < (double)(-var101)) {
               super.motionZ = (double)(-var101);
            }

            if(super.motionZ > (double)var101) {
               super.motionZ = (double)var101;
            }

            super.fallDistance = 0.0F;
            if(super.motionY < -0.15D) {
               super.motionY = -0.15D;
            }

            boolean var7 = this.isSneaking() && this instanceof EntityPlayer;
            if(var7 && super.motionY < 0.0D) {
               super.motionY = 0.0D;
            }
         }

         this.moveEntity(super.motionX, super.motionY, super.motionZ);
         if(super.isCollidedHorizontally && this.isOnLadder()) {
            super.motionY = 0.2D;
         }

         if(super.worldObj.isRemote && (!super.worldObj.blockExists((int)super.posX, 0, (int)super.posZ) || !super.worldObj.getChunkFromBlockCoords((int)super.posX, (int)super.posZ).isChunkLoaded)) {
            if(super.posY > 0.0D) {
               super.motionY = -0.1D;
            } else {
               super.motionY = 0.0D;
            }
         } else {
            super.motionY -= 0.08D;
         }

         super.motionY *= 0.9800000190734863D;
         super.motionX *= (double)var12;
         super.motionZ *= (double)var12;
      }

      this.prevLimbYaw = this.limbYaw;
      var9 = super.posX - super.prevPosX;
      double var121 = super.posZ - super.prevPosZ;
      var11 = MathHelper.sqrt_double(var9 * var9 + var121 * var121) * 4.0F;
      if(var11 > 1.0F) {
         var11 = 1.0F;
      }

      this.limbYaw += (var11 - this.limbYaw) * 0.4F;
      this.limbSwing += this.limbYaw;
   }

   public boolean isOnLadder() {
      int var1 = MathHelper.floor_double(super.posX);
      int var2 = MathHelper.floor_double(super.boundingBox.minY);
      int var3 = MathHelper.floor_double(super.posZ);
      int var4 = super.worldObj.getBlockId(var1, var2, var3);
      return Reflector.ForgeHooks_isLivingOnLadder.exists()?Reflector.callBoolean(Reflector.ForgeHooks_isLivingOnLadder, new Object[]{Block.blocksList[var4], super.worldObj, Integer.valueOf(var1), Integer.valueOf(var2), Integer.valueOf(var3)}):var4 == Block.ladder.blockID || var4 == Block.vine.blockID;
   }

   public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound) {
      if(this.health < -32768) {
         this.health = -32768;
      }

      par1NBTTagCompound.setShort("Health", (short)this.health);
      par1NBTTagCompound.setShort("HurtTime", (short)this.hurtTime);
      par1NBTTagCompound.setShort("DeathTime", (short)this.deathTime);
      par1NBTTagCompound.setShort("AttackTime", (short)this.attackTime);
      par1NBTTagCompound.setBoolean("CanPickUpLoot", this.canPickUpLoot());
      par1NBTTagCompound.setBoolean("PersistenceRequired", this.persistenceRequired);
      NBTTagList var2 = new NBTTagList();

      for(int var6 = 0; var6 < this.equipment.length; ++var6) {
         NBTTagCompound var8 = new NBTTagCompound();
         if(this.equipment[var6] != null) {
            this.equipment[var6].writeToNBT(var8);
         }

         var2.appendTag(var8);
      }

      par1NBTTagCompound.setTag("Equipment", var2);
      NBTTagList var61;
      if(!this.activePotionsMap.isEmpty()) {
         var61 = new NBTTagList();
         Iterator var7 = this.activePotionsMap.values().iterator();

         while(var7.hasNext()) {
            PotionEffect var5 = (PotionEffect)var7.next();
            var61.appendTag(var5.writeCustomPotionEffectToNBT(new NBTTagCompound()));
         }

         par1NBTTagCompound.setTag("ActiveEffects", var61);
      }

      var61 = new NBTTagList();

      for(int var81 = 0; var81 < this.equipmentDropChances.length; ++var81) {
         var61.appendTag(new NBTTagFloat(var81 + "", this.equipmentDropChances[var81]));
      }

      par1NBTTagCompound.setTag("DropChances", var61);
      par1NBTTagCompound.setString("CustomName", this.func_94057_bL());
      par1NBTTagCompound.setBoolean("CustomNameVisible", this.func_94062_bN());
      par1NBTTagCompound.setInteger("PersistentId", this.persistentId);
   }

   public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound) {
      this.health = par1NBTTagCompound.getShort("Health");
      if(!par1NBTTagCompound.hasKey("Health")) {
         this.health = this.getMaxHealth();
      }

      this.hurtTime = par1NBTTagCompound.getShort("HurtTime");
      this.deathTime = par1NBTTagCompound.getShort("DeathTime");
      this.attackTime = par1NBTTagCompound.getShort("AttackTime");
      this.setCanPickUpLoot(par1NBTTagCompound.getBoolean("CanPickUpLoot"));
      this.persistenceRequired = par1NBTTagCompound.getBoolean("PersistenceRequired");
      if(par1NBTTagCompound.hasKey("CustomName") && par1NBTTagCompound.getString("CustomName").length() > 0) {
         this.func_94058_c(par1NBTTagCompound.getString("CustomName"));
      }

      this.func_94061_f(par1NBTTagCompound.getBoolean("CustomNameVisible"));
      NBTTagList var2;
      int var3;
      if(par1NBTTagCompound.hasKey("Equipment")) {
         var2 = par1NBTTagCompound.getTagList("Equipment");

         for(var3 = 0; var3 < this.equipment.length; ++var3) {
            this.equipment[var3] = ItemStack.loadItemStackFromNBT((NBTTagCompound)var2.tagAt(var3));
         }
      }

      if(par1NBTTagCompound.hasKey("ActiveEffects")) {
         var2 = par1NBTTagCompound.getTagList("ActiveEffects");

         for(var3 = 0; var3 < var2.tagCount(); ++var3) {
            NBTTagCompound var4 = (NBTTagCompound)var2.tagAt(var3);
            PotionEffect var5 = PotionEffect.readCustomPotionEffectFromNBT(var4);
            this.activePotionsMap.put(Integer.valueOf(var5.getPotionID()), var5);
         }
      }

      if(par1NBTTagCompound.hasKey("DropChances")) {
         var2 = par1NBTTagCompound.getTagList("DropChances");

         for(var3 = 0; var3 < var2.tagCount(); ++var3) {
            this.equipmentDropChances[var3] = ((NBTTagFloat)var2.tagAt(var3)).data;
         }
      }

      this.persistentId = par1NBTTagCompound.getInteger("PersistentId");
      if(this.persistentId == 0) {
         this.persistentId = super.rand.nextInt(Integer.MAX_VALUE);
      }

   }

   public boolean isEntityAlive() {
      return !super.isDead && this.health > 0;
   }

   public boolean canBreatheUnderwater() {
      return false;
   }

   public void setMoveForward(float par1) {
      this.moveForward = par1;
   }

   public void setJumping(boolean par1) {
      this.isJumping = par1;
   }

   public void onLivingUpdate() {
      if(this.jumpTicks > 0) {
         --this.jumpTicks;
      }

      if(this.newPosRotationIncrements > 0) {
         double var11 = super.posX + (this.newPosX - super.posX) / (double)this.newPosRotationIncrements;
         double var12 = super.posY + (this.newPosY - super.posY) / (double)this.newPosRotationIncrements;
         double var13 = super.posZ + (this.newPosZ - super.posZ) / (double)this.newPosRotationIncrements;
         double var14 = MathHelper.wrapAngleTo180_double(this.newRotationYaw - (double)super.rotationYaw);
         super.rotationYaw = (float)((double)super.rotationYaw + var14 / (double)this.newPosRotationIncrements);
         super.rotationPitch = (float)((double)super.rotationPitch + (this.newRotationPitch - (double)super.rotationPitch) / (double)this.newPosRotationIncrements);
         --this.newPosRotationIncrements;
         this.setPosition(var11, var12, var13);
         this.setRotation(super.rotationYaw, super.rotationPitch);
      } else if(!this.isClientWorld()) {
         super.motionX *= 0.98D;
         super.motionY *= 0.98D;
         super.motionZ *= 0.98D;
      }

      if(Math.abs(super.motionX) < 0.005D) {
         super.motionX = 0.0D;
      }

      if(Math.abs(super.motionY) < 0.005D) {
         super.motionY = 0.0D;
      }

      if(Math.abs(super.motionZ) < 0.005D) {
         super.motionZ = 0.0D;
      }

      super.worldObj.theProfiler.startSection("ai");
      if(this.isMovementBlocked()) {
         this.isJumping = false;
         this.moveStrafing = 0.0F;
         this.moveForward = 0.0F;
         this.randomYawVelocity = 0.0F;
      } else if(this.isClientWorld()) {
         if(this.isAIEnabled()) {
            super.worldObj.theProfiler.startSection("newAi");
            this.updateAITasks();
            super.worldObj.theProfiler.endSection();
         } else {
            super.worldObj.theProfiler.startSection("oldAi");
            this.updateEntityActionState();
            super.worldObj.theProfiler.endSection();
            this.rotationYawHead = super.rotationYaw;
         }
      }

      super.worldObj.theProfiler.endSection();
      super.worldObj.theProfiler.startSection("jump");
      if(this.isJumping) {
         if(!this.isInWater() && !this.handleLavaMovement()) {
            if(super.onGround && this.jumpTicks == 0) {
               this.jump();
               this.jumpTicks = 10;
            }
         } else {
            super.motionY += 0.03999999910593033D;
         }
      } else {
         this.jumpTicks = 0;
      }

      super.worldObj.theProfiler.endSection();
      super.worldObj.theProfiler.startSection("travel");
      this.moveStrafing *= 0.98F;
      this.moveForward *= 0.98F;
      this.randomYawVelocity *= 0.9F;
      float var111 = this.landMovementFactor;
      this.landMovementFactor *= this.getSpeedModifier();
      this.moveEntityWithHeading(this.moveStrafing, this.moveForward);
      this.landMovementFactor = var111;
      super.worldObj.theProfiler.endSection();
      super.worldObj.theProfiler.startSection("push");
      if(!super.worldObj.isRemote) {
         this.func_85033_bc();
      }

      super.worldObj.theProfiler.endSection();
      super.worldObj.theProfiler.startSection("looting");
      if(!super.worldObj.isRemote && this.canPickUpLoot() && !this.dead && super.worldObj.getGameRules().getGameRuleBooleanValue("mobGriefing")) {
         List var2 = super.worldObj.getEntitiesWithinAABB(EntityItem.class, super.boundingBox.expand(1.0D, 0.0D, 1.0D));
         Iterator var121 = var2.iterator();

         while(var121.hasNext()) {
            EntityItem var4 = (EntityItem)var121.next();
            if(!var4.isDead && var4.getEntityItem() != null) {
               ItemStack var131 = var4.getEntityItem();
               int var6 = getArmorPosition(var131);
               if(var6 > -1) {
                  boolean var141 = true;
                  ItemStack var8 = this.getCurrentItemOrArmor(var6);
                  if(var8 != null) {
                     if(var6 == 0) {
                        if(var131.getItem() instanceof ItemSword && !(var8.getItem() instanceof ItemSword)) {
                           var141 = true;
                        } else if(var131.getItem() instanceof ItemSword && var8.getItem() instanceof ItemSword) {
                           ItemSword var15 = (ItemSword)var131.getItem();
                           ItemSword var16 = (ItemSword)var8.getItem();
                           if(var15.func_82803_g() == var16.func_82803_g()) {
                              var141 = var131.getItemDamage() > var8.getItemDamage() || var131.hasTagCompound() && !var8.hasTagCompound();
                           } else {
                              var141 = var15.func_82803_g() > var16.func_82803_g();
                           }
                        } else {
                           var141 = false;
                        }
                     } else if(var131.getItem() instanceof ItemArmor && !(var8.getItem() instanceof ItemArmor)) {
                        var141 = true;
                     } else if(var131.getItem() instanceof ItemArmor && var8.getItem() instanceof ItemArmor) {
                        ItemArmor var151 = (ItemArmor)var131.getItem();
                        ItemArmor var161 = (ItemArmor)var8.getItem();
                        if(var151.damageReduceAmount == var161.damageReduceAmount) {
                           var141 = var131.getItemDamage() > var8.getItemDamage() || var131.hasTagCompound() && !var8.hasTagCompound();
                        } else {
                           var141 = var151.damageReduceAmount > var161.damageReduceAmount;
                        }
                     } else {
                        var141 = false;
                     }
                  }

                  if(var141) {
                     if(var8 != null && super.rand.nextFloat() - 0.1F < this.equipmentDropChances[var6]) {
                        this.entityDropItem(var8, 0.0F);
                     }

                     this.setCurrentItemOrArmor(var6, var131);
                     this.equipmentDropChances[var6] = 2.0F;
                     this.persistenceRequired = true;
                     this.onItemPickup(var4, 1);
                     var4.setDead();
                  }
               }
            }
         }
      }

      super.worldObj.theProfiler.endSection();
   }

   protected void func_85033_bc() {
      List var1 = super.worldObj.getEntitiesWithinAABBExcludingEntity(this, super.boundingBox.expand(0.20000000298023224D, 0.0D, 0.20000000298023224D));
      if(var1 != null && !var1.isEmpty()) {
         for(int var2 = 0; var2 < var1.size(); ++var2) {
            Entity var3 = (Entity)var1.get(var2);
            if(var3.canBePushed()) {
               this.collideWithEntity(var3);
            }
         }
      }

   }

   protected void collideWithEntity(Entity par1Entity) {
      par1Entity.applyEntityCollision(this);
   }

   protected boolean isAIEnabled() {
      return false;
   }

   protected boolean isClientWorld() {
      return !super.worldObj.isRemote;
   }

   protected boolean isMovementBlocked() {
      return this.health <= 0;
   }

   public boolean isBlocking() {
      return false;
   }

   protected void jump() {
      super.motionY = 0.41999998688697815D;
      if(this.isPotionActive(Potion.jump)) {
         super.motionY += (double)((float)(this.getActivePotionEffect(Potion.jump).getAmplifier() + 1) * 0.1F);
      }

      if(this.isSprinting()) {
         float var1 = super.rotationYaw * 0.017453292F;
         super.motionX -= (double)(MathHelper.sin(var1) * 0.2F);
         super.motionZ += (double)(MathHelper.cos(var1) * 0.2F);
      }

      super.isAirBorne = true;
      if(Reflector.ForgeHooks_onLivingJump.exists()) {
         Reflector.callVoid(Reflector.ForgeHooks_onLivingJump, new Object[]{this});
      }

   }

   protected boolean canDespawn() {
      return true;
   }

   protected void despawnEntity() {
      if(!this.persistenceRequired) {
         EntityPlayer var1 = super.worldObj.getClosestPlayerToEntity(this, -1.0D);
         if(var1 != null) {
            double var2 = var1.posX - super.posX;
            double var4 = var1.posY - super.posY;
            double var6 = var1.posZ - super.posZ;
            double var8 = var2 * var2 + var4 * var4 + var6 * var6;
            if(this.canDespawn() && var8 > 16384.0D) {
               this.setDead();
            }

            if(this.entityAge > 600 && super.rand.nextInt(800) == 0 && var8 > 1024.0D && this.canDespawn()) {
               this.setDead();
            } else if(var8 < 1024.0D) {
               this.entityAge = 0;
            }
         }
      }

   }

   protected void updateAITasks() {
      ++this.entityAge;
      super.worldObj.theProfiler.startSection("checkDespawn");
      this.despawnEntity();
      super.worldObj.theProfiler.endSection();
      super.worldObj.theProfiler.startSection("sensing");
      this.senses.clearSensingCache();
      super.worldObj.theProfiler.endSection();
      super.worldObj.theProfiler.startSection("targetSelector");
      this.targetTasks.onUpdateTasks();
      super.worldObj.theProfiler.endSection();
      super.worldObj.theProfiler.startSection("goalSelector");
      this.tasks.onUpdateTasks();
      super.worldObj.theProfiler.endSection();
      super.worldObj.theProfiler.startSection("navigation");
      this.navigator.onUpdateNavigation();
      super.worldObj.theProfiler.endSection();
      super.worldObj.theProfiler.startSection("mob tick");
      this.updateAITick();
      super.worldObj.theProfiler.endSection();
      super.worldObj.theProfiler.startSection("controls");
      super.worldObj.theProfiler.startSection("move");
      this.moveHelper.onUpdateMoveHelper();
      super.worldObj.theProfiler.endStartSection("look");
      this.lookHelper.onUpdateLook();
      super.worldObj.theProfiler.endStartSection("jump");
      this.jumpHelper.doJump();
      super.worldObj.theProfiler.endSection();
      super.worldObj.theProfiler.endSection();
   }

   protected void updateAITick() {}

   protected void updateEntityActionState() {
      ++this.entityAge;
      this.despawnEntity();
      this.moveStrafing = 0.0F;
      this.moveForward = 0.0F;
      float var1 = 8.0F;
      if(super.rand.nextFloat() < 0.02F) {
         EntityPlayer var4 = super.worldObj.getClosestPlayerToEntity(this, (double)var1);
         if(var4 != null) {
            this.currentTarget = var4;
            this.numTicksToChaseTarget = 10 + super.rand.nextInt(20);
         } else {
            this.randomYawVelocity = (super.rand.nextFloat() - 0.5F) * 20.0F;
         }
      }

      if(this.currentTarget != null) {
         this.faceEntity(this.currentTarget, 10.0F, (float)this.getVerticalFaceSpeed());
         if(this.numTicksToChaseTarget-- <= 0 || this.currentTarget.isDead || this.currentTarget.getDistanceSqToEntity(this) > (double)(var1 * var1)) {
            this.currentTarget = null;
         }
      } else {
         if(super.rand.nextFloat() < 0.05F) {
            this.randomYawVelocity = (super.rand.nextFloat() - 0.5F) * 20.0F;
         }

         super.rotationYaw += this.randomYawVelocity;
         super.rotationPitch = this.defaultPitch;
      }

      boolean var41 = this.isInWater();
      boolean var3 = this.handleLavaMovement();
      if(var41 || var3) {
         this.isJumping = super.rand.nextFloat() < 0.8F;
      }

   }

   protected void updateArmSwingProgress() {
      int var1 = this.getArmSwingAnimationEnd();
      if(this.isSwingInProgress) {
         ++this.swingProgressInt;
         if(this.swingProgressInt >= var1) {
            this.swingProgressInt = 0;
            this.isSwingInProgress = false;
         }
      } else {
         this.swingProgressInt = 0;
      }

      this.swingProgress = (float)this.swingProgressInt / (float)var1;
   }

   public int getVerticalFaceSpeed() {
      return 40;
   }

   public void faceEntity(Entity par1Entity, float par2, float par3) {
      double var4 = par1Entity.posX - super.posX;
      double var8 = par1Entity.posZ - super.posZ;
      double var6;
      if(par1Entity instanceof EntityLiving) {
         EntityLiving var14 = (EntityLiving)par1Entity;
         var6 = var14.posY + (double)var14.getEyeHeight() - (super.posY + (double)this.getEyeHeight());
      } else {
         var6 = (par1Entity.boundingBox.minY + par1Entity.boundingBox.maxY) / 2.0D - (super.posY + (double)this.getEyeHeight());
      }

      double var141 = (double)MathHelper.sqrt_double(var4 * var4 + var8 * var8);
      float var12 = (float)(Math.atan2(var8, var4) * 180.0D / 3.141592653589793D) - 90.0F;
      float var13 = (float)(-(Math.atan2(var6, var141) * 180.0D / 3.141592653589793D));
      super.rotationPitch = this.updateRotation(super.rotationPitch, var13, par3);
      super.rotationYaw = this.updateRotation(super.rotationYaw, var12, par2);
   }

   private float updateRotation(float par1, float par2, float par3) {
      float var4 = MathHelper.wrapAngleTo180_float(par2 - par1);
      if(var4 > par3) {
         var4 = par3;
      }

      if(var4 < -par3) {
         var4 = -par3;
      }

      return par1 + var4;
   }

   public boolean getCanSpawnHere() {
      return super.worldObj.checkNoEntityCollision(super.boundingBox) && super.worldObj.getCollidingBoundingBoxes(this, super.boundingBox).isEmpty() && !super.worldObj.isAnyLiquid(super.boundingBox);
   }

   protected void kill() {
      this.attackEntityFrom(DamageSource.outOfWorld, 4);
   }

   public float getSwingProgress(float par1) {
      float var2 = this.swingProgress - this.prevSwingProgress;
      if(var2 < 0.0F) {
         ++var2;
      }

      return this.prevSwingProgress + var2 * par1;
   }

   public Vec3 getPosition(float par1) {
      if(par1 == 1.0F) {
         return super.worldObj.getWorldVec3Pool().getVecFromPool(super.posX, super.posY, super.posZ);
      } else {
         double var2 = super.prevPosX + (super.posX - super.prevPosX) * (double)par1;
         double var4 = super.prevPosY + (super.posY - super.prevPosY) * (double)par1;
         double var6 = super.prevPosZ + (super.posZ - super.prevPosZ) * (double)par1;
         return super.worldObj.getWorldVec3Pool().getVecFromPool(var2, var4, var6);
      }
   }

   public Vec3 getLookVec() {
      return this.getLook(1.0F);
   }

   public Vec3 getLook(float par1) {
      float var2;
      float var3;
      float var4;
      float var5;
      if(par1 == 1.0F) {
         var2 = MathHelper.cos(-super.rotationYaw * 0.017453292F - 3.1415927F);
         var3 = MathHelper.sin(-super.rotationYaw * 0.017453292F - 3.1415927F);
         var4 = -MathHelper.cos(-super.rotationPitch * 0.017453292F);
         var5 = MathHelper.sin(-super.rotationPitch * 0.017453292F);
         return super.worldObj.getWorldVec3Pool().getVecFromPool((double)(var3 * var4), (double)var5, (double)(var2 * var4));
      } else {
         var2 = super.prevRotationPitch + (super.rotationPitch - super.prevRotationPitch) * par1;
         var3 = super.prevRotationYaw + (super.rotationYaw - super.prevRotationYaw) * par1;
         var4 = MathHelper.cos(-var3 * 0.017453292F - 3.1415927F);
         var5 = MathHelper.sin(-var3 * 0.017453292F - 3.1415927F);
         float var6 = -MathHelper.cos(-var2 * 0.017453292F);
         float var7 = MathHelper.sin(-var2 * 0.017453292F);
         return super.worldObj.getWorldVec3Pool().getVecFromPool((double)(var5 * var6), (double)var7, (double)(var4 * var6));
      }
   }

   public float getRenderSizeModifier() {
      return 1.0F;
   }

   public MovingObjectPosition rayTrace(double par1, float par3) {
      Vec3 var4 = this.getPosition(par3);
      Vec3 var5 = this.getLook(par3);
      Vec3 var6 = var4.addVector(var5.xCoord * par1, var5.yCoord * par1, var5.zCoord * par1);
      return super.worldObj.rayTraceBlocks(var4, var6);
   }

   public int getMaxSpawnedInChunk() {
      return 4;
   }

   public void handleHealthUpdate(byte par1) {
      if(par1 == 2) {
         this.limbYaw = 1.5F;
         super.hurtResistantTime = this.maxHurtResistantTime;
         this.hurtTime = this.maxHurtTime = 10;
         this.attackedAtYaw = 0.0F;
         this.playSound(this.getHurtSound(), this.getSoundVolume(), (super.rand.nextFloat() - super.rand.nextFloat()) * 0.2F + 1.0F);
         this.attackEntityFrom(DamageSource.generic, 0);
      } else if(par1 == 3) {
         this.playSound(this.getDeathSound(), this.getSoundVolume(), (super.rand.nextFloat() - super.rand.nextFloat()) * 0.2F + 1.0F);
         this.health = 0;
         this.onDeath(DamageSource.generic);
      } else {
         super.handleHealthUpdate(par1);
      }

   }

   public boolean isPlayerSleeping() {
      return false;
   }

   public Icon getItemIcon(ItemStack par1ItemStack, int par2) {
      return par1ItemStack.getIconIndex();
   }

   protected void updatePotionEffects() {
      Iterator var1 = this.activePotionsMap.keySet().iterator();

      while(var1.hasNext()) {
         Integer var12 = (Integer)var1.next();
         PotionEffect var13 = (PotionEffect)this.activePotionsMap.get(var12);

         try {
            if(!var13.onUpdate(this)) {
               if(!super.worldObj.isRemote) {
                  var1.remove();
                  this.onFinishedPotionEffect(var13);
               }
            } else if(var13.getDuration() % 600 == 0) {
               this.onChangedPotionEffect(var13);
            }
         } catch (Throwable var11) {
            CrashReport var14 = CrashReport.makeCrashReport(var11, "Ticking mob effect instance");
            CrashReportCategory var6 = var14.makeCategory("Mob effect being ticked");
            var6.addCrashSectionCallable("Effect Name", new CallableEffectName(this, var13));
            var6.addCrashSectionCallable("Effect ID", new CallableEffectID(this, var13));
            var6.addCrashSectionCallable("Effect Duration", new CallableEffectDuration(this, var13));
            var6.addCrashSectionCallable("Effect Amplifier", new CallableEffectAmplifier(this, var13));
            var6.addCrashSectionCallable("Effect is Splash", new CallableEffectIsSplash(this, var13));
            var6.addCrashSectionCallable("Effect is Ambient", new CallableEffectIsAmbient(this, var13));
            throw new ReportedException(var14);
         }
      }

      int var121;
      if(this.potionsNeedUpdate) {
         if(!super.worldObj.isRemote) {
            if(this.activePotionsMap.isEmpty()) {
               super.dataWatcher.updateObject(9, Byte.valueOf((byte)0));
               super.dataWatcher.updateObject(8, Integer.valueOf(0));
               this.setInvisible(false);
            } else {
               var121 = PotionHelper.calcPotionLiquidColor(this.activePotionsMap.values());
               super.dataWatcher.updateObject(9, Byte.valueOf((byte)(PotionHelper.func_82817_b(this.activePotionsMap.values())?1:0)));
               super.dataWatcher.updateObject(8, Integer.valueOf(var121));
               this.setInvisible(this.isPotionActive(Potion.invisibility.id));
            }
         }

         this.potionsNeedUpdate = false;
      }

      var121 = super.dataWatcher.getWatchableObjectInt(8);
      boolean var131 = super.dataWatcher.getWatchableObjectByte(9) > 0;
      if(var121 > 0) {
         boolean var4 = false;
         if(!this.isInvisible()) {
            var4 = super.rand.nextBoolean();
         } else {
            var4 = super.rand.nextInt(15) == 0;
         }

         if(var131) {
            var4 &= super.rand.nextInt(5) == 0;
         }

         if(var4 && var121 > 0) {
            double var141 = (double)(var121 >> 16 & 255) / 255.0D;
            double var7 = (double)(var121 >> 8 & 255) / 255.0D;
            double var9 = (double)(var121 >> 0 & 255) / 255.0D;
            super.worldObj.spawnParticle(var131?"mobSpellAmbient":"mobSpell", super.posX + (super.rand.nextDouble() - 0.5D) * (double)super.width, super.posY + super.rand.nextDouble() * (double)super.height - (double)super.yOffset, super.posZ + (super.rand.nextDouble() - 0.5D) * (double)super.width, var141, var7, var9);
         }
      }

   }

   public void clearActivePotions() {
      Iterator var1 = this.activePotionsMap.keySet().iterator();

      while(var1.hasNext()) {
         Integer var2 = (Integer)var1.next();
         PotionEffect var3 = (PotionEffect)this.activePotionsMap.get(var2);
         if(!super.worldObj.isRemote) {
            var1.remove();
            this.onFinishedPotionEffect(var3);
         }
      }

   }

   public Collection getActivePotionEffects() {
      return this.activePotionsMap.values();
   }

   public boolean isPotionActive(int par1) {
      return this.activePotionsMap.containsKey(Integer.valueOf(par1));
   }

   public boolean isPotionActive(Potion par1Potion) {
      return this.activePotionsMap.containsKey(Integer.valueOf(par1Potion.id));
   }

   public PotionEffect getActivePotionEffect(Potion par1Potion) {
      return (PotionEffect)this.activePotionsMap.get(Integer.valueOf(par1Potion.id));
   }

   public void addPotionEffect(PotionEffect par1PotionEffect) {
      if(this.isPotionApplicable(par1PotionEffect)) {
         if(this.activePotionsMap.containsKey(Integer.valueOf(par1PotionEffect.getPotionID()))) {
            ((PotionEffect)this.activePotionsMap.get(Integer.valueOf(par1PotionEffect.getPotionID()))).combine(par1PotionEffect);
            this.onChangedPotionEffect((PotionEffect)this.activePotionsMap.get(Integer.valueOf(par1PotionEffect.getPotionID())));
         } else {
            this.activePotionsMap.put(Integer.valueOf(par1PotionEffect.getPotionID()), par1PotionEffect);
            this.onNewPotionEffect(par1PotionEffect);
         }
      }

   }

   public boolean isPotionApplicable(PotionEffect par1PotionEffect) {
      if(this.getCreatureAttribute() == EnumCreatureAttribute.UNDEAD) {
         int var2 = par1PotionEffect.getPotionID();
         if(var2 == Potion.regeneration.id || var2 == Potion.poison.id) {
            return false;
         }
      }

      return true;
   }

   public boolean isEntityUndead() {
      return this.getCreatureAttribute() == EnumCreatureAttribute.UNDEAD;
   }

   public void removePotionEffectClient(int par1) {
      this.activePotionsMap.remove(Integer.valueOf(par1));
   }

   public void removePotionEffect(int par1) {
      PotionEffect var2 = (PotionEffect)this.activePotionsMap.remove(Integer.valueOf(par1));
      if(var2 != null) {
         this.onFinishedPotionEffect(var2);
      }

   }

   protected void onNewPotionEffect(PotionEffect par1PotionEffect) {
      this.potionsNeedUpdate = true;
   }

   protected void onChangedPotionEffect(PotionEffect par1PotionEffect) {
      this.potionsNeedUpdate = true;
   }

   protected void onFinishedPotionEffect(PotionEffect par1PotionEffect) {
      this.potionsNeedUpdate = true;
   }

   public float getSpeedModifier() {
      float var1 = 1.0F;
      if(this.isPotionActive(Potion.moveSpeed)) {
         var1 *= 1.0F + 0.2F * (float)(this.getActivePotionEffect(Potion.moveSpeed).getAmplifier() + 1);
      }

      if(this.isPotionActive(Potion.moveSlowdown)) {
         var1 *= 1.0F - 0.15F * (float)(this.getActivePotionEffect(Potion.moveSlowdown).getAmplifier() + 1);
      }

      if(var1 < 0.0F) {
         var1 = 0.0F;
      }

      return var1;
   }

   public void setPositionAndUpdate(double par1, double par3, double par5) {
      this.setLocationAndAngles(par1, par3, par5, super.rotationYaw, super.rotationPitch);
   }

   public boolean isChild() {
      return false;
   }

   public EnumCreatureAttribute getCreatureAttribute() {
      return EnumCreatureAttribute.UNDEFINED;
   }

   public void renderBrokenItemStack(ItemStack par1ItemStack) {
      this.playSound("random.break", 0.8F, 0.8F + super.worldObj.rand.nextFloat() * 0.4F);

      for(int var2 = 0; var2 < 5; ++var2) {
         Vec3 var3 = super.worldObj.getWorldVec3Pool().getVecFromPool(((double)super.rand.nextFloat() - 0.5D) * 0.1D, Math.random() * 0.1D + 0.1D, 0.0D);
         var3.rotateAroundX(-super.rotationPitch * 3.1415927F / 180.0F);
         var3.rotateAroundY(-super.rotationYaw * 3.1415927F / 180.0F);
         Vec3 var4 = super.worldObj.getWorldVec3Pool().getVecFromPool(((double)super.rand.nextFloat() - 0.5D) * 0.3D, (double)(-super.rand.nextFloat()) * 0.6D - 0.3D, 0.6D);
         var4.rotateAroundX(-super.rotationPitch * 3.1415927F / 180.0F);
         var4.rotateAroundY(-super.rotationYaw * 3.1415927F / 180.0F);
         var4 = var4.addVector(super.posX, super.posY + (double)this.getEyeHeight(), super.posZ);
         super.worldObj.spawnParticle("iconcrack_" + par1ItemStack.getItem().itemID, var4.xCoord, var4.yCoord, var4.zCoord, var3.xCoord, var3.yCoord + 0.05D, var3.zCoord);
      }

   }

   public void curePotionEffects(ItemStack curativeItem) {
      Iterator potionKey = this.activePotionsMap.keySet().iterator();
      if(!super.worldObj.isRemote) {
         while(potionKey.hasNext()) {
            Integer key = (Integer)potionKey.next();
            PotionEffect effect = (PotionEffect)this.activePotionsMap.get(key);
            if(Reflector.callBoolean(effect, Reflector.ForgePotionEffect_isCurativeItem, new Object[]{curativeItem})) {
               potionKey.remove();
               this.onFinishedPotionEffect(effect);
            }
         }

      }
   }

   public boolean shouldRiderFaceForward(EntityPlayer player) {
      return this instanceof EntityPig;
   }

   public int func_82143_as() {
      if(this.getAttackTarget() == null) {
         return 3;
      } else {
         int var1 = (int)((float)this.health - (float)this.getMaxHealth() * 0.33F);
         var1 -= (3 - super.worldObj.difficultySetting) * 4;
         if(var1 < 0) {
            var1 = 0;
         }

         return var1 + 3;
      }
   }

   public ItemStack getHeldItem() {
      return this.equipment[0];
   }

   public ItemStack getCurrentItemOrArmor(int par1) {
      return this.equipment[par1];
   }

   public ItemStack getCurrentArmor(int par1) {
      return this.equipment[par1 + 1];
   }

   public void setCurrentItemOrArmor(int par1, ItemStack par2ItemStack) {
      this.equipment[par1] = par2ItemStack;
   }

   public ItemStack[] getLastActiveItems() {
      return this.equipment;
   }

   protected void dropEquipment(boolean par1, int par2) {
      for(int var3 = 0; var3 < this.getLastActiveItems().length; ++var3) {
         ItemStack var4 = this.getCurrentItemOrArmor(var3);
         boolean var5 = this.equipmentDropChances[var3] > 1.0F;
         if(var4 != null && (par1 || var5) && super.rand.nextFloat() - (float)par2 * 0.01F < this.equipmentDropChances[var3]) {
            if(!var5 && var4.isItemStackDamageable()) {
               int var6 = Math.max(var4.getMaxDamage() - 25, 1);
               int var7 = var4.getMaxDamage() - super.rand.nextInt(super.rand.nextInt(var6) + 1);
               if(var7 > var6) {
                  var7 = var6;
               }

               if(var7 < 1) {
                  var7 = 1;
               }

               var4.setItemDamage(var7);
            }

            this.entityDropItem(var4, 0.0F);
         }
      }

   }

   protected void addRandomArmor() {
      if(super.rand.nextFloat() < armorProbability[super.worldObj.difficultySetting]) {
         int var1 = super.rand.nextInt(2);
         float var2 = super.worldObj.difficultySetting == 3?0.1F:0.25F;
         if(super.rand.nextFloat() < 0.095F) {
            ++var1;
         }

         if(super.rand.nextFloat() < 0.095F) {
            ++var1;
         }

         if(super.rand.nextFloat() < 0.095F) {
            ++var1;
         }

         for(int var3 = 3; var3 >= 0; --var3) {
            ItemStack var4 = this.getCurrentArmor(var3);
            if(var3 < 3 && super.rand.nextFloat() < var2) {
               break;
            }

            if(var4 == null) {
               Item var5 = getArmorItemForSlot(var3 + 1, var1);
               if(var5 != null) {
                  this.setCurrentItemOrArmor(var3 + 1, new ItemStack(var5));
               }
            }
         }
      }

   }

   public void onItemPickup(Entity par1Entity, int par2) {
      if(!par1Entity.isDead && !super.worldObj.isRemote) {
         EntityTracker var3 = ((WorldServer)super.worldObj).getEntityTracker();
         if(par1Entity instanceof EntityItem) {
            var3.sendPacketToAllPlayersTrackingEntity(par1Entity, new Packet22Collect(par1Entity.entityId, super.entityId));
         }

         if(par1Entity instanceof EntityArrow) {
            var3.sendPacketToAllPlayersTrackingEntity(par1Entity, new Packet22Collect(par1Entity.entityId, super.entityId));
         }

         if(par1Entity instanceof EntityXPOrb) {
            var3.sendPacketToAllPlayersTrackingEntity(par1Entity, new Packet22Collect(par1Entity.entityId, super.entityId));
         }
      }

   }

   public static int getArmorPosition(ItemStack par0ItemStack) {
      if(par0ItemStack.itemID != Block.pumpkin.blockID && par0ItemStack.itemID != Item.skull.itemID) {
         if(par0ItemStack.getItem() instanceof ItemArmor) {
            switch(((ItemArmor)par0ItemStack.getItem()).armorType) {
            case 0:
               return 4;
            case 1:
               return 3;
            case 2:
               return 2;
            case 3:
               return 1;
            }
         }

         return 0;
      } else {
         return 4;
      }
   }

   public static Item getArmorItemForSlot(int par0, int par1) {
      switch(par0) {
      case 4:
         if(par1 == 0) {
            return Item.helmetLeather;
         } else if(par1 == 1) {
            return Item.helmetGold;
         } else if(par1 == 2) {
            return Item.helmetChain;
         } else if(par1 == 3) {
            return Item.helmetIron;
         } else if(par1 == 4) {
            return Item.helmetDiamond;
         }
      case 3:
         if(par1 == 0) {
            return Item.plateLeather;
         } else if(par1 == 1) {
            return Item.plateGold;
         } else if(par1 == 2) {
            return Item.plateChain;
         } else if(par1 == 3) {
            return Item.plateIron;
         } else if(par1 == 4) {
            return Item.plateDiamond;
         }
      case 2:
         if(par1 == 0) {
            return Item.legsLeather;
         } else if(par1 == 1) {
            return Item.legsGold;
         } else if(par1 == 2) {
            return Item.legsChain;
         } else if(par1 == 3) {
            return Item.legsIron;
         } else if(par1 == 4) {
            return Item.legsDiamond;
         }
      case 1:
         if(par1 == 0) {
            return Item.bootsLeather;
         } else if(par1 == 1) {
            return Item.bootsGold;
         } else if(par1 == 2) {
            return Item.bootsChain;
         } else if(par1 == 3) {
            return Item.bootsIron;
         } else if(par1 == 4) {
            return Item.bootsDiamond;
         }
      default:
         return null;
      }
   }

   protected void func_82162_bC() {
      if(this.getHeldItem() != null && super.rand.nextFloat() < enchantmentProbability[super.worldObj.difficultySetting]) {
         EnchantmentHelper.addRandomEnchantment(super.rand, this.getHeldItem(), 5 + super.worldObj.difficultySetting * super.rand.nextInt(6));
      }

      for(int var1 = 0; var1 < 4; ++var1) {
         ItemStack var2 = this.getCurrentArmor(var1);
         if(var2 != null && super.rand.nextFloat() < armorEnchantmentProbability[super.worldObj.difficultySetting]) {
            EnchantmentHelper.addRandomEnchantment(super.rand, var2, 5 + super.worldObj.difficultySetting * super.rand.nextInt(6));
         }
      }

   }

   public void initCreature() {}

   private int getArmSwingAnimationEnd() {
      return this.isPotionActive(Potion.digSpeed)?6 - (1 + this.getActivePotionEffect(Potion.digSpeed).getAmplifier()) * 1:(this.isPotionActive(Potion.digSlowdown)?6 + (1 + this.getActivePotionEffect(Potion.digSlowdown).getAmplifier()) * 2:6);
   }

   public void swingItem() {
      ItemStack stack = this.getHeldItem();
      if(stack != null && stack.getItem() != null) {
         Item item = stack.getItem();
         if(Reflector.callBoolean(item, Reflector.ForgeItem_onEntitySwing, new Object[]{this, stack})) {
            return;
         }
      }

      if(!this.isSwingInProgress || this.swingProgressInt >= this.getArmSwingAnimationEnd() / 2 || this.swingProgressInt < 0) {
         this.swingProgressInt = -1;
         this.isSwingInProgress = true;
         if(super.worldObj instanceof WorldServer) {
            ((WorldServer)super.worldObj).getEntityTracker().sendPacketToAllPlayersTrackingEntity(this, new Packet18Animation(this, 1));
         }
      }

   }

   public boolean canBeSteered() {
      return false;
   }

   public final int getArrowCountInEntity() {
      return super.dataWatcher.getWatchableObjectByte(10);
   }

   public final void setArrowCountInEntity(int par1) {
      super.dataWatcher.updateObject(10, Byte.valueOf((byte)par1));
   }

   public EntityLiving func_94060_bK() {
      return (EntityLiving)(this.field_94063_bt.func_94550_c() != null?this.field_94063_bt.func_94550_c():(this.attackingPlayer != null?this.attackingPlayer:(this.entityLivingToAttack != null?this.entityLivingToAttack:null)));
   }

   public String getEntityName() {
      return this.func_94056_bM()?this.func_94057_bL():super.getEntityName();
   }

   public void func_94058_c(String par1Str) {
      super.dataWatcher.updateObject(5, par1Str);
   }

   public String func_94057_bL() {
      return super.dataWatcher.getWatchableObjectString(5);
   }

   public boolean func_94056_bM() {
      return super.dataWatcher.getWatchableObjectString(5).length() > 0;
   }

   public void func_94061_f(boolean par1) {
      super.dataWatcher.updateObject(6, Byte.valueOf((byte)(par1?1:0)));
   }

   public boolean func_94062_bN() {
      return super.dataWatcher.getWatchableObjectByte(6) == 1;
   }

   public boolean func_94059_bO() {
      return this.func_94062_bN();
   }

   public void func_96120_a(int par1, float par2) {
      this.equipmentDropChances[par1] = par2;
   }

   public boolean canPickUpLoot() {
      return this.canPickUpLoot;
   }

   public void setCanPickUpLoot(boolean par1) {
      this.canPickUpLoot = par1;
   }

   public boolean func_104002_bU() {
      return this.persistenceRequired;
   }

}
