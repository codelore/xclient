package net.minecraft.server.integrated;

import java.io.File;
import java.io.IOException;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ThreadLanServerPing;
import net.minecraft.crash.CrashReport;
import net.minecraft.logging.ILogAgent;
import net.minecraft.logging.LogAgent;
import net.minecraft.network.NetworkListenThread;
import net.minecraft.profiler.PlayerUsageSnooper;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.integrated.CallableIsModded;
import net.minecraft.server.integrated.CallableType3;
import net.minecraft.server.integrated.IntegratedPlayerList;
import net.minecraft.server.integrated.IntegratedServerListenThread;
import net.minecraft.src.Reflector;
import net.minecraft.src.WorldServerOF;
import net.minecraft.util.CryptManager;
import net.minecraft.world.EnumGameType;
import net.minecraft.world.WorldManager;
import net.minecraft.world.WorldServer;
import net.minecraft.world.WorldServerMulti;
import net.minecraft.world.WorldSettings;
import net.minecraft.world.WorldType;
import net.minecraft.world.demo.DemoWorldServer;
import net.minecraft.world.storage.ISaveHandler;

public class IntegratedServer extends MinecraftServer {

   private final Minecraft mc;
   private final WorldSettings theWorldSettings;
   private final ILogAgent serverLogAgent = new LogAgent("Minecraft-Server", " [SERVER]", (new File(Minecraft.getMinecraftDir(), "output-server.log")).getAbsolutePath());
   private IntegratedServerListenThread theServerListeningThread;
   private boolean isGamePaused = false;
   private boolean isPublic;
   private ThreadLanServerPing lanServerPing;


   public IntegratedServer(Minecraft par1Minecraft, String par2Str, String par3Str, WorldSettings par4WorldSettings) {
      super(new File(Minecraft.getMinecraftDir(), "saves"));
      this.setServerOwner(par1Minecraft.session.username);
      this.setFolderName(par2Str);
      this.setWorldName(par3Str);
      this.setDemo(par1Minecraft.isDemo());
      this.canCreateBonusChest(par4WorldSettings.isBonusChestEnabled());
      this.setBuildLimit(256);
      this.setConfigurationManager(new IntegratedPlayerList(this));
      this.mc = par1Minecraft;
      this.theWorldSettings = par4WorldSettings;

      try {
         this.theServerListeningThread = new IntegratedServerListenThread(this);
      } catch (IOException var6) {
         throw new Error();
      }
   }

   protected void loadAllWorlds(String par1Str, String par2Str, long par3, WorldType par5WorldType, String par6Str) {
      this.convertMapIfNeeded(par1Str);
      ISaveHandler var7 = this.getActiveAnvilConverter().getSaveLoader(par1Str, true);
      if(Reflector.DimensionManager.exists()) {
         Object var8 = this.isDemo()?new DemoWorldServer(this, var7, par2Str, 0, super.theProfiler, this.getLogAgent()):new WorldServerOF(this, var7, par2Str, 0, this.theWorldSettings, super.theProfiler, this.getLogAgent());
         Integer[] var9 = (Integer[])((Integer[])Reflector.call(Reflector.DimensionManager_getStaticDimensionIDs, new Object[0]));
         Integer[] arr$ = var9;
         int len$ = var9.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            int dim = arr$[i$].intValue();
            Object world = dim == 0?var8:new WorldServerMulti(this, var7, par2Str, dim, this.theWorldSettings, (WorldServer)var8, super.theProfiler, this.getLogAgent());
            ((WorldServer)world).addWorldAccess(new WorldManager(this, (WorldServer)world));
            if(!this.isSinglePlayer()) {
               ((WorldServer)world).getWorldInfo().setGameType(this.getGameType());
            }

            if(Reflector.EventBus.exists()) {
               Reflector.postForgeBusEvent(Reflector.WorldEvent_Load_Constructor, new Object[]{world});
            }
         }

         this.getConfigurationManager().setPlayerManager(new WorldServer[]{(WorldServer)var8});
      } else {
         super.worldServers = new WorldServer[3];
         super.timeOfLastDimensionTick = new long[super.worldServers.length][100];

         for(int var15 = 0; var15 < super.worldServers.length; ++var15) {
            byte var16 = 0;
            if(var15 == 1) {
               var16 = -1;
            }

            if(var15 == 2) {
               var16 = 1;
            }

            if(var15 == 0) {
               if(this.isDemo()) {
                  super.worldServers[var15] = new DemoWorldServer(this, var7, par2Str, var16, super.theProfiler, this.getLogAgent());
               } else {
                  super.worldServers[var15] = new WorldServerOF(this, var7, par2Str, var16, this.theWorldSettings, super.theProfiler, this.getLogAgent());
               }
            } else {
               super.worldServers[var15] = new WorldServerMulti(this, var7, par2Str, var16, this.theWorldSettings, super.worldServers[0], super.theProfiler, this.getLogAgent());
            }

            super.worldServers[var15].addWorldAccess(new WorldManager(this, super.worldServers[var15]));
            this.getConfigurationManager().setPlayerManager(super.worldServers);
         }
      }

      this.setDifficultyForAllWorlds(this.getDifficulty());
      this.initialWorldChunkLoad();
   }

   protected boolean startServer() throws IOException {
      this.serverLogAgent.logInfo("Starting integrated minecraft server version 1.5.2");
      this.setOnlineMode(false);
      this.setCanSpawnAnimals(true);
      this.setCanSpawnNPCs(true);
      this.setAllowPvp(true);
      this.setAllowFlight(true);
      this.serverLogAgent.logInfo("Generating keypair");
      this.setKeyPair(CryptManager.createNewKeyPair());
      Object inst;
      if(Reflector.FMLCommonHandler_handleServerAboutToStart.exists()) {
         inst = Reflector.call(Reflector.FMLCommonHandler_instance, new Object[0]);
         if(!Reflector.callBoolean(inst, Reflector.FMLCommonHandler_handleServerAboutToStart, new Object[]{this})) {
            return false;
         }
      }

      this.loadAllWorlds(this.getFolderName(), this.getWorldName(), this.theWorldSettings.getSeed(), this.theWorldSettings.getTerrainType(), this.theWorldSettings.func_82749_j());
      this.setMOTD(this.getServerOwner() + " - " + super.worldServers[0].getWorldInfo().getWorldName());
      if(Reflector.FMLCommonHandler_handleServerStarting.exists()) {
         inst = Reflector.call(Reflector.FMLCommonHandler_instance, new Object[0]);
         if(Reflector.FMLCommonHandler_handleServerStarting.getReturnType() == Boolean.TYPE) {
            return Reflector.callBoolean(inst, Reflector.FMLCommonHandler_handleServerStarting, new Object[]{this});
         }

         Reflector.callVoid(inst, Reflector.FMLCommonHandler_handleServerStarting, new Object[]{this});
      }

      return true;
   }

   public void tick() {
      boolean var1 = this.isGamePaused;
      this.isGamePaused = this.theServerListeningThread.isGamePaused();
      if(!var1 && this.isGamePaused) {
         this.serverLogAgent.logInfo("Saving and pausing game...");
         this.getConfigurationManager().saveAllPlayerData();
         this.saveAllWorlds(false);
      }

      if(!this.isGamePaused) {
         super.tick();
      }

   }

   public boolean canStructuresSpawn() {
      return false;
   }

   public EnumGameType getGameType() {
      return this.theWorldSettings.getGameType();
   }

   public int getDifficulty() {
      return this.mc.gameSettings.difficulty;
   }

   public boolean isHardcore() {
      return this.theWorldSettings.getHardcoreEnabled();
   }

   protected File getDataDirectory() {
      return this.mc.mcDataDir;
   }

   public boolean isDedicatedServer() {
      return false;
   }

   public IntegratedServerListenThread getServerListeningThread() {
      return this.theServerListeningThread;
   }

   protected void finalTick(CrashReport par1CrashReport) {
      this.mc.crashed(par1CrashReport);
   }

   public CrashReport addServerInfoToCrashReport(CrashReport par1CrashReport) {
      par1CrashReport = super.addServerInfoToCrashReport(par1CrashReport);
      par1CrashReport.func_85056_g().addCrashSectionCallable("Type", new CallableType3(this));
      par1CrashReport.func_85056_g().addCrashSectionCallable("Is Modded", new CallableIsModded(this));
      return par1CrashReport;
   }

   public void addServerStatsToSnooper(PlayerUsageSnooper par1PlayerUsageSnooper) {
      super.addServerStatsToSnooper(par1PlayerUsageSnooper);
      par1PlayerUsageSnooper.addData("snooper_partner", this.mc.getPlayerUsageSnooper().getUniqueID());
   }

   public boolean isSnooperEnabled() {
      return Minecraft.getMinecraft().isSnooperEnabled();
   }

   public String shareToLAN(EnumGameType par1EnumGameType, boolean par2) {
      try {
         String var4 = this.theServerListeningThread.func_71755_c();
         this.getLogAgent().logInfo("Started on " + var4);
         this.isPublic = true;
         this.lanServerPing = new ThreadLanServerPing(this.getMOTD(), var4);
         this.lanServerPing.start();
         this.getConfigurationManager().setGameType(par1EnumGameType);
         this.getConfigurationManager().setCommandsAllowedForAll(par2);
         return var4;
      } catch (IOException var41) {
         return null;
      }
   }

   public ILogAgent getLogAgent() {
      return this.serverLogAgent;
   }

   public void stopServer() {
      super.stopServer();
      if(this.lanServerPing != null) {
         this.lanServerPing.interrupt();
         this.lanServerPing = null;
      }

   }

   public void initiateShutdown() {
      super.initiateShutdown();
      if(this.lanServerPing != null) {
         this.lanServerPing.interrupt();
         this.lanServerPing = null;
      }

   }

   public boolean getPublic() {
      return this.isPublic;
   }

   public void setGameType(EnumGameType par1EnumGameType) {
      this.getConfigurationManager().setGameType(par1EnumGameType);
   }

   public boolean isCommandBlockEnabled() {
      return true;
   }

   public NetworkListenThread getNetworkThread() {
      return this.getServerListeningThread();
   }
}
