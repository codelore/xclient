package net.minecraft.client.renderer.texture;

import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.texture.Rect2i;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.src.Config;
import net.minecraft.src.TextureUtils;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.Dimension;

public class Texture {

   private int glTextureId;
   private int textureId;
   private int textureType;
   private int width;
   private int height;
   private final int textureDepth;
   private final int textureFormat;
   private final int textureTarget;
   private final int textureMinFilter;
   private final int textureMagFilter;
   private final int textureWrap;
   private final boolean mipmapActive;
   private final String textureName;
   private Rect2i textureRect;
   private boolean transferred;
   private boolean autoCreate;
   private boolean textureNotModified;
   private ByteBuffer textureData;
   private boolean textureBound;
   public ByteBuffer[] mipmapDatas;
   public Dimension[] mipmapDimensions;


   private Texture(String par1Str, int par2, int par3, int par4, int par5, int par6, int par7, int par8, int par9) {
      this.textureName = par1Str;
      this.textureType = par2;
      this.width = par3;
      this.height = par4;
      this.textureDepth = par5;
      this.textureFormat = par7;
      if(Config.isUseMipmaps() && isMipMapTexture(this.textureType, this.textureName)) {
         par8 = Config.getMipmapType();
      }

      this.textureMinFilter = par8;
      this.textureMagFilter = par9;
      char par61 = '\u812f';
      this.textureWrap = par61;
      this.textureRect = new Rect2i(0, 0, par3, par4);
      if(par4 == 1 && par5 == 1) {
         this.textureTarget = 3552;
      } else if(par5 == 1) {
         this.textureTarget = 3553;
      } else {
         this.textureTarget = '\u806f';
      }

      this.mipmapActive = par8 != 9728 && par8 != 9729 || par9 != 9728 && par9 != 9729;
      if(par2 != 2) {
         this.glTextureId = GL11.glGenTextures();
         GL11.glBindTexture(this.textureTarget, this.glTextureId);
         GL11.glTexParameteri(this.textureTarget, 10241, par8);
         GL11.glTexParameteri(this.textureTarget, 10240, par9);
         if(this.mipmapActive) {
            this.updateMipmapLevel(-1);
         }

         GL11.glTexParameteri(this.textureTarget, 10242, par61);
         GL11.glTexParameteri(this.textureTarget, 10243, par61);
      } else {
         this.glTextureId = -1;
      }

      this.textureId = TextureManager.instance().getNextTextureId();
   }

   public Texture(String par1Str, int par2, int par3, int par4, int par5, int par6, int par7, int par8, BufferedImage par9BufferedImage) {
      this(par1Str, par2, par3, par4, 1, par5, par6, par7, par8, par9BufferedImage);
   }

   public Texture(String par1Str, int par2, int par3, int par4, int par5, int par6, int par7, int par8, int par9, BufferedImage par10BufferedImage) {
      this(par1Str, par2, par3, par4, par5, par6, par7, par8, par9);
      if(par10BufferedImage == null) {
         if(par3 != -1 && par4 != -1) {
            byte[] var11 = new byte[par3 * par4 * par5 * 4];

            for(int var12 = 0; var12 < var11.length; ++var12) {
               var11[var12] = 0;
            }

            this.textureData = GLAllocation.createDirectByteBuffer(var11.length);
            this.textureData.clear();
            this.textureData.put(var11);
            this.textureData.position(0).limit(var11.length);
            if(this.autoCreate) {
               this.uploadTexture();
            } else {
               this.textureNotModified = false;
            }
         } else {
            this.transferred = false;
         }
      } else {
         this.transferred = true;
         this.transferFromImage(par10BufferedImage);
         if(par2 != 2) {
            this.uploadTexture();
            this.autoCreate = false;
         }
      }

   }

   public final Rect2i getTextureRect() {
      return this.textureRect;
   }

   public void fillRect(Rect2i par1Rect2i, int par2) {
      if(this.textureTarget != '\u806f') {
         Rect2i var3 = new Rect2i(0, 0, this.width, this.height);
         var3.intersection(par1Rect2i);
         this.textureData.position(0);

         for(int r = var3.getRectY(); r < var3.getRectY() + var3.getRectHeight(); ++r) {
            int var5 = r * this.width * 4;

            for(int var6 = var3.getRectX(); var6 < var3.getRectX() + var3.getRectWidth(); ++var6) {
               this.textureData.put(var5 + var6 * 4 + 0, (byte)(par2 >> 24 & 255));
               this.textureData.put(var5 + var6 * 4 + 1, (byte)(par2 >> 16 & 255));
               this.textureData.put(var5 + var6 * 4 + 2, (byte)(par2 >> 8 & 255));
               this.textureData.put(var5 + var6 * 4 + 3, (byte)(par2 >> 0 & 255));
            }
         }

         if(par1Rect2i.getRectX() == 0 && par1Rect2i.getRectY() == 0 && par1Rect2i.getRectWidth() == this.width && par1Rect2i.getRectHeight() == this.height) {
            this.textureNotModified = false;
         }

         if(this.autoCreate) {
            this.uploadTexture();
         } else {
            this.textureNotModified = false;
         }
      }

   }

   public void writeImage(String par1Str) {
      BufferedImage var2 = new BufferedImage(this.width, this.height, 2);
      ByteBuffer var3 = this.getTextureData();
      byte[] var4 = new byte[this.width * this.height * 4];
      var3.position(0);
      var3.get(var4);

      for(int var9 = 0; var9 < this.width; ++var9) {
         for(int var6 = 0; var6 < this.height; ++var6) {
            int var7 = var6 * this.width * 4 + var9 * 4;
            byte var8 = 0;
            int var10 = var8 | (var4[var7 + 2] & 255) << 0;
            var10 |= (var4[var7 + 1] & 255) << 8;
            var10 |= (var4[var7 + 0] & 255) << 16;
            var10 |= (var4[var7 + 3] & 255) << 24;
            var2.setRGB(var9, var6, var10);
         }
      }

      this.textureData.position(this.width * this.height * 4);

      try {
         ImageIO.write(var2, "png", new File(Minecraft.getMinecraftDir(), par1Str));
      } catch (Exception var101) {
         var101.printStackTrace();
      }

   }

   public void copyFrom(int par1, int par2, Texture par3Texture, boolean par4) {
      if(this.textureTarget != '\u806f') {
         ByteBuffer var5;
         if(this.textureNotModified) {
            if(!this.textureBound) {
               return;
            }

            var5 = par3Texture.getTextureData();
            var5.position(0);
            GL11.glTexSubImage2D(this.textureTarget, 0, par1, par2, par3Texture.getWidth(), par3Texture.getHeight(), this.textureFormat, 5121, var5);
            if(this.mipmapActive) {
               if(par3Texture.mipmapDatas == null) {
                  par3Texture.generateMipMapData();
               }

               ByteBuffer[] var13 = par3Texture.mipmapDatas;
               Dimension[] var14 = par3Texture.mipmapDimensions;
               if(var13 != null && var14 != null) {
                  this.registerMipMapsSub(par1, par2, var13, var14);
               }
            }

            return;
         }

         var5 = par3Texture.getTextureData();
         this.textureData.position(0);
         var5.position(0);

         for(int var6 = 0; var6 < par3Texture.getHeight(); ++var6) {
            int var7 = par2 + var6;
            int var8 = var6 * par3Texture.getWidth() * 4;
            int var9 = var7 * this.width * 4;
            if(par4) {
               var7 = par2 + (par3Texture.getHeight() - var6);
            }

            for(int var10 = 0; var10 < par3Texture.getWidth(); ++var10) {
               int var11 = var9 + (var10 + par1) * 4;
               int var12 = var8 + var10 * 4;
               if(par4) {
                  var11 = par1 + var10 * this.width * 4 + var7 * 4;
               }

               this.textureData.put(var11 + 0, var5.get(var12 + 0));
               this.textureData.put(var11 + 1, var5.get(var12 + 1));
               this.textureData.put(var11 + 2, var5.get(var12 + 2));
               this.textureData.put(var11 + 3, var5.get(var12 + 3));
            }
         }

         this.textureData.position(this.width * this.height * 4);
         if(this.autoCreate) {
            this.uploadTexture();
         } else {
            this.textureNotModified = false;
         }
      }

   }

   public void func_104062_b(int par1, int par2, Texture par3Texture) {
      if(!this.textureBound) {
         Config.getRenderEngine().bindTexture(this.glTextureId);
      }

      GL11.glTexSubImage2D(this.textureTarget, 0, par1, par2, par3Texture.getWidth(), par3Texture.getHeight(), this.textureFormat, 5121, (ByteBuffer)par3Texture.getTextureData().position(0));
      this.textureNotModified = true;
      if(this.mipmapActive) {
         if(par3Texture.mipmapDatas == null) {
            par3Texture.generateMipMapData();
         }

         ByteBuffer[] srcMipDatas = par3Texture.mipmapDatas;
         Dimension[] srcMipDims = par3Texture.mipmapDimensions;
         if(srcMipDatas != null && srcMipDims != null) {
            this.registerMipMapsSub(par1, par2, srcMipDatas, srcMipDims);
         }
      }

   }

   public void transferFromImage(BufferedImage par1BufferedImage) {
      if(this.textureTarget != '\u806f') {
         int var2 = par1BufferedImage.getWidth();
         int var3 = par1BufferedImage.getHeight();
         if(var2 <= this.width && var3 <= this.height) {
            int[] var4 = new int[]{3, 0, 1, 2};
            int[] var5 = new int[]{3, 2, 1, 0};
            int[] var6 = this.textureFormat == '\u80e1'?var5:var4;
            int[] var7 = new int[this.width * this.height];
            int var8 = par1BufferedImage.getTransparency();
            par1BufferedImage.getRGB(0, 0, this.width, this.height, var7, 0, var2);
            byte[] var9 = new byte[this.width * this.height * 4];
            long redSum = 0L;
            long greenSum = 0L;
            long blueSum = 0L;
            long count = 0L;

            int greenAvg;
            int redAvg;
            int var10;
            int blueAvg;
            int var12;
            int var11;
            int var13;
            for(redAvg = 0; redAvg < this.height; ++redAvg) {
               for(greenAvg = 0; greenAvg < this.width; ++greenAvg) {
                  blueAvg = redAvg * this.width + greenAvg;
                  var10 = var7[blueAvg];
                  var11 = var10 >> 24 & 255;
                  if(var11 != 0) {
                     var12 = var10 >> 16 & 255;
                     var13 = var10 >> 8 & 255;
                     int alpha = var10 & 255;
                     redSum += (long)var12;
                     greenSum += (long)var13;
                     blueSum += (long)alpha;
                     ++count;
                  }
               }
            }

            redAvg = 0;
            greenAvg = 0;
            blueAvg = 0;
            if(count > 0L) {
               redAvg = (int)(redSum / count);
               greenAvg = (int)(greenSum / count);
               blueAvg = (int)(blueSum / count);
            }

            for(var10 = 0; var10 < this.height; ++var10) {
               for(var11 = 0; var11 < this.width; ++var11) {
                  var12 = var10 * this.width + var11;
                  var13 = var12 * 4;
                  var9[var13 + var6[0]] = (byte)(var7[var12] >> 24 & 255);
                  var9[var13 + var6[1]] = (byte)(var7[var12] >> 16 & 255);
                  var9[var13 + var6[2]] = (byte)(var7[var12] >> 8 & 255);
                  var9[var13 + var6[3]] = (byte)(var7[var12] >> 0 & 255);
                  byte var26 = (byte)(var7[var12] >> 24 & 255);
                  if(var26 == 0) {
                     var9[var13 + var6[1]] = (byte)redAvg;
                     var9[var13 + var6[2]] = (byte)greenAvg;
                     var9[var13 + var6[3]] = (byte)blueAvg;
                  }
               }
            }

            this.textureData = GLAllocation.createDirectByteBuffer(var9.length);
            this.textureData.clear();
            this.textureData.put(var9);
            this.textureData.limit(var9.length);
            if(this.autoCreate) {
               this.uploadTexture();
            } else {
               this.textureNotModified = false;
            }
         } else {
            Minecraft.getMinecraft().getLogAgent().logWarning("transferFromImage called with a BufferedImage with dimensions (" + var2 + ", " + var3 + ") larger than the Texture dimensions (" + this.width + ", " + this.height + "). Ignoring.");
         }
      }

   }

   public int getTextureId() {
      return this.textureId;
   }

   public int getGlTextureId() {
      return this.glTextureId;
   }

   public int getWidth() {
      return this.width;
   }

   public int getHeight() {
      return this.height;
   }

   public String getTextureName() {
      return this.textureName;
   }

   public void bindTexture(int par1) {
      Config.getRenderEngine().bindTexture(this.glTextureId);
   }

   public void uploadTexture() {
      if(this.glTextureId <= 0) {
         this.glTextureId = GL11.glGenTextures();
         GL11.glBindTexture(this.textureTarget, this.glTextureId);
         GL11.glTexParameteri(this.textureTarget, 10241, this.textureMinFilter);
         GL11.glTexParameteri(this.textureTarget, 10240, this.textureMagFilter);
         if(this.mipmapActive) {
            this.updateMipmapLevel(16);
         }

         GL11.glTexParameteri(this.textureTarget, 10242, 10496);
         GL11.glTexParameteri(this.textureTarget, 10243, 10496);
      }

      this.textureData.clear();
      if(this.height != 1 && this.textureDepth != 1) {
         GL12.glTexImage3D(this.textureTarget, 0, this.textureFormat, this.width, this.height, this.textureDepth, 0, this.textureFormat, 5121, this.textureData);
      } else if(this.height != 1) {
         GL11.glTexImage2D(this.textureTarget, 0, this.textureFormat, this.width, this.height, 0, this.textureFormat, 5121, this.textureData);
         if(this.mipmapActive) {
            this.generateMipMaps(true);
         }
      } else {
         GL11.glTexImage1D(this.textureTarget, 0, this.textureFormat, this.width, 0, this.textureFormat, 5121, this.textureData);
      }

      this.textureNotModified = true;
   }

   public ByteBuffer getTextureData() {
      return this.textureData;
   }

   public void generateMipMapData() {
      this.generateMipMaps(false);
   }

   private void generateMipMaps(boolean createTex) {
      if(this.mipmapDatas == null) {
         this.allocateMipmapDatas();
      }

      ByteBuffer parMipData = this.textureData;
      int parWidth = this.width;
      boolean scale = true;

      for(int i = 0; i < this.mipmapDatas.length; ++i) {
         ByteBuffer mipData = this.mipmapDatas[i];
         int level = i + 1;
         Dimension dim = this.mipmapDimensions[i];
         int mipWidth = dim.getWidth();
         int mipHeight = dim.getHeight();
         if(scale) {
            mipData.clear();
            parMipData.clear();

            for(int mipX = 0; mipX < mipWidth; ++mipX) {
               for(int mipY = 0; mipY < mipHeight; ++mipY) {
                  int p1 = parMipData.getInt((mipX * 2 + 0 + (mipY * 2 + 0) * parWidth) * 4);
                  int p2 = parMipData.getInt((mipX * 2 + 1 + (mipY * 2 + 0) * parWidth) * 4);
                  int p3 = parMipData.getInt((mipX * 2 + 1 + (mipY * 2 + 1) * parWidth) * 4);
                  int p4 = parMipData.getInt((mipX * 2 + 0 + (mipY * 2 + 1) * parWidth) * 4);
                  int pixel = this.alphaBlend(p1, p2, p3, p4);
                  mipData.putInt((mipX + mipY * mipWidth) * 4, pixel);
               }
            }

            mipData.clear();
            parMipData.clear();
         }

         if(createTex) {
            GL11.glTexImage2D(this.textureTarget, level, this.textureFormat, mipWidth, mipHeight, 0, this.textureFormat, 5121, mipData);
         }

         parMipData = mipData;
         parWidth = mipWidth;
         if(mipWidth <= 1 || mipHeight <= 1) {
            scale = false;
         }
      }

   }

   private void registerMipMapsSub(int xOffset, int yOffset, ByteBuffer[] srcMipDatas, Dimension[] srcMipDims) {
      int xMipOffset = xOffset / 2;
      int yMipOffset = yOffset / 2;

      for(int i = 0; i < srcMipDatas.length; ++i) {
         ByteBuffer mipData = srcMipDatas[i];
         int level = i + 1;
         Dimension dim = srcMipDims[i];
         int mipWidth = dim.getWidth();
         int mipHeight = dim.getHeight();
         mipData.clear();
         GL11.glTexSubImage2D(this.textureTarget, level, xMipOffset, yMipOffset, mipWidth, mipHeight, this.textureFormat, 5121, mipData);
         xMipOffset /= 2;
         yMipOffset /= 2;
      }

   }

   private int alphaBlend(int c1, int c2, int c3, int c4) {
      int cx1 = this.alphaBlend(c1, c2);
      int cx2 = this.alphaBlend(c3, c4);
      int cx = this.alphaBlend(cx1, cx2);
      return cx;
   }

   private int alphaBlend(int c1, int c2) {
      int a1 = (c1 & -16777216) >> 24 & 255;
      int a2 = (c2 & -16777216) >> 24 & 255;
      int ax = (a1 + a2) / 2;
      if(a1 == 0 && a2 == 0) {
         a1 = 1;
         a2 = 1;
      } else {
         if(a1 == 0) {
            c1 = c2;
            ax /= 2;
         }

         if(a2 == 0) {
            c2 = c1;
            ax /= 2;
         }
      }

      int r1 = (c1 >> 16 & 255) * a1;
      int g1 = (c1 >> 8 & 255) * a1;
      int b1 = (c1 & 255) * a1;
      int r2 = (c2 >> 16 & 255) * a2;
      int g2 = (c2 >> 8 & 255) * a2;
      int b2 = (c2 & 255) * a2;
      int rx = (r1 + r2) / (a1 + a2);
      int gx = (g1 + g2) / (a1 + a2);
      int bx = (b1 + b2) / (a1 + a2);
      return ax << 24 | rx << 16 | gx << 8 | bx;
   }

   private int averageColor(int i, int j) {
      int k = (i & -16777216) >> 24 & 255;
      int l = (j & -16777216) >> 24 & 255;
      return (k + l >> 1 << 24) + ((i & 16711422) + (j & 16711422) >> 1);
   }

   private void allocateMipmapDatas() {
      int texWidth = TextureUtils.ceilPowerOfTwo(this.width);
      int texHeight = TextureUtils.ceilPowerOfTwo(this.height);
      if(texWidth == this.width && texHeight == this.height) {
         int imgLen = texWidth * texHeight * 4;
         ArrayList listDatas = new ArrayList();
         ArrayList listDims = new ArrayList();
         int mipWidth = texWidth;
         int mipHeight = texHeight;

         while(true) {
            mipWidth /= 2;
            mipHeight /= 2;
            if(mipWidth <= 0 && mipHeight <= 0) {
               this.mipmapDatas = (ByteBuffer[])((ByteBuffer[])listDatas.toArray(new ByteBuffer[listDatas.size()]));
               this.mipmapDimensions = (Dimension[])((Dimension[])listDims.toArray(new Dimension[listDims.size()]));
               return;
            }

            if(mipWidth <= 0) {
               mipWidth = 1;
            }

            if(mipHeight <= 0) {
               mipHeight = 1;
            }

            int mipLen = mipWidth * mipHeight * 4;
            ByteBuffer buf = GLAllocation.createDirectByteBuffer(mipLen);
            listDatas.add(buf);
            Dimension dim = new Dimension(mipWidth, mipHeight);
            listDims.add(dim);
         }
      } else {
         Config.dbg("Mipmaps not possible (power of 2 dimensions needed), texture: " + this.textureName + ", dim: " + this.width + "x" + this.height);
         this.mipmapDatas = new ByteBuffer[0];
         this.mipmapDimensions = new Dimension[0];
      }
   }

   private int getMaxMipmapLevel(int size) {
      int level;
      for(level = 0; size > 0; ++level) {
         size /= 2;
      }

      return level - 1;
   }

   public static boolean isMipMapTexture(int type, String name) {
      return type == 3?true:(type == 2?false:name.equals("terrain"));
   }

   public void scaleUp(int tileSize) {
      if(this.textureTarget == 3553) {
         int tileSizePo2 = TextureUtils.ceilPowerOfTwo(tileSize);
         int size = Math.max(this.width, this.height);

         for(int sizePo2 = TextureUtils.ceilPowerOfTwo(size); sizePo2 < tileSizePo2; sizePo2 *= 2) {
            this.scale2x();
         }

      }
   }

   private void scale2x() {
      int w = this.width;
      int h = this.height;
      byte[] data = new byte[this.width * this.height * 4];
      this.textureData.position(0);
      this.textureData.get(data);
      this.width *= 2;
      this.height *= 2;
      this.textureRect = new Rect2i(0, 0, this.width, this.height);
      this.textureData = GLAllocation.createDirectByteBuffer(this.width * this.height * 4);
      this.copyScaled(data, w, this.textureData, this.width);
   }

   private void copyScaled(byte[] buf, int srcWidth, ByteBuffer dstBuf, int dstWidth) {
      int scale = dstWidth / srcWidth;
      byte[] buf4 = new byte[4];
      int len = dstWidth * dstWidth;
      dstBuf.clear();
      if(scale > 1) {
         for(int y = 0; y < srcWidth; ++y) {
            int yMul = y * srcWidth;
            int ty = y * scale;
            int tyMul = ty * dstWidth;

            for(int x = 0; x < srcWidth; ++x) {
               int srcPos = (x + yMul) * 4;
               buf4[0] = buf[srcPos];
               buf4[1] = buf[srcPos + 1];
               buf4[2] = buf[srcPos + 2];
               buf4[3] = buf[srcPos + 3];
               int tx = x * scale;
               int dstPosBase = tx + tyMul;

               for(int tdy = 0; tdy < scale; ++tdy) {
                  int dstPosY = dstPosBase + tdy * dstWidth;
                  dstBuf.position(dstPosY * 4);

                  for(int tdx = 0; tdx < scale; ++tdx) {
                     dstBuf.put(buf4);
                  }
               }
            }
         }
      }

      dstBuf.position(0).limit(dstWidth * dstWidth * 4);
   }

   public void updateMipmapLevel(int gridSize) {
      if(this.mipmapActive) {
         if(GLContext.getCapabilities().OpenGL12) {
            GL11.glTexParameteri(3553, '\u813c', 0);
            int maxAF = Config.getMipmapLevel();
            if(maxAF >= 4) {
               int maxLevel = Math.min(this.width, this.height);
               maxAF = this.getMaxMipmapLevel(maxLevel);
               if(gridSize > 1) {
                  int level = TextureUtils.getPowerOfTwo(gridSize);
                  maxAF = level;
               }

               if(maxAF < 0) {
                  maxAF = 0;
               }
            }

            GL11.glTexParameteri(3553, '\u813d', maxAF);
         }

         if(Config.getAnisotropicFilterLevel() > 1 && GLContext.getCapabilities().GL_EXT_texture_filter_anisotropic) {
            FloatBuffer maxAF1 = BufferUtils.createFloatBuffer(16);
            maxAF1.rewind();
            GL11.glGetFloat('\u84ff', maxAF1);
            float maxLevel1 = maxAF1.get(0);
            float level1 = (float)Config.getAnisotropicFilterLevel();
            level1 = Math.min(level1, maxLevel1);
            GL11.glTexParameterf(3553, '\u84fe', level1);
         }

      }
   }

   public void setTextureBound(boolean textureBound) {
      this.textureBound = textureBound;
   }

   public boolean isTextureBound() {
      return this.textureBound;
   }

   public void deleteTexture() {
      if(this.glTextureId > 0) {
         GL11.glDeleteTextures(this.glTextureId);
         this.glTextureId = 0;
      }

      this.textureData = null;
      this.mipmapDatas = null;
      this.mipmapDimensions = null;
   }

   public String toString() {
      return "Texture: " + this.textureName + ", dim: " + this.width + "x" + this.height + ", gl: " + this.glTextureId + ", created: " + this.textureNotModified;
   }

   public Texture duplicate(int newType) {
      Texture tex = new Texture(this.textureName, newType, this.width, this.height, this.textureDepth, this.textureWrap, this.textureFormat, this.textureMinFilter, this.textureMagFilter);
      this.textureData.clear();
      tex.textureData = GLAllocation.createDirectByteBuffer(this.textureData.capacity());
      tex.textureData.put(this.textureData);
      this.textureData.clear();
      tex.textureData.clear();
      return tex;
   }

   public void createAndUploadTexture() {
      Config.dbg("Forge method not implemented: TextureStitched.createAndUploadTexture()");
   }
}
