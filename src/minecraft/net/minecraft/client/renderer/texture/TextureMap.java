package net.minecraft.client.renderer.texture;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.StitcherException;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.client.renderer.texture.StitchHolder;
import net.minecraft.client.renderer.texture.StitchSlot;
import net.minecraft.client.renderer.texture.Stitcher;
import net.minecraft.client.renderer.texture.Texture;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.texture.TextureStitched;
import net.minecraft.client.texturepacks.ITexturePack;
import net.minecraft.item.Item;
import net.minecraft.src.Config;
import net.minecraft.src.ConnectedTextures;
import net.minecraft.src.NaturalTextures;
import net.minecraft.src.Reflector;
import net.minecraft.src.TextureUtils;
import net.minecraft.util.Icon;

public class TextureMap implements IconRegister {

   public final int textureType;
   public final String textureName;
   public final String basePath;
   public final String textureExt;
   private final HashMap mapTexturesStiched = new HashMap();
   private BufferedImage missingImage = new BufferedImage(64, 64, 2);
   private TextureStitched missingTextureStiched;
   private Texture atlasTexture;
   private final List listTextureStiched = new ArrayList();
   private final Map textureStichedMap = new HashMap();
   private int iconGridSize = -1;
   private int iconGridCountX = -1;
   private int iconGridCountY = -1;
   private double iconGridSizeU = -1.0D;
   private double iconGridSizeV = -1.0D;
   private TextureStitched[] iconGrid = null;


   public TextureMap(int par1, String par2, String par3Str, BufferedImage par4BufferedImage) {
      this.textureType = par1;
      this.textureName = par2;
      this.basePath = par3Str;
      this.textureExt = ".png";
      this.missingImage = par4BufferedImage;
   }

   public void refreshTextures() {
      Config.dbg("Creating texture map: " + this.textureName);
      if(this.atlasTexture != null) {
         this.atlasTexture.deleteTexture();
      }

      Iterator var2 = this.textureStichedMap.values().iterator();

      while(var2.hasNext()) {
         TextureStitched var3 = (TextureStitched)var2.next();
         var3.deleteTextures();
      }

      this.textureStichedMap.clear();
      Reflector.callVoid(Reflector.ForgeHooksClient_onTextureStitchedPre, new Object[]{this});
      int var231;
      int var241;
      if(this.textureType == 0) {
         Block[] var19 = Block.blocksList;
         var231 = var19.length;

         for(var241 = 0; var241 < var231; ++var241) {
            Block var20 = var19[var241];
            if(var20 != null) {
               var20.registerIcons(this);
            }
         }

         Minecraft.getMinecraft().renderGlobal.registerDestroyBlockIcons(this);
         RenderManager.instance.updateIcons(this);
         ConnectedTextures.updateIcons(this);
         NaturalTextures.updateIcons(this);
      }

      Item[] var25 = Item.itemsList;
      var231 = var25.length;

      for(var241 = 0; var241 < var231; ++var241) {
         Item var26 = var25[var241];
         if(var26 != null && var26.getSpriteNumber() == this.textureType) {
            var26.registerIcons(this);
         }
      }

      HashMap var27 = new HashMap();
      Stitcher var21 = TextureManager.instance().createStitcher(this.textureName);
      this.mapTexturesStiched.clear();
      this.listTextureStiched.clear();
      Texture var23 = TextureManager.instance().makeTexture("missingno", 2, this.missingImage.getWidth(), this.missingImage.getHeight(), 10496, 6408, 9728, 9728, false, this.missingImage);
      StitchHolder var24 = new StitchHolder(var23);
      var21.addStitchHolder(var24);
      var27.put(var24, Arrays.asList(new Texture[]{var23}));
      Iterator var5 = this.textureStichedMap.keySet().iterator();
      ArrayList texLists = new ArrayList();

      while(var5.hasNext()) {
         String it = (String)var5.next();
         String ts = this.makeFullTextureName(it) + this.textureExt;
         List var28 = TextureManager.instance().createNewTexture(it, ts, (TextureStitched)this.textureStichedMap.get(it));
         texLists.add(var28);
      }

      this.iconGridSize = this.getStandardTileSize(texLists);
      Config.dbg("Icon grid size: " + this.textureName + ", " + this.iconGridSize);
      Iterator var31 = texLists.iterator();

      List var291;
      while(var31.hasNext()) {
         var291 = (List)var31.next();
         if(!var291.isEmpty()) {
            this.scaleTextures(var291, this.iconGridSize);
         }
      }

      var31 = texLists.iterator();

      while(var31.hasNext()) {
         var291 = (List)var31.next();
         if(!var291.isEmpty()) {
            StitchHolder var32 = new StitchHolder((Texture)var291.get(0));
            var21.addStitchHolder(var32);
            var27.put(var32, var291);
         }
      }

      try {
         var21.doStitch();
      } catch (StitcherException var22) {
         throw var22;
      }

      this.atlasTexture = var21.getTexture();
      Config.dbg("Texture size: " + this.textureName + ", " + this.atlasTexture.getWidth() + "x" + this.atlasTexture.getHeight());
      this.atlasTexture.updateMipmapLevel(this.iconGridSize);
      var5 = var21.getStichSlots().iterator();

      while(var5.hasNext()) {
         StitchSlot var281 = (StitchSlot)var5.next();
         StitchHolder var35 = var281.getStitchHolder();
         Texture var34 = var35.func_98150_a();
         String var29 = var34.getTextureName();
         List var10 = (List)var27.get(var35);
         TextureStitched var11 = (TextureStitched)this.textureStichedMap.get(var29);
         boolean var12 = false;
         if(var11 == null) {
            var12 = true;
            var11 = TextureStitched.makeTextureStitched(var29);
            if(!var29.equals("missingno")) {
               Minecraft.getMinecraft().getLogAgent().logWarning("Couldn\'t find premade icon for " + var29 + " doing " + this.textureName);
            }
         }

         var11.init(this.atlasTexture, var10, var281.getOriginX(), var281.getOriginY(), var35.func_98150_a().getWidth(), var35.func_98150_a().getHeight(), var35.isRotated());
         this.mapTexturesStiched.put(var29, var11);
         if(!var12) {
            this.textureStichedMap.remove(var29);
         }

         if(var10.size() > 1) {
            this.listTextureStiched.add(var11);
            String var13 = this.makeFullTextureName(var29) + ".txt";
            ITexturePack var14 = Minecraft.getMinecraft().texturePackList.getSelectedTexturePack();
            boolean var15 = !var14.func_98138_b("/" + this.basePath + var29 + ".png", false);

            try {
               InputStream var17 = var14.func_98137_a("/" + var13, var15);
               Minecraft.getMinecraft().getLogAgent().logInfo("Found animation info for: " + var13);
               var11.readAnimationInfo(new BufferedReader(new InputStreamReader(var17)));
            } catch (IOException var211) {
               ;
            }
         }
      }

      this.missingTextureStiched = (TextureStitched)this.mapTexturesStiched.get("missingno");
      var5 = this.textureStichedMap.values().iterator();

      while(var5.hasNext()) {
         TextureStitched var30 = (TextureStitched)var5.next();
         var30.copyFrom(this.missingTextureStiched);
      }

      this.textureStichedMap.putAll(this.mapTexturesStiched);
      this.mapTexturesStiched.clear();
      this.updateIconGrid();
      this.atlasTexture.writeImage("debug.stitched_" + this.textureName + ".png");
      Reflector.callVoid(Reflector.ForgeHooksClient_onTextureStitchedPost, new Object[]{this});
      this.atlasTexture.uploadTexture();
      if(Config.isMultiTexture()) {
         var31 = this.textureStichedMap.values().iterator();

         while(var31.hasNext()) {
            TextureStitched var33 = (TextureStitched)var31.next();
            var33.createTileTexture();
         }
      }

   }

   public void updateAnimations() {
      if(this.listTextureStiched.size() > 0) {
         this.getTexture().bindTexture(0);
         this.atlasTexture.setTextureBound(true);
         Iterator var1 = this.listTextureStiched.iterator();

         while(var1.hasNext()) {
            TextureStitched i = (TextureStitched)var1.next();
            if(this.textureType == 0) {
               if(!this.isTerrainAnimationActive(i)) {
                  continue;
               }
            } else if(this.textureType == 1 && !Config.isAnimatedItems()) {
               continue;
            }

            i.updateAnimation();
         }

         this.atlasTexture.setTextureBound(false);
         if(Config.isMultiTexture()) {
            for(int var4 = 0; var4 < this.listTextureStiched.size(); ++var4) {
               TextureStitched ts = (TextureStitched)this.listTextureStiched.get(var4);
               if(this.isTerrainAnimationActive(ts)) {
                  ts.updateTileAnimation();
               }
            }
         }

      }
   }

   public Texture getTexture() {
      return this.atlasTexture;
   }

   public Icon registerIcon(String par1Str) {
      if(par1Str == null) {
         (new RuntimeException("Don\'t register null!")).printStackTrace();
         par1Str = "null";
      }

      TextureStitched var2 = (TextureStitched)this.textureStichedMap.get(par1Str);
      if(var2 == null) {
         var2 = TextureStitched.makeTextureStitched(par1Str);
         var2.setIndexInMap(this.textureStichedMap.size());
         this.textureStichedMap.put(par1Str, var2);
      }

      return var2;
   }

   public Icon getMissingIcon() {
      return this.missingTextureStiched;
   }

   private String makeFullTextureName(String name) {
      int modSepPos = name.indexOf(":");
      if(modSepPos > 0) {
         String modName = name.substring(0, modSepPos);
         String texName = name.substring(modSepPos + 1);
         return "mods/" + modName + "/" + this.basePath + texName;
      } else {
         return name.startsWith("ctm/")?name:this.basePath + name;
      }
   }

   public TextureStitched getIconSafe(String name) {
      return (TextureStitched)this.textureStichedMap.get(name);
   }

   private int getStandardTileSize(List texLists) {
      int[] sizeCounts = new int[16];
      Iterator mostUsedPo2 = texLists.iterator();

      int count;
      while(mostUsedPo2.hasNext()) {
         List mostUsedCount = (List)mostUsedPo2.next();
         if(!mostUsedCount.isEmpty()) {
            Texture value = (Texture)mostUsedCount.get(0);
            if(value != null) {
               count = TextureUtils.getPowerOfTwo(value.getWidth());
               int po2h = TextureUtils.getPowerOfTwo(value.getHeight());
               int po2 = Math.max(count, po2h);
               if(po2 < sizeCounts.length) {
                  ++sizeCounts[po2];
               }
            }
         }
      }

      int var9 = 4;
      int var10 = 0;

      int var11;
      for(var11 = 0; var11 < sizeCounts.length; ++var11) {
         count = sizeCounts[var11];
         if(count > var10) {
            var9 = var11;
            var10 = count;
         }
      }

      if(var9 < 4) {
         var9 = 4;
      }

      var11 = TextureUtils.twoToPower(var9);
      return var11;
   }

   private void scaleTextures(List texList, int tileSize) {
      if(!texList.isEmpty()) {
         Texture tex = (Texture)texList.get(0);
         int size = Math.max(tex.getWidth(), tex.getHeight());
         if(size < tileSize) {
            for(int i = 0; i < texList.size(); ++i) {
               Texture t = (Texture)texList.get(i);
               t.scaleUp(tileSize);
            }

         }
      }
   }

   public TextureStitched getTextureExtry(String name) {
      return (TextureStitched)this.textureStichedMap.get(name);
   }

   public boolean setTextureEntry(String name, TextureStitched entry) {
      if(!this.textureStichedMap.containsKey(name)) {
         entry.setIndexInMap(this.textureStichedMap.size());
         this.textureStichedMap.put(name, entry);
         return true;
      } else {
         return false;
      }
   }

   private void updateIconGrid() {
      this.iconGridCountX = -1;
      this.iconGridCountY = -1;
      this.iconGrid = null;
      if(this.iconGridSize > 0) {
         this.iconGridCountX = this.atlasTexture.getWidth() / this.iconGridSize;
         this.iconGridCountY = this.atlasTexture.getHeight() / this.iconGridSize;
         this.iconGrid = new TextureStitched[this.iconGridCountX * this.iconGridCountY];
         this.iconGridSizeU = 1.0D / (double)this.iconGridCountX;
         this.iconGridSizeV = 1.0D / (double)this.iconGridCountY;
         Iterator it = this.textureStichedMap.values().iterator();

         while(it.hasNext()) {
            TextureStitched ts = (TextureStitched)it.next();
            double uMin = (double)Math.min(ts.getMinU(), ts.getMaxU());
            double vMin = (double)Math.min(ts.getMinV(), ts.getMaxV());
            double uMax = (double)Math.max(ts.getMinU(), ts.getMaxU());
            double vMax = (double)Math.max(ts.getMinV(), ts.getMaxV());
            int iuMin = (int)(uMin / this.iconGridSizeU);
            int ivMin = (int)(vMin / this.iconGridSizeV);
            int iuMax = (int)(uMax / this.iconGridSizeU);
            int ivMax = (int)(vMax / this.iconGridSizeV);

            for(int iu = iuMin; iu <= iuMax; ++iu) {
               if(iu >= 0 && iu < this.iconGridCountX) {
                  for(int iv = ivMin; iv <= ivMax; ++iv) {
                     if(iv >= 0 && iv < this.iconGridCountX) {
                        int index = iv * this.iconGridCountX + iu;
                        this.iconGrid[index] = ts;
                     } else {
                        Config.dbg("Invalid grid V: " + iv + ", icon: " + ts.getIconName());
                     }
                  }
               } else {
                  Config.dbg("Invalid grid U: " + iu + ", icon: " + ts.getIconName());
               }
            }
         }

      }
   }

   public TextureStitched getIconByUV(double u, double v) {
      if(this.iconGrid == null) {
         return null;
      } else {
         int iu = (int)(u / this.iconGridSizeU);
         int iv = (int)(v / this.iconGridSizeV);
         int index = iv * this.iconGridCountX + iu;
         return index >= 0 && index <= this.iconGrid.length?this.iconGrid[index]:null;
      }
   }

   public TextureStitched getMissingTextureStiched() {
      return this.missingTextureStiched;
   }

   public int getMaxTextureIndex() {
      return this.textureStichedMap.size();
   }

   private boolean isTerrainAnimationActive(TextureStitched ts) {
      return ts != TextureUtils.iconWater && ts != TextureUtils.iconWaterFlow?(ts != TextureUtils.iconLava && ts != TextureUtils.iconLavaFlow?(ts != TextureUtils.iconFire0 && ts != TextureUtils.iconFire1?(ts == TextureUtils.iconPortal?Config.isAnimatedPortal():Config.isAnimatedTerrain()):Config.isAnimatedFire()):Config.isAnimatedLava()):Config.isAnimatedWater();
   }
}
