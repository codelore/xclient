package net.minecraft.client.renderer.texture;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.renderer.texture.Texture;
import net.minecraft.client.renderer.texture.TextureClock;
import net.minecraft.client.renderer.texture.TextureCompass;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.texturepacks.ITexturePack;
import net.minecraft.src.Config;
import net.minecraft.util.Icon;
import net.minecraft.util.Tuple;

public class TextureStitched implements Icon {

   private final String textureName;
   protected Texture textureSheet;
   protected List textureList;
   private List listAnimationTuples;
   protected boolean rotated;
   protected int originX;
   protected int originY;
   private int width;
   private int height;
   private float minU;
   private float maxU;
   private float minV;
   private float maxV;
   private float widthNorm;
   private float heightNorm;
   protected int frameCounter = 0;
   protected int tickCounter = 0;
   private int indexInMap = -1;
   public float baseU;
   public float baseV;
   public Texture tileTexture = null;
   private int currentAnimationIndex = -1;


   public static TextureStitched makeTextureStitched(String par0Str) {
      return (TextureStitched)("clock".equals(par0Str)?new TextureClock():("compass".equals(par0Str)?new TextureCompass():new TextureStitched(par0Str)));
   }

   protected TextureStitched(String par1) {
      this.textureName = par1;
   }

   public void init(Texture par1Texture, List par2List, int par3, int par4, int par5, int par6, boolean par7) {
      this.textureSheet = par1Texture;
      this.textureList = par2List;
      this.originX = par3;
      this.originY = par4;
      this.width = par5;
      this.height = par6;
      this.rotated = par7;
      float var8 = 0.01F / (float)par1Texture.getWidth();
      float var9 = 0.01F / (float)par1Texture.getHeight();
      this.minU = (float)par3 / (float)par1Texture.getWidth() + var8;
      this.maxU = (float)(par3 + par5) / (float)par1Texture.getWidth() - var8;
      this.minV = (float)par4 / (float)par1Texture.getHeight() + var9;
      this.maxV = (float)(par4 + par6) / (float)par1Texture.getHeight() - var9;
      this.widthNorm = (float)par5 / 16.0F;
      this.heightNorm = (float)par6 / 16.0F;
      this.baseU = Math.min(this.minU, this.maxU);
      this.baseV = Math.min(this.minV, this.maxV);
   }

   public void copyFrom(TextureStitched par1TextureStitched) {
      this.init(par1TextureStitched.textureSheet, par1TextureStitched.textureList, par1TextureStitched.originX, par1TextureStitched.originY, par1TextureStitched.width, par1TextureStitched.height, par1TextureStitched.rotated);
   }

   public int getOriginX() {
      return this.originX;
   }

   public int getOriginY() {
      return this.originY;
   }

   public float getMinU() {
      return this.minU;
   }

   public float getMaxU() {
      return this.maxU;
   }

   public float getInterpolatedU(double par1) {
      float var3 = this.maxU - this.minU;
      return this.minU + var3 * ((float)par1 / 16.0F);
   }

   public float getMinV() {
      return this.minV;
   }

   public float getMaxV() {
      return this.maxV;
   }

   public float getInterpolatedV(double par1) {
      float var3 = this.maxV - this.minV;
      return this.minV + var3 * ((float)par1 / 16.0F);
   }

   public String getIconName() {
      return this.textureName;
   }

   public int getSheetWidth() {
      return this.textureSheet.getWidth();
   }

   public int getSheetHeight() {
      return this.textureSheet.getHeight();
   }

   public void updateAnimation() {
      if(this.listAnimationTuples != null) {
         Tuple var4 = (Tuple)this.listAnimationTuples.get(this.frameCounter);
         ++this.tickCounter;
         if(this.tickCounter >= ((Integer)var4.getSecond()).intValue()) {
            int var2 = ((Integer)var4.getFirst()).intValue();
            this.frameCounter = (this.frameCounter + 1) % this.listAnimationTuples.size();
            this.tickCounter = 0;
            var4 = (Tuple)this.listAnimationTuples.get(this.frameCounter);
            int var3 = ((Integer)var4.getFirst()).intValue();
            if(var2 != var3 && var3 >= 0 && var3 < this.textureList.size()) {
               this.textureSheet.func_104062_b(this.originX, this.originY, (Texture)this.textureList.get(var3));
               this.currentAnimationIndex = var3;
            }
         }
      } else {
         int var41 = this.frameCounter;
         this.frameCounter = (this.frameCounter + 1) % this.textureList.size();
         if(var41 != this.frameCounter) {
            this.textureSheet.func_104062_b(this.originX, this.originY, (Texture)this.textureList.get(this.frameCounter));
            this.currentAnimationIndex = this.frameCounter;
         }
      }

   }

   public void readAnimationInfo(BufferedReader par1BufferedReader) {
      ArrayList var2 = new ArrayList();

      try {
         for(String var12 = par1BufferedReader.readLine(); var12 != null; var12 = par1BufferedReader.readLine()) {
            var12 = var12.trim();
            if(var12.length() > 0) {
               String[] var4 = var12.split(",");
               String[] var5 = var4;
               int var6 = var4.length;

               for(int var7 = 0; var7 < var6; ++var7) {
                  String var8 = var5[var7];
                  int var9 = var8.indexOf(42);
                  if(var9 > 0) {
                     Integer var10 = new Integer(var8.substring(0, var9));
                     Integer var11 = new Integer(var8.substring(var9 + 1));
                     var2.add(new Tuple(var10, var11));
                  } else {
                     var2.add(new Tuple(new Integer(var8), Integer.valueOf(1)));
                  }
               }
            }
         }
      } catch (Exception var121) {
         System.err.println("Failed to read animation info for " + this.textureName + ": " + var121.getMessage());
      }

      if(!var2.isEmpty() && var2.size() < 600) {
         this.listAnimationTuples = var2;
      }

   }

   public Texture getTexture() {
      return this.textureSheet;
   }

   public int getIndexInMap() {
      return this.indexInMap;
   }

   public void setIndexInMap(int indexInMap) {
      this.indexInMap = indexInMap;
   }

   public void deleteTextures() {
      if(this.tileTexture != null) {
         this.tileTexture.deleteTexture();
      }

      if(this.textureList != null) {
         for(int i = 0; i < this.textureList.size(); ++i) {
            Texture tex = (Texture)this.textureList.get(i);
            tex.deleteTexture();
         }
      }

   }

   public void createTileTexture() {
      if(this.tileTexture == null) {
         Texture baseTexture = (Texture)this.textureList.get(0);
         this.tileTexture = baseTexture.duplicate(3);
         this.tileTexture.uploadTexture();
      }
   }

   public int getWidth() {
      return this.width;
   }

   public int getHeight() {
      return this.height;
   }

   public String toString() {
      return "Icon: " + this.textureName + ", " + this.originX + "," + this.originY + ", " + this.width + "x" + this.height + ", " + this.rotated;
   }

   public void updateTileAnimation() {
      if(this.tileTexture != null) {
         if(this.currentAnimationIndex >= 0) {
            Texture srcTexture = (Texture)this.textureList.get(this.currentAnimationIndex);
            this.tileTexture.bindTexture(0);
            this.tileTexture.setTextureBound(true);
            this.tileTexture.copyFrom(0, 0, srcTexture, false);
            this.tileTexture.setTextureBound(false);
            this.currentAnimationIndex = -1;
         }
      }
   }

   public boolean loadTexture(TextureManager manager, ITexturePack texturepack, String name, String fileName, BufferedImage image, ArrayList textures) {
      return false;
   }

   public void createAndUploadTextures() {
      Config.dbg("Forge method not implemented: TextureStitched.createAndUploadTextures()");
   }
}
