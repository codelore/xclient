package net.minecraft.client.renderer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.IImageBuffer;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.texturepacks.ITexturePack;
import net.minecraft.client.texturepacks.TexturePackList;
import net.minecraft.src.Config;
import net.minecraft.src.ConnectedTextures;
import net.minecraft.src.CustomColorizer;
import net.minecraft.src.CustomSky;
import net.minecraft.src.NaturalTextures;
import net.minecraft.src.RandomMobs;
import net.minecraft.src.Reflector;
import net.minecraft.src.TextureAnimations;
import net.minecraft.src.TextureUtils;
import net.minecraft.src.WrUpdates;
import net.minecraft.util.Icon;
import net.minecraft.util.IntHashMap;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;

public class RenderEngine {

   private HashMap textureMap = new HashMap();
   private HashMap textureContentsMap = new HashMap();
   private IntHashMap textureNameToImageMap = new IntHashMap();
   private IntBuffer imageData = GLAllocation.createDirectIntBuffer(4194304);
   private Map urlToImageDataMap = new HashMap();
   private GameSettings options;
   public TexturePackList texturePack;
   private BufferedImage missingTextureImage = new BufferedImage(64, 64, 2);
   public final TextureMap textureMapBlocks;
   public final TextureMap textureMapItems;
   public int boundTexture;
   public static Logger log = Logger.getAnonymousLogger();
   private boolean initialized = false;


   public RenderEngine(TexturePackList par1TexturePackList, GameSettings par2GameSettings) {
      if(Config.isMultiTexture()) {
         int var3 = Config.getAntialiasingLevel();
         Config.dbg("FSAA Samples: " + var3);

         try {
            Display.destroy();
            Display.create((new PixelFormat()).withDepthBits(24).withSamples(var3));
         } catch (LWJGLException var9) {
            Config.dbg("Error setting FSAA: " + var3 + "x");
            var9.printStackTrace();

            try {
               Display.create((new PixelFormat()).withDepthBits(24));
            } catch (LWJGLException var8) {
               var8.printStackTrace();

               try {
                  Display.create();
               } catch (LWJGLException var7) {
                  var7.printStackTrace();
               }
            }
         }
      }

      this.texturePack = par1TexturePackList;
      this.options = par2GameSettings;
      Graphics var10 = this.missingTextureImage.getGraphics();
      var10.setColor(Color.WHITE);
      var10.fillRect(0, 0, 64, 64);
      var10.setColor(Color.BLACK);
      int var4 = 10;
      int var5 = 0;

      while(var4 < 64) {
         String var6 = var5++ % 2 == 0?"missing":"texture";
         var10.drawString(var6, 1, var4);
         var4 += var10.getFont().getSize();
         if(var5 % 2 == 0) {
            var4 += 5;
         }
      }

      var10.dispose();
      this.textureMapBlocks = new TextureMap(0, "terrain", "textures/blocks/", this.missingTextureImage);
      this.textureMapItems = new TextureMap(1, "items", "textures/items/", this.missingTextureImage);
   }

   public int[] getTextureContents(String par1Str) {
      ITexturePack var2 = this.texturePack.getSelectedTexturePack();
      int[] var3 = (int[])((int[])this.textureContentsMap.get(par1Str));
      if(var3 != null) {
         return var3;
      } else {
         int[] var5;
         try {
            InputStream var6 = var2.getResourceAsStream(par1Str);
            if(var6 == null) {
               var5 = this.getImageContentsAndAllocate(this.missingTextureImage);
            } else {
               var5 = this.getImageContentsAndAllocate(this.readTextureImage(var6));
            }

            this.textureContentsMap.put(par1Str, var5);
            return var5;
         } catch (IOException var61) {
            var61.printStackTrace();
            var5 = this.getImageContentsAndAllocate(this.missingTextureImage);
            this.textureContentsMap.put(par1Str, var5);
            return var5;
         }
      }
   }

   private int[] getImageContentsAndAllocate(BufferedImage par1BufferedImage) {
      return this.getImageContents(par1BufferedImage, new int[par1BufferedImage.getWidth() * par1BufferedImage.getHeight()]);
   }

   private int[] getImageContents(BufferedImage par1BufferedImage, int[] par2ArrayOfInteger) {
      int var3 = par1BufferedImage.getWidth();
      int var4 = par1BufferedImage.getHeight();
      par1BufferedImage.getRGB(0, 0, var3, var4, par2ArrayOfInteger, 0, var3);
      return par2ArrayOfInteger;
   }

   public void bindTexture(String par1Str) {
      this.bindTexture(this.getTexture(par1Str));
   }

   public void bindTexture(int par1) {
      if(par1 != this.boundTexture) {
         GL11.glBindTexture(3553, par1);
         this.boundTexture = par1;
      }

   }

   public void resetBoundTexture() {
      this.boundTexture = -1;
   }

   public int getTexture(String par1Str) {
      if(Config.isRandomMobs()) {
         par1Str = RandomMobs.getTexture(par1Str);
      }

      if(par1Str.equals("/terrain.png")) {
         this.textureMapBlocks.getTexture().bindTexture(0);
         return this.textureMapBlocks.getTexture().getGlTextureId();
      } else if(par1Str.equals("/gui/items.png")) {
         this.textureMapItems.getTexture().bindTexture(0);
         return this.textureMapItems.getTexture().getGlTextureId();
      } else {
         Integer var2 = (Integer)this.textureMap.get(par1Str);
         if(var2 != null) {
            return var2.intValue();
         } else {
            String var8 = par1Str;

            try {
               Reflector.callVoid(Reflector.ForgeHooksClient_onTextureLoadPre, new Object[]{par1Str});
               int var7 = GLAllocation.generateTextureNames();
               boolean var41 = par1Str.startsWith("%blur%");
               if(var41) {
                  par1Str = par1Str.substring(6);
               }

               boolean var5 = par1Str.startsWith("%clamp%");
               if(var5) {
                  par1Str = par1Str.substring(7);
               }

               InputStream var6 = this.texturePack.getSelectedTexturePack().getResourceAsStream(par1Str);
               if(var6 == null) {
                  this.setupTextureExt(this.missingTextureImage, var7, var41, var5);
               } else {
                  this.setupTextureExt(this.readTextureImage(var6), var7, var41, var5);
               }

               this.textureMap.put(var8, Integer.valueOf(var7));
               Reflector.callVoid(Reflector.ForgeHooksClient_onTextureLoad, new Object[]{par1Str, this.texturePack.getSelectedTexturePack()});
               return var7;
            } catch (Exception var81) {
               var81.printStackTrace();
               int var4 = GLAllocation.generateTextureNames();
               this.setupTexture(this.missingTextureImage, var4);
               this.textureMap.put(par1Str, Integer.valueOf(var4));
               return var4;
            }
         }
      }
   }

   public int allocateAndSetupTexture(BufferedImage par1BufferedImage) {
      int var2 = GLAllocation.generateTextureNames();
      this.setupTexture(par1BufferedImage, var2);
      this.textureNameToImageMap.addKey(var2, par1BufferedImage);
      return var2;
   }

   public void setupTexture(BufferedImage par1BufferedImage, int par2) {
      this.setupTextureExt(par1BufferedImage, par2, false, false);
   }

   public void setupTextureExt(BufferedImage par1BufferedImage, int par2, boolean par3, boolean par4) {
      this.bindTexture(par2);
      GL11.glTexParameteri(3553, 10241, 9728);
      GL11.glTexParameteri(3553, 10240, 9728);
      if(par3) {
         GL11.glTexParameteri(3553, 10241, 9729);
         GL11.glTexParameteri(3553, 10240, 9729);
      }

      if(par4) {
         GL11.glTexParameteri(3553, 10242, 10496);
         GL11.glTexParameteri(3553, 10243, 10496);
      } else {
         GL11.glTexParameteri(3553, 10242, 10497);
         GL11.glTexParameteri(3553, 10243, 10497);
      }

      int var5 = par1BufferedImage.getWidth();
      int var6 = par1BufferedImage.getHeight();
      int[] var7 = new int[var5 * var6];
      par1BufferedImage.getRGB(0, 0, var5, var6, var7, 0, var5);
      if(this.options != null && this.options.anaglyph) {
         var7 = this.colorToAnaglyph(var7);
      }

      this.fixTransparency(var7);
      this.checkImageDataSize(var7.length);
      this.imageData.clear();
      this.imageData.put(var7);
      this.imageData.position(0).limit(var7.length);
      GL11.glTexImage2D(3553, 0, 6408, var5, var6, 0, '\u80e1', '\u8367', this.imageData);
   }

   private int[] colorToAnaglyph(int[] par1ArrayOfInteger) {
      int[] var2 = new int[par1ArrayOfInteger.length];

      for(int var3 = 0; var3 < par1ArrayOfInteger.length; ++var3) {
         int var4 = par1ArrayOfInteger[var3] >> 24 & 255;
         int var5 = par1ArrayOfInteger[var3] >> 16 & 255;
         int var6 = par1ArrayOfInteger[var3] >> 8 & 255;
         int var7 = par1ArrayOfInteger[var3] & 255;
         int var8 = (var5 * 30 + var6 * 59 + var7 * 11) / 100;
         int var9 = (var5 * 30 + var6 * 70) / 100;
         int var10 = (var5 * 30 + var7 * 70) / 100;
         var2[var3] = var4 << 24 | var8 << 16 | var9 << 8 | var10;
      }

      return var2;
   }

   public void createTextureFromBytes(int[] par1ArrayOfInteger, int par2, int par3, int par4) {
      this.bindTexture(par4);
      GL11.glTexParameteri(3553, 10241, 9728);
      GL11.glTexParameteri(3553, 10240, 9728);
      GL11.glTexParameteri(3553, 10242, 10497);
      GL11.glTexParameteri(3553, 10243, 10497);
      if(this.options != null && this.options.anaglyph) {
         par1ArrayOfInteger = this.colorToAnaglyph(par1ArrayOfInteger);
      }

      this.checkImageDataSize(par1ArrayOfInteger.length);
      this.imageData.clear();
      this.imageData.put(par1ArrayOfInteger);
      this.imageData.position(0).limit(par1ArrayOfInteger.length);
      GL11.glTexSubImage2D(3553, 0, 0, 0, par2, par3, '\u80e1', '\u8367', this.imageData);
   }

   public void deleteTexture(int par1) {
      this.textureNameToImageMap.removeObject(par1);
      GL11.glDeleteTextures(par1);
   }

   public int getTextureForDownloadableImage(String par1Str, String par2Str) {
      ThreadDownloadImageData var3 = (ThreadDownloadImageData)this.urlToImageDataMap.get(par1Str);
      if(var3 != null && var3.image != null && !var3.textureSetupComplete) {
         if(var3.textureName < 0) {
            var3.textureName = this.allocateAndSetupTexture(var3.image);
         } else {
            this.setupTexture(var3.image, var3.textureName);
         }

         var3.textureSetupComplete = true;
      }

      return var3 != null && var3.textureName >= 0?var3.textureName:(par2Str == null?-1:this.getTexture(par2Str));
   }

   public boolean hasImageData(String par1Str) {
      return this.urlToImageDataMap.containsKey(par1Str);
   }

   public ThreadDownloadImageData obtainImageData(String par1Str, IImageBuffer par2IImageBuffer) {
      if(par1Str != null && par1Str.length() > 0 && Character.isDigit(par1Str.charAt(0))) {
         return null;
      } else {
         ThreadDownloadImageData var3 = (ThreadDownloadImageData)this.urlToImageDataMap.get(par1Str);
         if(var3 == null) {
            this.urlToImageDataMap.put(par1Str, new ThreadDownloadImageData(par1Str, par2IImageBuffer));
         } else {
            ++var3.referenceCount;
         }

         return var3;
      }
   }

   public void releaseImageData(String par1Str) {
      ThreadDownloadImageData var2 = (ThreadDownloadImageData)this.urlToImageDataMap.get(par1Str);
      if(var2 != null) {
         --var2.referenceCount;
         if(var2.referenceCount == 0) {
            if(var2.textureName >= 0) {
               this.deleteTexture(var2.textureName);
            }

            this.urlToImageDataMap.remove(par1Str);
         }
      }

   }

   public void updateDynamicTextures() {
      this.checkInitialized();
      this.textureMapBlocks.updateAnimations();
      this.textureMapItems.updateAnimations();
      TextureAnimations.updateCustomAnimations();
   }

   public void refreshTextures() {
      Config.dbg("*** Reloading textures ***");
      Config.log("Texture pack: \"" + this.texturePack.getSelectedTexturePack().getTexturePackFileName() + "\"");
      CustomSky.reset();
      TextureAnimations.reset();
      WrUpdates.finishCurrentUpdate();
      ITexturePack var1 = this.texturePack.getSelectedTexturePack();
      this.refreshTextureMaps();
      Iterator var2 = this.textureNameToImageMap.getKeySet().iterator();

      BufferedImage var4;
      while(var2.hasNext()) {
         int var10 = ((Integer)var2.next()).intValue();
         var4 = (BufferedImage)this.textureNameToImageMap.lookup(var10);
         this.setupTexture(var4, var10);
      }

      ThreadDownloadImageData var102;
      for(var2 = this.urlToImageDataMap.values().iterator(); var2.hasNext(); var102.textureSetupComplete = false) {
         var102 = (ThreadDownloadImageData)var2.next();
      }

      var2 = this.textureMap.keySet().iterator();

      String var11;
      while(var2.hasNext()) {
         var11 = (String)var2.next();

         try {
            int var8 = ((Integer)this.textureMap.get(var11)).intValue();
            boolean var6 = var11.startsWith("%blur%");
            if(var6) {
               var11 = var11.substring(6);
            }

            boolean var7 = var11.startsWith("%clamp%");
            if(var7) {
               var11 = var11.substring(7);
            }

            BufferedImage var5 = this.readTextureImage(var1.getResourceAsStream(var11));
            this.setupTextureExt(var5, var8, var6, var7);
         } catch (FileNotFoundException var12) {
            ;
         } catch (Exception var13) {
            if(!"input == null!".equals(var13.getMessage())) {
               var13.printStackTrace();
            }
         }
      }

      var2 = this.textureContentsMap.keySet().iterator();

      while(var2.hasNext()) {
         var11 = (String)var2.next();

         try {
            var4 = this.readTextureImage(var1.getResourceAsStream(var11));
            this.getImageContents(var4, (int[])((int[])this.textureContentsMap.get(var11)));
         } catch (FileNotFoundException var101) {
            ;
         } catch (Exception var111) {
            if(!"input == null!".equals(var111.getMessage())) {
               var111.printStackTrace();
            }
         }
      }

      Minecraft.getMinecraft().fontRenderer.readFontData();
      Minecraft.getMinecraft().standardGalacticFontRenderer.readFontData();
      TextureAnimations.update(this);
      CustomColorizer.update(this);
      CustomSky.update(this);
      RandomMobs.resetTextures();
      Config.updateTexturePackClouds();
      this.updateDynamicTextures();
   }

   private BufferedImage readTextureImage(InputStream par1InputStream) throws IOException {
      BufferedImage var2 = ImageIO.read(par1InputStream);
      par1InputStream.close();
      return var2;
   }

   public void refreshTextureMaps() {
      this.textureMapBlocks.refreshTextures();
      this.textureMapItems.refreshTextures();
      TextureUtils.update(this);
      NaturalTextures.update(this);
      ConnectedTextures.update(this);
   }

   public Icon getMissingIcon(int par1) {
      switch(par1) {
      case 0:
         return this.textureMapBlocks.getMissingIcon();
      case 1:
      default:
         return this.textureMapItems.getMissingIcon();
      }
   }

   public BufferedImage readTextureImage(String path) throws IOException {
      InputStream in = this.texturePack.getSelectedTexturePack().getResourceAsStream(path);
      if(in == null) {
         return null;
      } else {
         BufferedImage bi = ImageIO.read(in);
         in.close();
         return bi;
      }
   }

   public TexturePackList getTexturePack() {
      return this.texturePack;
   }

   public void checkInitialized() {
      if(!this.initialized) {
         Minecraft mc = Config.getMinecraft();
         if(mc != null) {
            this.initialized = true;
            Config.log("Texture pack: \"" + this.texturePack.getSelectedTexturePack().getTexturePackFileName() + "\"");
            CustomColorizer.update(this);
            CustomSky.update(this);
            TextureAnimations.update(this);
            Config.updateTexturePackClouds();
         }
      }
   }

   public void checkImageDataSize(int dataLen) {
      if(this.imageData == null || this.imageData.capacity() < dataLen) {
         dataLen = TextureUtils.ceilPowerOfTwo(dataLen);
         this.imageData = GLAllocation.createDirectIntBuffer(dataLen);
      }
   }

   private void fixTransparency(int[] data) {
      for(int i = 0; i < data.length; ++i) {
         int alpha = data[i] >> 24 & 255;
         if(alpha == 0) {
            data[i] = 0;
         }
      }

   }

   public void refreshBlockTextures() {
      Config.dbg("*** Reloading block textures ***");
      WrUpdates.finishCurrentUpdate();
      this.textureMapBlocks.refreshTextures();
      TextureUtils.update(this);
      NaturalTextures.update(this);
      ConnectedTextures.update(this);
      this.updateDynamicTextures();
   }

}
