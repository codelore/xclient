package net.minecraft.client.renderer;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.BlockChest;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.particle.EntityAuraFX;
import net.minecraft.client.particle.EntityBreakingFX;
import net.minecraft.client.particle.EntityBubbleFX;
import net.minecraft.client.particle.EntityCloudFX;
import net.minecraft.client.particle.EntityCritFX;
import net.minecraft.client.particle.EntityDiggingFX;
import net.minecraft.client.particle.EntityDropParticleFX;
import net.minecraft.client.particle.EntityEnchantmentTableParticleFX;
import net.minecraft.client.particle.EntityExplodeFX;
import net.minecraft.client.particle.EntityFX;
import net.minecraft.client.particle.EntityFireworkSparkFX;
import net.minecraft.client.particle.EntityFlameFX;
import net.minecraft.client.particle.EntityFootStepFX;
import net.minecraft.client.particle.EntityHeartFX;
import net.minecraft.client.particle.EntityHugeExplodeFX;
import net.minecraft.client.particle.EntityLargeExplodeFX;
import net.minecraft.client.particle.EntityLavaFX;
import net.minecraft.client.particle.EntityNoteFX;
import net.minecraft.client.particle.EntityPortalFX;
import net.minecraft.client.particle.EntityReddustFX;
import net.minecraft.client.particle.EntitySmokeFX;
import net.minecraft.client.particle.EntitySnowShovelFX;
import net.minecraft.client.particle.EntitySpellParticleFX;
import net.minecraft.client.particle.EntitySplashFX;
import net.minecraft.client.particle.EntitySuspendFX;
import net.minecraft.client.renderer.CallableParticlePositionInfo;
import net.minecraft.client.renderer.DestroyBlockProgress;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.renderer.EntitySorter;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.ImageBufferDownload;
import net.minecraft.client.renderer.OpenGlCapsChecker;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.RenderEngine;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.ThreadDownloadImage;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.culling.ICamera;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.item.EntityItemFrame;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemRecord;
import net.minecraft.item.ItemStack;
import net.minecraft.profiler.Profiler;
import net.minecraft.src.CompactArrayList;
import net.minecraft.src.Config;
import net.minecraft.src.CustomColorizer;
import net.minecraft.src.CustomSky;
import net.minecraft.src.RandomMobs;
import net.minecraft.src.Reflector;
import net.minecraft.src.WrUpdates;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.EnumMovingObjectType;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.ReportedException;
import net.minecraft.util.StringUtils;
import net.minecraft.util.Vec3;
import net.minecraft.world.IWorldAccess;
import net.minecraft.world.WorldProvider;
import org.lwjgl.BufferUtils;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.ARBOcclusionQuery;
import org.lwjgl.opengl.GL11;

public class RenderGlobal implements IWorldAccess {

   public List tileEntities = new ArrayList();
   public WorldClient theWorld;
   public final RenderEngine renderEngine;
   public CompactArrayList worldRenderersToUpdate = new CompactArrayList(100, 0.8F);
   private WorldRenderer[] sortedWorldRenderers;
   private WorldRenderer[] worldRenderers;
   private int renderChunksWide;
   private int renderChunksTall;
   private int renderChunksDeep;
   private int glRenderListBase;
   public Minecraft mc;
   public RenderBlocks globalRenderBlocks;
   private IntBuffer glOcclusionQueryBase;
   private boolean occlusionEnabled = false;
   private int cloudTickCounter = 0;
   private int starGLCallList;
   private int glSkyList;
   private int glSkyList2;
   private int minBlockX;
   private int minBlockY;
   private int minBlockZ;
   private int maxBlockX;
   private int maxBlockY;
   private int maxBlockZ;
   public Map damagedBlocks = new HashMap();
   private Icon[] destroyBlockIcons;
   private int renderDistance = -1;
   private int renderEntitiesStartupCounter = 2;
   private int countEntitiesTotal;
   private int countEntitiesRendered;
   private int countEntitiesHidden;
   int[] dummyBuf50k = new int['\uc350'];
   IntBuffer occlusionResult = GLAllocation.createDirectIntBuffer(64);
   private int renderersLoaded;
   private int renderersBeingClipped;
   private int renderersBeingOccluded;
   private int renderersBeingRendered;
   private int renderersSkippingRenderPass;
   private int dummyRenderInt;
   private int worldRenderersCheckIndex;
   private IntBuffer glListBuffer = BufferUtils.createIntBuffer(65536);
   double prevSortX = -9999.0D;
   double prevSortY = -9999.0D;
   double prevSortZ = -9999.0D;
   int frustumCheckOffset = 0;
   double prevReposX;
   double prevReposY;
   double prevReposZ;
   public Entity renderedEntity;
   private long lastMovedTime = System.currentTimeMillis();
   private long lastActionTime = System.currentTimeMillis();


   public RenderGlobal(Minecraft par1Minecraft, RenderEngine par2RenderEngine) {
      this.mc = par1Minecraft;
      this.renderEngine = par2RenderEngine;
      byte maxChunkDim = 65;
      byte maxChunkHeight = 16;
      this.glRenderListBase = GLAllocation.generateDisplayLists(maxChunkDim * maxChunkDim * maxChunkHeight * 3);
      this.occlusionEnabled = OpenGlCapsChecker.checkARBOcclusion();
      if(this.occlusionEnabled) {
         this.occlusionResult.clear();
         this.glOcclusionQueryBase = GLAllocation.createDirectIntBuffer(maxChunkDim * maxChunkDim * maxChunkHeight);
         this.glOcclusionQueryBase.clear();
         this.glOcclusionQueryBase.position(0);
         this.glOcclusionQueryBase.limit(maxChunkDim * maxChunkDim * maxChunkHeight);
         ARBOcclusionQuery.glGenQueriesARB(this.glOcclusionQueryBase);
      }

      this.starGLCallList = GLAllocation.generateDisplayLists(3);
      GL11.glPushMatrix();
      GL11.glNewList(this.starGLCallList, 4864);
      this.renderStars();
      GL11.glEndList();
      GL11.glPopMatrix();
      Tessellator var5 = Tessellator.instance;
      this.glSkyList = this.starGLCallList + 1;
      GL11.glNewList(this.glSkyList, 4864);
      byte var7 = 64;
      int var8 = 256 / var7 + 2;
      float var6 = 16.0F;

      int var9;
      int var10;
      for(var9 = -var7 * var8; var9 <= var7 * var8; var9 += var7) {
         for(var10 = -var7 * var8; var10 <= var7 * var8; var10 += var7) {
            var5.startDrawingQuads();
            var5.addVertex((double)(var9 + 0), (double)var6, (double)(var10 + 0));
            var5.addVertex((double)(var9 + var7), (double)var6, (double)(var10 + 0));
            var5.addVertex((double)(var9 + var7), (double)var6, (double)(var10 + var7));
            var5.addVertex((double)(var9 + 0), (double)var6, (double)(var10 + var7));
            var5.draw();
         }
      }

      GL11.glEndList();
      this.glSkyList2 = this.starGLCallList + 2;
      GL11.glNewList(this.glSkyList2, 4864);
      var6 = -16.0F;
      var5.startDrawingQuads();

      for(var9 = -var7 * var8; var9 <= var7 * var8; var9 += var7) {
         for(var10 = -var7 * var8; var10 <= var7 * var8; var10 += var7) {
            var5.addVertex((double)(var9 + var7), (double)var6, (double)(var10 + 0));
            var5.addVertex((double)(var9 + 0), (double)var6, (double)(var10 + 0));
            var5.addVertex((double)(var9 + 0), (double)var6, (double)(var10 + var7));
            var5.addVertex((double)(var9 + var7), (double)var6, (double)(var10 + var7));
         }
      }

      var5.draw();
      GL11.glEndList();
      this.renderEngine.updateDynamicTextures();
   }

   private void renderStars() {
      Random var1 = new Random(10842L);
      Tessellator var2 = Tessellator.instance;
      var2.startDrawingQuads();

      for(int var3 = 0; var3 < 1500; ++var3) {
         double var4 = (double)(var1.nextFloat() * 2.0F - 1.0F);
         double var6 = (double)(var1.nextFloat() * 2.0F - 1.0F);
         double var8 = (double)(var1.nextFloat() * 2.0F - 1.0F);
         double var10 = (double)(0.15F + var1.nextFloat() * 0.1F);
         double var12 = var4 * var4 + var6 * var6 + var8 * var8;
         if(var12 < 1.0D && var12 > 0.01D) {
            var12 = 1.0D / Math.sqrt(var12);
            var4 *= var12;
            var6 *= var12;
            var8 *= var12;
            double var14 = var4 * 100.0D;
            double var16 = var6 * 100.0D;
            double var18 = var8 * 100.0D;
            double var20 = Math.atan2(var4, var8);
            double var22 = Math.sin(var20);
            double var24 = Math.cos(var20);
            double var26 = Math.atan2(Math.sqrt(var4 * var4 + var8 * var8), var6);
            double var28 = Math.sin(var26);
            double var30 = Math.cos(var26);
            double var32 = var1.nextDouble() * 3.141592653589793D * 2.0D;
            double var34 = Math.sin(var32);
            double var36 = Math.cos(var32);

            for(int var38 = 0; var38 < 4; ++var38) {
               double var39 = 0.0D;
               double var41 = (double)((var38 & 2) - 1) * var10;
               double var43 = (double)((var38 + 1 & 2) - 1) * var10;
               double var47 = var41 * var36 - var43 * var34;
               double var49 = var43 * var36 + var41 * var34;
               double var53 = var47 * var28 + var39 * var30;
               double var55 = var39 * var28 - var47 * var30;
               double var57 = var55 * var22 - var49 * var24;
               double var61 = var49 * var22 + var55 * var24;
               var2.addVertex(var14 + var57, var16 + var53, var18 + var61);
            }
         }
      }

      var2.draw();
   }

   public void setWorldAndLoadRenderers(WorldClient par1WorldClient) {
      if(this.theWorld != null) {
         this.theWorld.removeWorldAccess(this);
      }

      this.prevSortX = -9999.0D;
      this.prevSortY = -9999.0D;
      this.prevSortZ = -9999.0D;
      RenderManager.instance.set(par1WorldClient);
      this.theWorld = par1WorldClient;
      this.globalRenderBlocks = new RenderBlocks(par1WorldClient);
      if(par1WorldClient != null) {
         par1WorldClient.addWorldAccess(this);
         this.loadRenderers();
      }

   }

   public void loadRenderers() {
      if(this.theWorld != null) {
         Block.leaves.setGraphicsLevel(Config.isTreesFancy());
         this.renderDistance = this.mc.gameSettings.renderDistance;
         int numBlocks;
         if(this.worldRenderers != null) {
            for(numBlocks = 0; numBlocks < this.worldRenderers.length; ++numBlocks) {
               this.worldRenderers[numBlocks].stopRendering();
            }
         }

         numBlocks = 64 << 3 - this.renderDistance;
         short numBlocksFar = 512;
         numBlocks = 2 * this.mc.gameSettings.ofRenderDistanceFine;
         if(Config.isLoadChunksFar() && numBlocks < numBlocksFar) {
            numBlocks = numBlocksFar;
         }

         numBlocks += Config.getPreloadedChunks() * 2 * 16;
         short limit = 400;
         if(this.mc.gameSettings.ofRenderDistanceFine > 256) {
            limit = 1024;
         }

         if(numBlocks > limit) {
            numBlocks = limit;
         }

         this.prevReposX = -9999.0D;
         this.prevReposY = -9999.0D;
         this.prevReposZ = -9999.0D;
         this.renderChunksWide = numBlocks / 16 + 1;
         this.renderChunksTall = 16;
         this.renderChunksDeep = numBlocks / 16 + 1;
         this.worldRenderers = new WorldRenderer[this.renderChunksWide * this.renderChunksTall * this.renderChunksDeep];
         this.sortedWorldRenderers = new WorldRenderer[this.renderChunksWide * this.renderChunksTall * this.renderChunksDeep];
         int var2 = 0;
         int var3 = 0;
         this.minBlockX = 0;
         this.minBlockY = 0;
         this.minBlockZ = 0;
         this.maxBlockX = this.renderChunksWide;
         this.maxBlockY = this.renderChunksTall;
         this.maxBlockZ = this.renderChunksDeep;

         int var10;
         for(var10 = 0; var10 < this.worldRenderersToUpdate.size(); ++var10) {
            WorldRenderer cy = (WorldRenderer)this.worldRenderersToUpdate.get(var10);
            if(cy != null) {
               cy.needsUpdate = false;
            }
         }

         this.worldRenderersToUpdate.clear();
         this.tileEntities.clear();

         for(var10 = 0; var10 < this.renderChunksWide; ++var10) {
            for(int var12 = 0; var12 < this.renderChunksTall; ++var12) {
               for(int cz = 0; cz < this.renderChunksDeep; ++cz) {
                  int wri = (cz * this.renderChunksTall + var12) * this.renderChunksWide + var10;
                  this.worldRenderers[wri] = WrUpdates.makeWorldRenderer(this.theWorld, this.tileEntities, var10 * 16, var12 * 16, cz * 16, this.glRenderListBase + var2);
                  if(this.occlusionEnabled) {
                     this.worldRenderers[wri].glOcclusionQuery = this.glOcclusionQueryBase.get(var3);
                  }

                  this.worldRenderers[wri].isWaitingOnOcclusionQuery = false;
                  this.worldRenderers[wri].isVisible = true;
                  this.worldRenderers[wri].isInFrustum = false;
                  this.worldRenderers[wri].chunkIndex = var3++;
                  this.sortedWorldRenderers[wri] = this.worldRenderers[wri];
                  if(this.theWorld.chunkExists(var10, cz)) {
                     this.worldRenderers[wri].markDirty();
                     this.worldRenderersToUpdate.add(this.worldRenderers[wri]);
                  }

                  var2 += 3;
               }
            }
         }

         if(this.theWorld != null) {
            Object var11 = this.mc.renderViewEntity;
            if(var11 == null) {
               var11 = this.mc.thePlayer;
            }

            if(var11 != null) {
               this.markRenderersForNewPosition(MathHelper.floor_double(((Entity)var11).posX), MathHelper.floor_double(((Entity)var11).posY), MathHelper.floor_double(((Entity)var11).posZ));
               Arrays.sort(this.sortedWorldRenderers, new EntitySorter((Entity)var11));
            }
         }

         this.renderEntitiesStartupCounter = 2;
      }

   }

   public void renderEntities(Vec3 par1Vec3, ICamera par2ICamera, float par3) {
      if(this.renderEntitiesStartupCounter > 0) {
         --this.renderEntitiesStartupCounter;
      } else {
         this.theWorld.theProfiler.startSection("prepare");
         TileEntityRenderer.instance.cacheActiveRenderInfo(this.theWorld, this.renderEngine, this.mc.fontRenderer, this.mc.renderViewEntity, par3);
         RenderManager.instance.cacheActiveRenderInfo(this.theWorld, this.renderEngine, this.mc.fontRenderer, this.mc.renderViewEntity, this.mc.pointedEntityLiving, this.mc.gameSettings, par3);
         this.countEntitiesTotal = 0;
         this.countEntitiesRendered = 0;
         this.countEntitiesHidden = 0;
         EntityLiving var4 = this.mc.renderViewEntity;
         RenderManager.renderPosX = var4.lastTickPosX + (var4.posX - var4.lastTickPosX) * (double)par3;
         RenderManager.renderPosY = var4.lastTickPosY + (var4.posY - var4.lastTickPosY) * (double)par3;
         RenderManager.renderPosZ = var4.lastTickPosZ + (var4.posZ - var4.lastTickPosZ) * (double)par3;
         TileEntityRenderer.staticPlayerX = var4.lastTickPosX + (var4.posX - var4.lastTickPosX) * (double)par3;
         TileEntityRenderer.staticPlayerY = var4.lastTickPosY + (var4.posY - var4.lastTickPosY) * (double)par3;
         TileEntityRenderer.staticPlayerZ = var4.lastTickPosZ + (var4.posZ - var4.lastTickPosZ) * (double)par3;
         this.mc.entityRenderer.enableLightmap((double)par3);
         this.theWorld.theProfiler.endStartSection("global");
         List var5 = this.theWorld.getLoadedEntityList();
         this.countEntitiesTotal = var5.size();

         int var6;
         Entity var7;
         for(var6 = 0; var6 < this.theWorld.weatherEffects.size(); ++var6) {
            var7 = (Entity)this.theWorld.weatherEffects.get(var6);
            ++this.countEntitiesRendered;
            if(var7.isInRangeToRenderVec3D(par1Vec3)) {
               RenderManager.instance.renderEntity(var7, par3);
            }
         }

         this.theWorld.theProfiler.endStartSection("entities");
         boolean oldFancyGraphics = this.mc.gameSettings.fancyGraphics;
         this.mc.gameSettings.fancyGraphics = Config.isDroppedItemsFancy();

         for(var6 = 0; var6 < var5.size(); ++var6) {
            var7 = (Entity)var5.get(var6);
            if(var7.isInRangeToRenderVec3D(par1Vec3) && (var7.ignoreFrustumCheck || par2ICamera.isBoundingBoxInFrustum(var7.boundingBox) || var7.riddenByEntity == this.mc.thePlayer) && (var7 != this.mc.renderViewEntity || this.mc.gameSettings.thirdPersonView != 0 || this.mc.renderViewEntity.isPlayerSleeping()) && this.theWorld.blockExists(MathHelper.floor_double(var7.posX), 0, MathHelper.floor_double(var7.posZ))) {
               ++this.countEntitiesRendered;
               if(var7.getClass() == EntityItemFrame.class) {
                  var7.renderDistanceWeight = 0.06D;
               }

               this.renderedEntity = var7;
               RenderManager.instance.renderEntity(var7, par3);
               this.renderedEntity = null;
            }
         }

         this.mc.gameSettings.fancyGraphics = oldFancyGraphics;
         this.theWorld.theProfiler.endStartSection("tileentities");
         RenderHelper.enableStandardItemLighting();

         for(var6 = 0; var6 < this.tileEntities.size(); ++var6) {
            TileEntity te = (TileEntity)this.tileEntities.get(var6);
            Class teClass = te.getClass();
            if(teClass == TileEntitySign.class && !Config.zoomMode) {
               EntityClientPlayerMP blockId = this.mc.thePlayer;
               double block = te.getDistanceFrom(blockId.posX, blockId.posY, blockId.posZ);
               if(block > 256.0D) {
                  FontRenderer fr = TileEntityRenderer.instance.getFontRenderer();
                  fr.enabled = false;
                  TileEntityRenderer.instance.renderTileEntity(te, par3);
                  fr.enabled = true;
                  continue;
               }
            }

            if(teClass == TileEntityChest.class) {
               int var15 = this.theWorld.getBlockId(te.xCoord, te.yCoord, te.zCoord);
               Block var16 = Block.blocksList[var15];
               if(!(var16 instanceof BlockChest)) {
                  continue;
               }
            }

            TileEntityRenderer.instance.renderTileEntity(te, par3);
         }

         this.mc.entityRenderer.disableLightmap((double)par3);
         this.theWorld.theProfiler.endSection();
      }

   }

   public String getDebugInfoRenders() {
      return "C: " + this.renderersBeingRendered + "/" + this.renderersLoaded + ". F: " + this.renderersBeingClipped + ", O: " + this.renderersBeingOccluded + ", E: " + this.renderersSkippingRenderPass;
   }

   public String getDebugInfoEntities() {
      return "E: " + this.countEntitiesRendered + "/" + this.countEntitiesTotal + ". B: " + this.countEntitiesHidden + ", I: " + (this.countEntitiesTotal - this.countEntitiesHidden - this.countEntitiesRendered) + ", " + Config.getVersion();
   }

   private void markRenderersForNewPosition(int x, int y, int z) {
      x -= 8;
      y -= 8;
      z -= 8;
      this.minBlockX = Integer.MAX_VALUE;
      this.minBlockY = Integer.MAX_VALUE;
      this.minBlockZ = Integer.MAX_VALUE;
      this.maxBlockX = Integer.MIN_VALUE;
      this.maxBlockY = Integer.MIN_VALUE;
      this.maxBlockZ = Integer.MIN_VALUE;
      int blocksWide = this.renderChunksWide * 16;
      int blocksWide2 = blocksWide / 2;

      for(int ix = 0; ix < this.renderChunksWide; ++ix) {
         int blockX = ix * 16;
         int blockXAbs = blockX + blocksWide2 - x;
         if(blockXAbs < 0) {
            blockXAbs -= blocksWide - 1;
         }

         blockXAbs /= blocksWide;
         blockX -= blockXAbs * blocksWide;
         if(blockX < this.minBlockX) {
            this.minBlockX = blockX;
         }

         if(blockX > this.maxBlockX) {
            this.maxBlockX = blockX;
         }

         for(int iz = 0; iz < this.renderChunksDeep; ++iz) {
            int blockZ = iz * 16;
            int blockZAbs = blockZ + blocksWide2 - z;
            if(blockZAbs < 0) {
               blockZAbs -= blocksWide - 1;
            }

            blockZAbs /= blocksWide;
            blockZ -= blockZAbs * blocksWide;
            if(blockZ < this.minBlockZ) {
               this.minBlockZ = blockZ;
            }

            if(blockZ > this.maxBlockZ) {
               this.maxBlockZ = blockZ;
            }

            for(int iy = 0; iy < this.renderChunksTall; ++iy) {
               int blockY = iy * 16;
               if(blockY < this.minBlockY) {
                  this.minBlockY = blockY;
               }

               if(blockY > this.maxBlockY) {
                  this.maxBlockY = blockY;
               }

               WorldRenderer worldrenderer = this.worldRenderers[(iz * this.renderChunksTall + iy) * this.renderChunksWide + ix];
               boolean wasNeedingUpdate = worldrenderer.needsUpdate;
               worldrenderer.setPosition(blockX, blockY, blockZ);
               if(!wasNeedingUpdate && worldrenderer.needsUpdate) {
                  this.worldRenderersToUpdate.add(worldrenderer);
               }
            }
         }
      }

   }

   public int sortAndRender(EntityLiving player, int renderPass, double partialTicks) {
      Profiler profiler = this.theWorld.theProfiler;
      profiler.startSection("sortchunks");
      if(this.worldRenderersToUpdate.size() < 10) {
         byte partialX = 10;

         for(int i = 0; i < partialX; ++i) {
            this.worldRenderersCheckIndex = (this.worldRenderersCheckIndex + 1) % this.worldRenderers.length;
            WorldRenderer partialY = this.worldRenderers[this.worldRenderersCheckIndex];
            if(partialY.needsUpdate && !this.worldRenderersToUpdate.contains(partialY)) {
               this.worldRenderersToUpdate.add(partialY);
            }
         }
      }

      if(this.mc.gameSettings.renderDistance != this.renderDistance && !Config.isLoadChunksFar()) {
         this.loadRenderers();
      }

      if(renderPass == 0) {
         this.renderersLoaded = 0;
         this.dummyRenderInt = 0;
         this.renderersBeingClipped = 0;
         this.renderersBeingOccluded = 0;
         this.renderersBeingRendered = 0;
         this.renderersSkippingRenderPass = 0;
      }

      double var40 = player.lastTickPosX + (player.posX - player.lastTickPosX) * partialTicks;
      double var41 = player.lastTickPosY + (player.posY - player.lastTickPosY) * partialTicks;
      double partialZ = player.lastTickPosZ + (player.posZ - player.lastTickPosZ) * partialTicks;
      double dSortX = player.posX - this.prevSortX;
      double dSortY = player.posY - this.prevSortY;
      double dSortZ = player.posZ - this.prevSortZ;
      double distSqSort = dSortX * dSortX + dSortY * dSortY + dSortZ * dSortZ;
      int num;
      if(distSqSort > 16.0D) {
         this.prevSortX = player.posX;
         this.prevSortY = player.posY;
         this.prevSortZ = player.posZ;
         num = Config.getPreloadedChunks() * 16;
         double ocReq = player.posX - this.prevReposX;
         double lastIndex = player.posY - this.prevReposY;
         double stepNum = player.posZ - this.prevReposZ;
         double switchStep = ocReq * ocReq + lastIndex * lastIndex + stepNum * stepNum;
         if(switchStep > (double)(num * num) + 16.0D) {
            this.prevReposX = player.posX;
            this.prevReposY = player.posY;
            this.prevReposZ = player.posZ;
            this.markRenderersForNewPosition(MathHelper.floor_double(player.posX), MathHelper.floor_double(player.posY), MathHelper.floor_double(player.posZ));
         }

         Arrays.sort(this.sortedWorldRenderers, new EntitySorter(player));
         int sumTX = (int)player.posX;
         int sumTY = (int)player.posZ;
         short sumTZ = 2000;
         if(Math.abs(sumTX - WorldRenderer.globalChunkOffsetX) > sumTZ || Math.abs(sumTY - WorldRenderer.globalChunkOffsetZ) > sumTZ) {
            WorldRenderer.globalChunkOffsetX = sumTX;
            WorldRenderer.globalChunkOffsetZ = sumTY;
            this.loadRenderers();
         }
      }

      RenderHelper.disableStandardItemLighting();
      WrUpdates.preRender(this, player);
      if(this.mc.gameSettings.ofSmoothFps && renderPass == 0) {
         GL11.glFinish();
      }

      byte var42 = 0;
      int var44 = 0;
      if(this.occlusionEnabled && this.mc.gameSettings.advancedOpengl && !this.mc.gameSettings.anaglyph && renderPass == 0) {
         byte firstIndex = 0;
         byte var43 = 20;
         this.checkOcclusionQueryResult(firstIndex, var43, player.posX, player.posY, player.posZ);

         int endIndex;
         for(endIndex = firstIndex; endIndex < var43; ++endIndex) {
            this.sortedWorldRenderers[endIndex].isVisible = true;
         }

         profiler.endStartSection("render");
         num = var42 + this.renderSortedRenderers(firstIndex, var43, renderPass, partialTicks);
         endIndex = var43;
         int var45 = 0;
         byte step = 40;

         int startIndex;
         for(int var46 = this.renderChunksWide; endIndex < this.sortedWorldRenderers.length; num += this.renderSortedRenderers(startIndex, endIndex, renderPass, partialTicks)) {
            profiler.endStartSection("occ");
            startIndex = endIndex;
            if(var45 < var46) {
               ++var45;
            } else {
               --var45;
            }

            endIndex += var45 * step;
            if(endIndex <= startIndex) {
               endIndex = startIndex + 10;
            }

            if(endIndex > this.sortedWorldRenderers.length) {
               endIndex = this.sortedWorldRenderers.length;
            }

            GL11.glDisable(3553);
            GL11.glDisable(2896);
            GL11.glDisable(3008);
            GL11.glDisable(2912);
            GL11.glColorMask(false, false, false, false);
            GL11.glDepthMask(false);
            profiler.startSection("check");
            this.checkOcclusionQueryResult(startIndex, endIndex, player.posX, player.posY, player.posZ);
            profiler.endSection();
            GL11.glPushMatrix();
            float var49 = 0.0F;
            float var47 = 0.0F;
            float var48 = 0.0F;

            for(int k = startIndex; k < endIndex; ++k) {
               WorldRenderer wr = this.sortedWorldRenderers[k];
               if(wr.skipAllRenderPasses()) {
                  wr.isInFrustum = false;
               } else if(wr.isUpdating) {
                  wr.isVisible = true;
               } else if(wr.isInFrustum) {
                  if(Config.isOcclusionFancy() && !wr.isInFrustrumFully) {
                     wr.isVisible = true;
                  } else if(wr.isInFrustum && !wr.isWaitingOnOcclusionQuery) {
                     float bbX;
                     float bbY;
                     float bbZ;
                     float tX;
                     if(wr.isVisibleFromPosition) {
                        bbX = Math.abs((float)(wr.visibleFromX - player.posX));
                        bbY = Math.abs((float)(wr.visibleFromY - player.posY));
                        bbZ = Math.abs((float)(wr.visibleFromZ - player.posZ));
                        tX = bbX + bbY + bbZ;
                        if((double)tX < 10.0D + (double)k / 1000.0D) {
                           wr.isVisible = true;
                           continue;
                        }

                        wr.isVisibleFromPosition = false;
                     }

                     bbX = (float)((double)wr.posXMinus - var40);
                     bbY = (float)((double)wr.posYMinus - var41);
                     bbZ = (float)((double)wr.posZMinus - partialZ);
                     tX = bbX - var49;
                     float tY = bbY - var47;
                     float tZ = bbZ - var48;
                     if(tX != 0.0F || tY != 0.0F || tZ != 0.0F) {
                        GL11.glTranslatef(tX, tY, tZ);
                        var49 += tX;
                        var47 += tY;
                        var48 += tZ;
                     }

                     profiler.startSection("bb");
                     ARBOcclusionQuery.glBeginQueryARB('\u8914', wr.glOcclusionQuery);
                     wr.callOcclusionQueryList();
                     ARBOcclusionQuery.glEndQueryARB('\u8914');
                     profiler.endSection();
                     wr.isWaitingOnOcclusionQuery = true;
                     ++var44;
                  }
               }
            }

            GL11.glPopMatrix();
            if(this.mc.gameSettings.anaglyph) {
               if(EntityRenderer.anaglyphField == 0) {
                  GL11.glColorMask(false, true, true, true);
               } else {
                  GL11.glColorMask(true, false, false, true);
               }
            } else {
               GL11.glColorMask(true, true, true, true);
            }

            GL11.glDepthMask(true);
            GL11.glEnable(3553);
            GL11.glEnable(3008);
            GL11.glEnable(2912);
            profiler.endStartSection("render");
         }
      } else {
         profiler.endStartSection("render");
         num = var42 + this.renderSortedRenderers(0, this.sortedWorldRenderers.length, renderPass, partialTicks);
      }

      profiler.endSection();
      WrUpdates.postRender();
      return num;
   }

   private void checkOcclusionQueryResult(int startIndex, int endIndex, double px, double py, double pz) {
      for(int k = startIndex; k < endIndex; ++k) {
         WorldRenderer wr = this.sortedWorldRenderers[k];
         if(wr.isWaitingOnOcclusionQuery) {
            this.occlusionResult.clear();
            ARBOcclusionQuery.glGetQueryObjectuARB(wr.glOcclusionQuery, '\u8867', this.occlusionResult);
            if(this.occlusionResult.get(0) != 0) {
               wr.isWaitingOnOcclusionQuery = false;
               this.occlusionResult.clear();
               ARBOcclusionQuery.glGetQueryObjectuARB(wr.glOcclusionQuery, '\u8866', this.occlusionResult);
               boolean wasVisible = wr.isVisible;
               wr.isVisible = this.occlusionResult.get(0) > 0;
               if(wasVisible && wr.isVisible) {
                  wr.isVisibleFromPosition = true;
                  wr.visibleFromX = px;
                  wr.visibleFromY = py;
                  wr.visibleFromZ = pz;
               }
            }
         }
      }

   }

   private int renderSortedRenderers(int startIndex, int endIndex, int renderPass, double partialTicks) {
      this.glListBuffer.clear();
      int l = 0;

      for(int entityliving = startIndex; entityliving < endIndex; ++entityliving) {
         WorldRenderer partialX = this.sortedWorldRenderers[entityliving];
         if(renderPass == 0) {
            ++this.renderersLoaded;
            if(partialX.skipRenderPass[renderPass]) {
               ++this.renderersSkippingRenderPass;
            } else if(!partialX.isInFrustum) {
               ++this.renderersBeingClipped;
            } else if(this.occlusionEnabled && !partialX.isVisible) {
               ++this.renderersBeingOccluded;
            } else {
               ++this.renderersBeingRendered;
            }
         }

         if(partialX.isInFrustum && !partialX.skipRenderPass[renderPass] && (!this.occlusionEnabled || partialX.isVisible)) {
            int glCallList = partialX.getGLCallListForPass(renderPass);
            if(glCallList >= 0) {
               this.glListBuffer.put(glCallList);
               ++l;
            }
         }
      }

      if(l == 0) {
         return 0;
      } else {
         if(Config.isFogOff()) {
            GL11.glDisable(2912);
         }

         this.glListBuffer.flip();
         EntityLiving var14 = this.mc.renderViewEntity;
         double var15 = var14.lastTickPosX + (var14.posX - var14.lastTickPosX) * partialTicks - (double)WorldRenderer.globalChunkOffsetX;
         double partialY = var14.lastTickPosY + (var14.posY - var14.lastTickPosY) * partialTicks;
         double partialZ = var14.lastTickPosZ + (var14.posZ - var14.lastTickPosZ) * partialTicks - (double)WorldRenderer.globalChunkOffsetZ;
         this.mc.entityRenderer.enableLightmap(partialTicks);
         GL11.glTranslatef((float)(-var15), (float)(-partialY), (float)(-partialZ));
         GL11.glCallLists(this.glListBuffer);
         GL11.glTranslatef((float)var15, (float)partialY, (float)partialZ);
         this.mc.entityRenderer.disableLightmap(partialTicks);
         return l;
      }
   }

   public void renderAllRenderLists(int par1, double par2) {}

   public void updateClouds() {
      ++this.cloudTickCounter;
      if(this.cloudTickCounter % 20 == 0) {
         Iterator var1 = this.damagedBlocks.values().iterator();

         while(var1.hasNext()) {
            DestroyBlockProgress var2 = (DestroyBlockProgress)var1.next();
            int var3 = var2.getCreationCloudUpdateTick();
            if(this.cloudTickCounter - var3 > 400) {
               var1.remove();
            }
         }
      }

   }

   public void renderSky(float par1) {
      if(Reflector.ForgeWorldProvider_getSkyRenderer.exists()) {
         WorldProvider var2 = this.mc.theWorld.provider;
         Object var3 = Reflector.call(var2, Reflector.ForgeWorldProvider_getSkyRenderer, new Object[0]);
         if(var3 != null) {
            Reflector.callVoid(var3, Reflector.IRenderHandler_render, new Object[]{Float.valueOf(par1), this.theWorld, this.mc});
            return;
         }
      }

      if(this.mc.theWorld.provider.dimensionId == 1) {
         if(!Config.isSkyEnabled()) {
            return;
         }

         GL11.glDisable(2912);
         GL11.glDisable(3008);
         GL11.glEnable(3042);
         GL11.glBlendFunc(770, 771);
         RenderHelper.disableStandardItemLighting();
         GL11.glDepthMask(false);
         this.renderEngine.bindTexture("/misc/tunnel.png");
         Tessellator var201 = Tessellator.instance;

         for(int var22 = 0; var22 < 6; ++var22) {
            GL11.glPushMatrix();
            if(var22 == 1) {
               GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
            }

            if(var22 == 2) {
               GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
            }

            if(var22 == 3) {
               GL11.glRotatef(180.0F, 1.0F, 0.0F, 0.0F);
            }

            if(var22 == 4) {
               GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
            }

            if(var22 == 5) {
               GL11.glRotatef(-90.0F, 0.0F, 0.0F, 1.0F);
            }

            var201.startDrawingQuads();
            var201.setColorOpaque_I(2631720);
            var201.addVertexWithUV(-100.0D, -100.0D, -100.0D, 0.0D, 0.0D);
            var201.addVertexWithUV(-100.0D, -100.0D, 100.0D, 0.0D, 16.0D);
            var201.addVertexWithUV(100.0D, -100.0D, 100.0D, 16.0D, 16.0D);
            var201.addVertexWithUV(100.0D, -100.0D, -100.0D, 16.0D, 0.0D);
            var201.draw();
            GL11.glPopMatrix();
         }

         GL11.glDepthMask(true);
         GL11.glEnable(3553);
         GL11.glEnable(3008);
      } else if(this.mc.theWorld.provider.isSurfaceWorld()) {
         GL11.glDisable(3553);
         Vec3 var21 = this.theWorld.getSkyColor(this.mc.renderViewEntity, par1);
         var21 = CustomColorizer.getSkyColor(var21, this.mc.theWorld, this.mc.renderViewEntity.posX, this.mc.renderViewEntity.posY + 1.0D, this.mc.renderViewEntity.posZ);
         float var231 = (float)var21.xCoord;
         float var4 = (float)var21.yCoord;
         float var5 = (float)var21.zCoord;
         float var8;
         if(this.mc.gameSettings.anaglyph) {
            float var23 = (var231 * 30.0F + var4 * 59.0F + var5 * 11.0F) / 100.0F;
            float var24 = (var231 * 30.0F + var4 * 70.0F) / 100.0F;
            var8 = (var231 * 30.0F + var5 * 70.0F) / 100.0F;
            var231 = var23;
            var4 = var24;
            var5 = var8;
         }

         GL11.glColor3f(var231, var4, var5);
         Tessellator var241 = Tessellator.instance;
         GL11.glDepthMask(false);
         GL11.glEnable(2912);
         GL11.glColor3f(var231, var4, var5);
         if(Config.isSkyEnabled()) {
            GL11.glCallList(this.glSkyList);
         }

         GL11.glDisable(2912);
         GL11.glDisable(3008);
         GL11.glEnable(3042);
         GL11.glBlendFunc(770, 771);
         RenderHelper.disableStandardItemLighting();
         float[] var251 = this.theWorld.provider.calcSunriseSunsetColors(this.theWorld.getCelestialAngle(par1), par1);
         float var9;
         float var10;
         float var11;
         float var12;
         float var20;
         int var29;
         float var17;
         float var16;
         if(var251 != null && Config.isSunMoonEnabled()) {
            GL11.glDisable(3553);
            GL11.glShadeModel(7425);
            GL11.glPushMatrix();
            GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
            GL11.glRotatef(MathHelper.sin(this.theWorld.getCelestialAngleRadians(par1)) < 0.0F?180.0F:0.0F, 0.0F, 0.0F, 1.0F);
            GL11.glRotatef(90.0F, 0.0F, 0.0F, 1.0F);
            var8 = var251[0];
            var9 = var251[1];
            var10 = var251[2];
            if(this.mc.gameSettings.anaglyph) {
               var11 = (var8 * 30.0F + var9 * 59.0F + var10 * 11.0F) / 100.0F;
               var12 = (var8 * 30.0F + var9 * 70.0F) / 100.0F;
               var20 = (var8 * 30.0F + var10 * 70.0F) / 100.0F;
               var8 = var11;
               var9 = var12;
               var10 = var20;
            }

            var241.startDrawing(6);
            var241.setColorRGBA_F(var8, var9, var10, var251[3]);
            var241.addVertex(0.0D, 100.0D, 0.0D);
            byte var25 = 16;
            var241.setColorRGBA_F(var251[0], var251[1], var251[2], 0.0F);

            for(var29 = 0; var29 <= var25; ++var29) {
               var20 = (float)var29 * 3.1415927F * 2.0F / (float)var25;
               var16 = MathHelper.sin(var20);
               var17 = MathHelper.cos(var20);
               var241.addVertex((double)(var16 * 120.0F), (double)(var17 * 120.0F), (double)(-var17 * 40.0F * var251[3]));
            }

            var241.draw();
            GL11.glPopMatrix();
            GL11.glShadeModel(7424);
         }

         GL11.glEnable(3553);
         GL11.glBlendFunc(770, 1);
         GL11.glPushMatrix();
         var8 = 1.0F - this.theWorld.getRainStrength(par1);
         var9 = 0.0F;
         var10 = 0.0F;
         var11 = 0.0F;
         GL11.glColor4f(1.0F, 1.0F, 1.0F, var8);
         GL11.glTranslatef(var9, var10, var11);
         GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
         CustomSky.renderSky(this.theWorld, this.renderEngine, this.theWorld.getCelestialAngle(par1), var8);
         GL11.glRotatef(this.theWorld.getCelestialAngle(par1) * 360.0F, 1.0F, 0.0F, 0.0F);
         if(Config.isSunMoonEnabled()) {
            var12 = 30.0F;
            this.renderEngine.bindTexture("/environment/sun.png");
            var241.startDrawingQuads();
            var241.addVertexWithUV((double)(-var12), 100.0D, (double)(-var12), 0.0D, 0.0D);
            var241.addVertexWithUV((double)var12, 100.0D, (double)(-var12), 1.0D, 0.0D);
            var241.addVertexWithUV((double)var12, 100.0D, (double)var12, 1.0D, 1.0D);
            var241.addVertexWithUV((double)(-var12), 100.0D, (double)var12, 0.0D, 1.0D);
            var241.draw();
            var12 = 20.0F;
            this.renderEngine.bindTexture("/environment/moon_phases.png");
            int var26 = this.theWorld.getMoonPhase();
            int var27 = var26 % 4;
            var29 = var26 / 4 % 2;
            var16 = (float)(var27 + 0) / 4.0F;
            var17 = (float)(var29 + 0) / 2.0F;
            float var18 = (float)(var27 + 1) / 4.0F;
            float var19 = (float)(var29 + 1) / 2.0F;
            var241.startDrawingQuads();
            var241.addVertexWithUV((double)(-var12), -100.0D, (double)var12, (double)var18, (double)var19);
            var241.addVertexWithUV((double)var12, -100.0D, (double)var12, (double)var16, (double)var19);
            var241.addVertexWithUV((double)var12, -100.0D, (double)(-var12), (double)var16, (double)var17);
            var241.addVertexWithUV((double)(-var12), -100.0D, (double)(-var12), (double)var18, (double)var17);
            var241.draw();
         }

         GL11.glDisable(3553);
         var20 = this.theWorld.getStarBrightness(par1) * var8;
         if(var20 > 0.0F && Config.isStarsEnabled() && !CustomSky.hasSkyLayers(this.theWorld)) {
            GL11.glColor4f(var20, var20, var20, var20);
            GL11.glCallList(this.starGLCallList);
         }

         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         GL11.glDisable(3042);
         GL11.glEnable(3008);
         GL11.glEnable(2912);
         GL11.glPopMatrix();
         GL11.glDisable(3553);
         GL11.glColor3f(0.0F, 0.0F, 0.0F);
         double var28 = this.mc.thePlayer.getPosition(par1).yCoord - this.theWorld.getHorizon();
         if(var28 < 0.0D) {
            GL11.glPushMatrix();
            GL11.glTranslatef(0.0F, 12.0F, 0.0F);
            GL11.glCallList(this.glSkyList2);
            GL11.glPopMatrix();
            var10 = 1.0F;
            var11 = -((float)(var28 + 65.0D));
            var12 = -var10;
            var241.startDrawingQuads();
            var241.setColorRGBA_I(0, 255);
            var241.addVertex((double)(-var10), (double)var11, (double)var10);
            var241.addVertex((double)var10, (double)var11, (double)var10);
            var241.addVertex((double)var10, (double)var12, (double)var10);
            var241.addVertex((double)(-var10), (double)var12, (double)var10);
            var241.addVertex((double)(-var10), (double)var12, (double)(-var10));
            var241.addVertex((double)var10, (double)var12, (double)(-var10));
            var241.addVertex((double)var10, (double)var11, (double)(-var10));
            var241.addVertex((double)(-var10), (double)var11, (double)(-var10));
            var241.addVertex((double)var10, (double)var12, (double)(-var10));
            var241.addVertex((double)var10, (double)var12, (double)var10);
            var241.addVertex((double)var10, (double)var11, (double)var10);
            var241.addVertex((double)var10, (double)var11, (double)(-var10));
            var241.addVertex((double)(-var10), (double)var11, (double)(-var10));
            var241.addVertex((double)(-var10), (double)var11, (double)var10);
            var241.addVertex((double)(-var10), (double)var12, (double)var10);
            var241.addVertex((double)(-var10), (double)var12, (double)(-var10));
            var241.addVertex((double)(-var10), (double)var12, (double)(-var10));
            var241.addVertex((double)(-var10), (double)var12, (double)var10);
            var241.addVertex((double)var10, (double)var12, (double)var10);
            var241.addVertex((double)var10, (double)var12, (double)(-var10));
            var241.draw();
         }

         if(this.theWorld.provider.isSkyColored()) {
            GL11.glColor3f(var231 * 0.2F + 0.04F, var4 * 0.2F + 0.04F, var5 * 0.6F + 0.1F);
         } else {
            GL11.glColor3f(var231, var4, var5);
         }

         if(this.mc.gameSettings.ofRenderDistanceFine <= 64) {
            GL11.glColor3f(this.mc.entityRenderer.fogColorRed, this.mc.entityRenderer.fogColorGreen, this.mc.entityRenderer.fogColorBlue);
         }

         GL11.glPushMatrix();
         GL11.glTranslatef(0.0F, -((float)(var28 - 16.0D)), 0.0F);
         if(Config.isSkyEnabled()) {
            GL11.glCallList(this.glSkyList2);
         }

         GL11.glPopMatrix();
         GL11.glEnable(3553);
         GL11.glDepthMask(true);
      }

   }

   public void renderClouds(float par1) {
      if(!Config.isCloudsOff()) {
         if(Reflector.ForgeWorldProvider_getCloudRenderer.exists()) {
            WorldProvider var2 = this.mc.theWorld.provider;
            Object var3 = Reflector.call(var2, Reflector.ForgeWorldProvider_getCloudRenderer, new Object[0]);
            if(var3 != null) {
               Reflector.callVoid(var3, Reflector.IRenderHandler_render, new Object[]{Float.valueOf(par1), this.theWorld, this.mc});
               return;
            }
         }

         if(this.mc.theWorld.provider.isSurfaceWorld()) {
            if(Config.isCloudsFancy()) {
               this.renderCloudsFancy(par1);
            } else {
               GL11.glDisable(2884);
               float var21 = (float)(this.mc.renderViewEntity.lastTickPosY + (this.mc.renderViewEntity.posY - this.mc.renderViewEntity.lastTickPosY) * (double)par1);
               byte var31 = 32;
               int var4 = 256 / var31;
               Tessellator var5 = Tessellator.instance;
               this.renderEngine.bindTexture("/environment/clouds.png");
               GL11.glEnable(3042);
               GL11.glBlendFunc(770, 771);
               Vec3 var6 = this.theWorld.getCloudColour(par1);
               float var7 = (float)var6.xCoord;
               float var8 = (float)var6.yCoord;
               float var9 = (float)var6.zCoord;
               float var10;
               if(this.mc.gameSettings.anaglyph) {
                  var10 = (var7 * 30.0F + var8 * 59.0F + var9 * 11.0F) / 100.0F;
                  float var24 = (var7 * 30.0F + var8 * 70.0F) / 100.0F;
                  float var12 = (var7 * 30.0F + var9 * 70.0F) / 100.0F;
                  var7 = var10;
                  var8 = var24;
                  var9 = var12;
               }

               var10 = 4.8828125E-4F;
               double var241 = (double)((float)this.cloudTickCounter + par1);
               double var13 = this.mc.renderViewEntity.prevPosX + (this.mc.renderViewEntity.posX - this.mc.renderViewEntity.prevPosX) * (double)par1 + var241 * 0.029999999329447746D;
               double var15 = this.mc.renderViewEntity.prevPosZ + (this.mc.renderViewEntity.posZ - this.mc.renderViewEntity.prevPosZ) * (double)par1;
               int var17 = MathHelper.floor_double(var13 / 2048.0D);
               int var18 = MathHelper.floor_double(var15 / 2048.0D);
               var13 -= (double)(var17 * 2048);
               var15 -= (double)(var18 * 2048);
               float var19 = this.theWorld.provider.getCloudHeight() - var21 + 0.33F;
               var19 += this.mc.gameSettings.ofCloudsHeight * 128.0F;
               float var20 = (float)(var13 * (double)var10);
               var21 = (float)(var15 * (double)var10);
               var5.startDrawingQuads();
               var5.setColorRGBA_F(var7, var8, var9, 0.8F);

               for(int var22 = -var31 * var4; var22 < var31 * var4; var22 += var31) {
                  for(int var23 = -var31 * var4; var23 < var31 * var4; var23 += var31) {
                     var5.addVertexWithUV((double)(var22 + 0), (double)var19, (double)(var23 + var31), (double)((float)(var22 + 0) * var10 + var20), (double)((float)(var23 + var31) * var10 + var21));
                     var5.addVertexWithUV((double)(var22 + var31), (double)var19, (double)(var23 + var31), (double)((float)(var22 + var31) * var10 + var20), (double)((float)(var23 + var31) * var10 + var21));
                     var5.addVertexWithUV((double)(var22 + var31), (double)var19, (double)(var23 + 0), (double)((float)(var22 + var31) * var10 + var20), (double)((float)(var23 + 0) * var10 + var21));
                     var5.addVertexWithUV((double)(var22 + 0), (double)var19, (double)(var23 + 0), (double)((float)(var22 + 0) * var10 + var20), (double)((float)(var23 + 0) * var10 + var21));
                  }
               }

               var5.draw();
               GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
               GL11.glDisable(3042);
               GL11.glEnable(2884);
            }
         }

      }
   }

   public boolean hasCloudFog(double par1, double par3, double par5, float par7) {
      return false;
   }

   public void renderCloudsFancy(float par1) {
      GL11.glDisable(2884);
      float var2 = (float)(this.mc.renderViewEntity.lastTickPosY + (this.mc.renderViewEntity.posY - this.mc.renderViewEntity.lastTickPosY) * (double)par1);
      Tessellator var3 = Tessellator.instance;
      float var4 = 12.0F;
      float var5 = 4.0F;
      double var6 = (double)((float)this.cloudTickCounter + par1);
      double var8 = (this.mc.renderViewEntity.prevPosX + (this.mc.renderViewEntity.posX - this.mc.renderViewEntity.prevPosX) * (double)par1 + var6 * 0.029999999329447746D) / (double)var4;
      double var10 = (this.mc.renderViewEntity.prevPosZ + (this.mc.renderViewEntity.posZ - this.mc.renderViewEntity.prevPosZ) * (double)par1) / (double)var4 + 0.33000001311302185D;
      float var12 = this.theWorld.provider.getCloudHeight() - var2 + 0.33F;
      var12 += this.mc.gameSettings.ofCloudsHeight * 128.0F;
      int var13 = MathHelper.floor_double(var8 / 2048.0D);
      int var14 = MathHelper.floor_double(var10 / 2048.0D);
      var8 -= (double)(var13 * 2048);
      var10 -= (double)(var14 * 2048);
      this.renderEngine.bindTexture("/environment/clouds.png");
      GL11.glEnable(3042);
      GL11.glBlendFunc(770, 771);
      Vec3 var15 = this.theWorld.getCloudColour(par1);
      float var16 = (float)var15.xCoord;
      float var17 = (float)var15.yCoord;
      float var18 = (float)var15.zCoord;
      float var19;
      float var20;
      float var21;
      if(this.mc.gameSettings.anaglyph) {
         var19 = (var16 * 30.0F + var17 * 59.0F + var18 * 11.0F) / 100.0F;
         var20 = (var16 * 30.0F + var17 * 70.0F) / 100.0F;
         var21 = (var16 * 30.0F + var18 * 70.0F) / 100.0F;
         var16 = var19;
         var17 = var20;
         var18 = var21;
      }

      var19 = (float)(var8 * 0.0D);
      var20 = (float)(var10 * 0.0D);
      var21 = 0.00390625F;
      var19 = (float)MathHelper.floor_double(var8) * var21;
      var20 = (float)MathHelper.floor_double(var10) * var21;
      float var22 = (float)(var8 - (double)MathHelper.floor_double(var8));
      float var23 = (float)(var10 - (double)MathHelper.floor_double(var10));
      byte var24 = 8;
      byte var25 = 4;
      float var26 = 9.765625E-4F;
      GL11.glScalef(var4, 1.0F, var4);

      for(int var27 = 0; var27 < 2; ++var27) {
         if(var27 == 0) {
            GL11.glColorMask(false, false, false, false);
         } else if(this.mc.gameSettings.anaglyph) {
            if(EntityRenderer.anaglyphField == 0) {
               GL11.glColorMask(false, true, true, true);
            } else {
               GL11.glColorMask(true, false, false, true);
            }
         } else {
            GL11.glColorMask(true, true, true, true);
         }

         for(int var28 = -var25 + 1; var28 <= var25; ++var28) {
            for(int var29 = -var25 + 1; var29 <= var25; ++var29) {
               var3.startDrawingQuads();
               float var30 = (float)(var28 * var24);
               float var31 = (float)(var29 * var24);
               float var32 = var30 - var22;
               float var33 = var31 - var23;
               if(var12 > -var5 - 1.0F) {
                  var3.setColorRGBA_F(var16 * 0.7F, var17 * 0.7F, var18 * 0.7F, 0.8F);
                  var3.setNormal(0.0F, -1.0F, 0.0F);
                  var3.addVertexWithUV((double)(var32 + 0.0F), (double)(var12 + 0.0F), (double)(var33 + (float)var24), (double)((var30 + 0.0F) * var21 + var19), (double)((var31 + (float)var24) * var21 + var20));
                  var3.addVertexWithUV((double)(var32 + (float)var24), (double)(var12 + 0.0F), (double)(var33 + (float)var24), (double)((var30 + (float)var24) * var21 + var19), (double)((var31 + (float)var24) * var21 + var20));
                  var3.addVertexWithUV((double)(var32 + (float)var24), (double)(var12 + 0.0F), (double)(var33 + 0.0F), (double)((var30 + (float)var24) * var21 + var19), (double)((var31 + 0.0F) * var21 + var20));
                  var3.addVertexWithUV((double)(var32 + 0.0F), (double)(var12 + 0.0F), (double)(var33 + 0.0F), (double)((var30 + 0.0F) * var21 + var19), (double)((var31 + 0.0F) * var21 + var20));
               }

               if(var12 <= var5 + 1.0F) {
                  var3.setColorRGBA_F(var16, var17, var18, 0.8F);
                  var3.setNormal(0.0F, 1.0F, 0.0F);
                  var3.addVertexWithUV((double)(var32 + 0.0F), (double)(var12 + var5 - var26), (double)(var33 + (float)var24), (double)((var30 + 0.0F) * var21 + var19), (double)((var31 + (float)var24) * var21 + var20));
                  var3.addVertexWithUV((double)(var32 + (float)var24), (double)(var12 + var5 - var26), (double)(var33 + (float)var24), (double)((var30 + (float)var24) * var21 + var19), (double)((var31 + (float)var24) * var21 + var20));
                  var3.addVertexWithUV((double)(var32 + (float)var24), (double)(var12 + var5 - var26), (double)(var33 + 0.0F), (double)((var30 + (float)var24) * var21 + var19), (double)((var31 + 0.0F) * var21 + var20));
                  var3.addVertexWithUV((double)(var32 + 0.0F), (double)(var12 + var5 - var26), (double)(var33 + 0.0F), (double)((var30 + 0.0F) * var21 + var19), (double)((var31 + 0.0F) * var21 + var20));
               }

               var3.setColorRGBA_F(var16 * 0.9F, var17 * 0.9F, var18 * 0.9F, 0.8F);
               int var34;
               if(var28 > -1) {
                  var3.setNormal(-1.0F, 0.0F, 0.0F);

                  for(var34 = 0; var34 < var24; ++var34) {
                     var3.addVertexWithUV((double)(var32 + (float)var34 + 0.0F), (double)(var12 + 0.0F), (double)(var33 + (float)var24), (double)((var30 + (float)var34 + 0.5F) * var21 + var19), (double)((var31 + (float)var24) * var21 + var20));
                     var3.addVertexWithUV((double)(var32 + (float)var34 + 0.0F), (double)(var12 + var5), (double)(var33 + (float)var24), (double)((var30 + (float)var34 + 0.5F) * var21 + var19), (double)((var31 + (float)var24) * var21 + var20));
                     var3.addVertexWithUV((double)(var32 + (float)var34 + 0.0F), (double)(var12 + var5), (double)(var33 + 0.0F), (double)((var30 + (float)var34 + 0.5F) * var21 + var19), (double)((var31 + 0.0F) * var21 + var20));
                     var3.addVertexWithUV((double)(var32 + (float)var34 + 0.0F), (double)(var12 + 0.0F), (double)(var33 + 0.0F), (double)((var30 + (float)var34 + 0.5F) * var21 + var19), (double)((var31 + 0.0F) * var21 + var20));
                  }
               }

               if(var28 <= 1) {
                  var3.setNormal(1.0F, 0.0F, 0.0F);

                  for(var34 = 0; var34 < var24; ++var34) {
                     var3.addVertexWithUV((double)(var32 + (float)var34 + 1.0F - var26), (double)(var12 + 0.0F), (double)(var33 + (float)var24), (double)((var30 + (float)var34 + 0.5F) * var21 + var19), (double)((var31 + (float)var24) * var21 + var20));
                     var3.addVertexWithUV((double)(var32 + (float)var34 + 1.0F - var26), (double)(var12 + var5), (double)(var33 + (float)var24), (double)((var30 + (float)var34 + 0.5F) * var21 + var19), (double)((var31 + (float)var24) * var21 + var20));
                     var3.addVertexWithUV((double)(var32 + (float)var34 + 1.0F - var26), (double)(var12 + var5), (double)(var33 + 0.0F), (double)((var30 + (float)var34 + 0.5F) * var21 + var19), (double)((var31 + 0.0F) * var21 + var20));
                     var3.addVertexWithUV((double)(var32 + (float)var34 + 1.0F - var26), (double)(var12 + 0.0F), (double)(var33 + 0.0F), (double)((var30 + (float)var34 + 0.5F) * var21 + var19), (double)((var31 + 0.0F) * var21 + var20));
                  }
               }

               var3.setColorRGBA_F(var16 * 0.8F, var17 * 0.8F, var18 * 0.8F, 0.8F);
               if(var29 > -1) {
                  var3.setNormal(0.0F, 0.0F, -1.0F);

                  for(var34 = 0; var34 < var24; ++var34) {
                     var3.addVertexWithUV((double)(var32 + 0.0F), (double)(var12 + var5), (double)(var33 + (float)var34 + 0.0F), (double)((var30 + 0.0F) * var21 + var19), (double)((var31 + (float)var34 + 0.5F) * var21 + var20));
                     var3.addVertexWithUV((double)(var32 + (float)var24), (double)(var12 + var5), (double)(var33 + (float)var34 + 0.0F), (double)((var30 + (float)var24) * var21 + var19), (double)((var31 + (float)var34 + 0.5F) * var21 + var20));
                     var3.addVertexWithUV((double)(var32 + (float)var24), (double)(var12 + 0.0F), (double)(var33 + (float)var34 + 0.0F), (double)((var30 + (float)var24) * var21 + var19), (double)((var31 + (float)var34 + 0.5F) * var21 + var20));
                     var3.addVertexWithUV((double)(var32 + 0.0F), (double)(var12 + 0.0F), (double)(var33 + (float)var34 + 0.0F), (double)((var30 + 0.0F) * var21 + var19), (double)((var31 + (float)var34 + 0.5F) * var21 + var20));
                  }
               }

               if(var29 <= 1) {
                  var3.setNormal(0.0F, 0.0F, 1.0F);

                  for(var34 = 0; var34 < var24; ++var34) {
                     var3.addVertexWithUV((double)(var32 + 0.0F), (double)(var12 + var5), (double)(var33 + (float)var34 + 1.0F - var26), (double)((var30 + 0.0F) * var21 + var19), (double)((var31 + (float)var34 + 0.5F) * var21 + var20));
                     var3.addVertexWithUV((double)(var32 + (float)var24), (double)(var12 + var5), (double)(var33 + (float)var34 + 1.0F - var26), (double)((var30 + (float)var24) * var21 + var19), (double)((var31 + (float)var34 + 0.5F) * var21 + var20));
                     var3.addVertexWithUV((double)(var32 + (float)var24), (double)(var12 + 0.0F), (double)(var33 + (float)var34 + 1.0F - var26), (double)((var30 + (float)var24) * var21 + var19), (double)((var31 + (float)var34 + 0.5F) * var21 + var20));
                     var3.addVertexWithUV((double)(var32 + 0.0F), (double)(var12 + 0.0F), (double)(var33 + (float)var34 + 1.0F - var26), (double)((var30 + 0.0F) * var21 + var19), (double)((var31 + (float)var34 + 0.5F) * var21 + var20));
                  }
               }

               var3.draw();
            }
         }
      }

      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glDisable(3042);
      GL11.glEnable(2884);
   }

   public boolean updateRenderers(EntityLiving entityliving, boolean flag) {
      if(WrUpdates.hasWrUpdater()) {
         return WrUpdates.updateRenderers(this, entityliving, flag);
      } else if(this.worldRenderersToUpdate.size() <= 0) {
         return false;
      } else {
         int num = 0;
         int maxNum = Config.getUpdatesPerFrame();
         if(Config.isDynamicUpdates() && !this.isMoving(entityliving)) {
            maxNum *= 3;
         }

         byte NOT_IN_FRUSTRUM_MUL = 4;
         int numValid = 0;
         WorldRenderer wrBest = null;
         float distSqBest = Float.MAX_VALUE;
         int indexBest = -1;

         for(int maxDiffDistSq = 0; maxDiffDistSq < this.worldRenderersToUpdate.size(); ++maxDiffDistSq) {
            WorldRenderer i = (WorldRenderer)this.worldRenderersToUpdate.get(maxDiffDistSq);
            if(i != null) {
               ++numValid;
               if(!i.needsUpdate) {
                  this.worldRenderersToUpdate.set(maxDiffDistSq, (Object)null);
               } else {
                  float wr = i.distanceToEntitySquared(entityliving);
                  if(wr <= 256.0F && this.isActingNow()) {
                     i.updateRenderer();
                     i.needsUpdate = false;
                     this.worldRenderersToUpdate.set(maxDiffDistSq, (Object)null);
                     ++num;
                  } else {
                     if(wr > 256.0F && num >= maxNum) {
                        break;
                     }

                     if(!i.isInFrustum) {
                        wr *= (float)NOT_IN_FRUSTRUM_MUL;
                     }

                     if(wrBest == null) {
                        wrBest = i;
                        distSqBest = wr;
                        indexBest = maxDiffDistSq;
                     } else if(wr < distSqBest) {
                        wrBest = i;
                        distSqBest = wr;
                        indexBest = maxDiffDistSq;
                     }
                  }
               }
            }
         }

         if(wrBest != null) {
            wrBest.updateRenderer();
            wrBest.needsUpdate = false;
            this.worldRenderersToUpdate.set(indexBest, (Object)null);
            ++num;
            float var16 = distSqBest / 5.0F;

            for(int var15 = 0; var15 < this.worldRenderersToUpdate.size() && num < maxNum; ++var15) {
               WorldRenderer var17 = (WorldRenderer)this.worldRenderersToUpdate.get(var15);
               if(var17 != null) {
                  float distSq = var17.distanceToEntitySquared(entityliving);
                  if(!var17.isInFrustum) {
                     distSq *= (float)NOT_IN_FRUSTRUM_MUL;
                  }

                  float diffDistSq = Math.abs(distSq - distSqBest);
                  if(diffDistSq < var16) {
                     var17.updateRenderer();
                     var17.needsUpdate = false;
                     this.worldRenderersToUpdate.set(var15, (Object)null);
                     ++num;
                  }
               }
            }
         }

         if(numValid == 0) {
            this.worldRenderersToUpdate.clear();
         }

         this.worldRenderersToUpdate.compact();
         return true;
      }
   }

   public void drawBlockBreaking(EntityPlayer par1EntityPlayer, MovingObjectPosition par2MovingObjectPosition, int par3, ItemStack par4ItemStack, float par5) {
      Tessellator var6 = Tessellator.instance;
      GL11.glEnable(3042);
      GL11.glEnable(3008);
      GL11.glBlendFunc(770, 1);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, (MathHelper.sin((float)Minecraft.getSystemTime() / 100.0F) * 0.2F + 0.4F) * 0.5F);
      if(par3 != 0 && par4ItemStack != null) {
         GL11.glBlendFunc(770, 771);
         float var7 = MathHelper.sin((float)Minecraft.getSystemTime() / 100.0F) * 0.2F + 0.8F;
         GL11.glColor4f(var7, var7, var7, MathHelper.sin((float)Minecraft.getSystemTime() / 200.0F) * 0.2F + 0.5F);
         this.renderEngine.bindTexture("/terrain.png");
      }

      GL11.glDisable(3042);
      GL11.glDisable(3008);
   }

   public void drawBlockDamageTexture(Tessellator par1Tessellator, EntityPlayer par2EntityPlayer, float par3) {
      this.drawBlockDamageTexture(par1Tessellator, (EntityLiving)par2EntityPlayer, par3);
   }

   public void drawBlockDamageTexture(Tessellator par1Tessellator, EntityLiving par2EntityPlayer, float par3) {
      double var4 = par2EntityPlayer.lastTickPosX + (par2EntityPlayer.posX - par2EntityPlayer.lastTickPosX) * (double)par3;
      double var6 = par2EntityPlayer.lastTickPosY + (par2EntityPlayer.posY - par2EntityPlayer.lastTickPosY) * (double)par3;
      double var8 = par2EntityPlayer.lastTickPosZ + (par2EntityPlayer.posZ - par2EntityPlayer.lastTickPosZ) * (double)par3;
      if(!this.damagedBlocks.isEmpty()) {
         GL11.glBlendFunc(774, 768);
         this.renderEngine.bindTexture("/terrain.png");
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 0.5F);
         GL11.glPushMatrix();
         GL11.glDisable(3008);
         GL11.glPolygonOffset(-3.0F, -3.0F);
         GL11.glEnable('\u8037');
         GL11.glEnable(3008);
         par1Tessellator.startDrawingQuads();
         par1Tessellator.setTranslation(-var4, -var6, -var8);
         par1Tessellator.disableColor();
         Iterator var10 = this.damagedBlocks.values().iterator();

         while(var10.hasNext()) {
            DestroyBlockProgress var11 = (DestroyBlockProgress)var10.next();
            double var12 = (double)var11.getPartialBlockX() - var4;
            double var14 = (double)var11.getPartialBlockY() - var6;
            double var16 = (double)var11.getPartialBlockZ() - var8;
            if(var12 * var12 + var14 * var14 + var16 * var16 > 1024.0D) {
               var10.remove();
            } else {
               int var18 = this.theWorld.getBlockId(var11.getPartialBlockX(), var11.getPartialBlockY(), var11.getPartialBlockZ());
               Block var19 = var18 > 0?Block.blocksList[var18]:null;
               if(var19 == null) {
                  var19 = Block.stone;
               }

               this.globalRenderBlocks.renderBlockUsingTexture(var19, var11.getPartialBlockX(), var11.getPartialBlockY(), var11.getPartialBlockZ(), this.destroyBlockIcons[var11.getPartialBlockDamage()]);
            }
         }

         par1Tessellator.draw();
         par1Tessellator.setTranslation(0.0D, 0.0D, 0.0D);
         GL11.glDisable(3008);
         GL11.glPolygonOffset(0.0F, 0.0F);
         GL11.glDisable('\u8037');
         GL11.glEnable(3008);
         GL11.glDepthMask(true);
         GL11.glPopMatrix();
      }

   }

   public void drawSelectionBox(EntityPlayer par1EntityPlayer, MovingObjectPosition par2MovingObjectPosition, int par3, ItemStack par4ItemStack, float par5) {
      if(par3 == 0 && par2MovingObjectPosition.typeOfHit == EnumMovingObjectType.TILE) {
         GL11.glEnable(3042);
         GL11.glBlendFunc(770, 771);
         GL11.glColor4f(0.0F, 0.0F, 0.0F, 0.4F);
         GL11.glLineWidth(2.0F);
         GL11.glDisable(3553);
         GL11.glDepthMask(false);
         float var6 = 0.002F;
         int var7 = this.theWorld.getBlockId(par2MovingObjectPosition.blockX, par2MovingObjectPosition.blockY, par2MovingObjectPosition.blockZ);
         if(var7 > 0) {
            Block.blocksList[var7].setBlockBoundsBasedOnState(this.theWorld, par2MovingObjectPosition.blockX, par2MovingObjectPosition.blockY, par2MovingObjectPosition.blockZ);
            double var8 = par1EntityPlayer.lastTickPosX + (par1EntityPlayer.posX - par1EntityPlayer.lastTickPosX) * (double)par5;
            double var10 = par1EntityPlayer.lastTickPosY + (par1EntityPlayer.posY - par1EntityPlayer.lastTickPosY) * (double)par5;
            double var12 = par1EntityPlayer.lastTickPosZ + (par1EntityPlayer.posZ - par1EntityPlayer.lastTickPosZ) * (double)par5;
            this.drawOutlinedBoundingBox(Block.blocksList[var7].getSelectedBoundingBoxFromPool(this.theWorld, par2MovingObjectPosition.blockX, par2MovingObjectPosition.blockY, par2MovingObjectPosition.blockZ).expand((double)var6, (double)var6, (double)var6).getOffsetBoundingBox(-var8, -var10, -var12));
         }

         GL11.glDepthMask(true);
         GL11.glEnable(3553);
         GL11.glDisable(3042);
      }

   }

   private void drawOutlinedBoundingBox(AxisAlignedBB par1AxisAlignedBB) {
      Tessellator var2 = Tessellator.instance;
      var2.startDrawing(3);
      var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
      var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
      var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY, par1AxisAlignedBB.maxZ);
      var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.maxZ);
      var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
      var2.draw();
      var2.startDrawing(3);
      var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
      var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
      var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.maxZ);
      var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.maxZ);
      var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
      var2.draw();
      var2.startDrawing(1);
      var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
      var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
      var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY, par1AxisAlignedBB.minZ);
      var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.minZ);
      var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.minY, par1AxisAlignedBB.maxZ);
      var2.addVertex(par1AxisAlignedBB.maxX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.maxZ);
      var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.minY, par1AxisAlignedBB.maxZ);
      var2.addVertex(par1AxisAlignedBB.minX, par1AxisAlignedBB.maxY, par1AxisAlignedBB.maxZ);
      var2.draw();
   }

   public void markBlocksForUpdate(int par1, int par2, int par3, int par4, int par5, int par6) {
      int var7 = MathHelper.bucketInt(par1, 16);
      int var8 = MathHelper.bucketInt(par2, 16);
      int var9 = MathHelper.bucketInt(par3, 16);
      int var10 = MathHelper.bucketInt(par4, 16);
      int var11 = MathHelper.bucketInt(par5, 16);
      int var12 = MathHelper.bucketInt(par6, 16);

      for(int var13 = var7; var13 <= var10; ++var13) {
         int var14 = var13 % this.renderChunksWide;
         if(var14 < 0) {
            var14 += this.renderChunksWide;
         }

         for(int var15 = var8; var15 <= var11; ++var15) {
            int var16 = var15 % this.renderChunksTall;
            if(var16 < 0) {
               var16 += this.renderChunksTall;
            }

            for(int var17 = var9; var17 <= var12; ++var17) {
               int var18 = var17 % this.renderChunksDeep;
               if(var18 < 0) {
                  var18 += this.renderChunksDeep;
               }

               int var19 = (var18 * this.renderChunksTall + var16) * this.renderChunksWide + var14;
               WorldRenderer var20 = this.worldRenderers[var19];
               if(var20 != null && !var20.needsUpdate) {
                  this.worldRenderersToUpdate.add(var20);
                  var20.markDirty();
               }
            }
         }
      }

   }

   public void markBlockForUpdate(int par1, int par2, int par3) {
      this.markBlocksForUpdate(par1 - 1, par2 - 1, par3 - 1, par1 + 1, par2 + 1, par3 + 1);
   }

   public void markBlockForRenderUpdate(int par1, int par2, int par3) {
      this.markBlocksForUpdate(par1 - 1, par2 - 1, par3 - 1, par1 + 1, par2 + 1, par3 + 1);
   }

   public void markBlockRangeForRenderUpdate(int par1, int par2, int par3, int par4, int par5, int par6) {
      this.markBlocksForUpdate(par1 - 1, par2 - 1, par3 - 1, par4 + 1, par5 + 1, par6 + 1);
   }

   public void clipRenderersByFrustum(ICamera par1ICamera, float par2) {
      for(int var3 = 0; var3 < this.worldRenderers.length; ++var3) {
         if(!this.worldRenderers[var3].skipAllRenderPasses()) {
            this.worldRenderers[var3].updateInFrustum(par1ICamera);
         }
      }

      ++this.frustumCheckOffset;
   }

   public void playRecord(String par1Str, int par2, int par3, int par4) {
      ItemRecord var5 = ItemRecord.getRecord(par1Str);
      if(par1Str != null && var5 != null) {
         this.mc.ingameGUI.setRecordPlayingMessage(var5.getRecordTitle());
      }

      this.mc.sndManager.playStreaming(par1Str, (float)par2, (float)par3, (float)par4);
   }

   public void playSound(String par1Str, double par2, double par4, double par6, float par8, float par9) {}

   public void playSoundToNearExcept(EntityPlayer par1EntityPlayer, String par2Str, double par3, double par5, double par7, float par9, float par10) {}

   public void spawnParticle(String par1Str, double par2, double par4, double par6, double par8, double par10, double par12) {
      try {
         this.doSpawnParticle(par1Str, par2, par4, par6, par8, par10, par12);
      } catch (Throwable var17) {
         CrashReport var15 = CrashReport.makeCrashReport(var17, "Exception while adding particle");
         CrashReportCategory var16 = var15.makeCategory("Particle being added");
         var16.addCrashSection("Name", par1Str);
         var16.addCrashSectionCallable("Position", new CallableParticlePositionInfo(this, par2, par4, par6));
         throw new ReportedException(var15);
      }
   }

   public EntityFX doSpawnParticle(String par1Str, double par2, double par4, double par6, double par8, double par10, double par12) {
      if(this.mc != null && this.mc.renderViewEntity != null && this.mc.effectRenderer != null) {
         int var14 = this.mc.gameSettings.particleSetting;
         if(var14 == 1 && this.theWorld.rand.nextInt(3) == 0) {
            var14 = 2;
         }

         double var15 = this.mc.renderViewEntity.posX - par2;
         double var17 = this.mc.renderViewEntity.posY - par4;
         double var19 = this.mc.renderViewEntity.posZ - par6;
         EntityFX var21 = null;
         if(par1Str.equals("hugeexplosion")) {
            if(Config.isAnimatedExplosion()) {
               this.mc.effectRenderer.addEffect(var21 = new EntityHugeExplodeFX(this.theWorld, par2, par4, par6, par8, par10, par12));
            }
         } else if(par1Str.equals("largeexplode")) {
            if(Config.isAnimatedExplosion()) {
               this.mc.effectRenderer.addEffect(var21 = new EntityLargeExplodeFX(this.renderEngine, this.theWorld, par2, par4, par6, par8, par10, par12));
            }
         } else if(par1Str.equals("fireworksSpark")) {
            this.mc.effectRenderer.addEffect(var21 = new EntityFireworkSparkFX(this.theWorld, par2, par4, par6, par8, par10, par12, this.mc.effectRenderer));
         }

         if(var21 != null) {
            return (EntityFX)var21;
         } else {
            double var22 = 16.0D;
            double d3 = 16.0D;
            if(par1Str.equals("crit")) {
               var22 = 196.0D;
            }

            if(var15 * var15 + var17 * var17 + var19 * var19 > var22 * var22) {
               return null;
            } else if(var14 > 1) {
               return null;
            } else {
               if(par1Str.equals("bubble")) {
                  var21 = new EntityBubbleFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                  CustomColorizer.updateWaterFX((EntityFX)var21, this.theWorld);
               } else if(par1Str.equals("suspended")) {
                  if(Config.isWaterParticles()) {
                     var21 = new EntitySuspendFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                  }
               } else if(par1Str.equals("depthsuspend")) {
                  if(Config.isVoidParticles()) {
                     var21 = new EntityAuraFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                  }
               } else if(par1Str.equals("townaura")) {
                  var21 = new EntityAuraFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                  CustomColorizer.updateMyceliumFX((EntityFX)var21);
               } else if(par1Str.equals("crit")) {
                  var21 = new EntityCritFX(this.theWorld, par2, par4, par6, par8, par10, par12);
               } else if(par1Str.equals("magicCrit")) {
                  var21 = new EntityCritFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                  ((EntityFX)var21).setRBGColorF(((EntityFX)var21).getRedColorF() * 0.3F, ((EntityFX)var21).getGreenColorF() * 0.8F, ((EntityFX)var21).getBlueColorF());
                  ((EntityFX)var21).nextTextureIndexX();
               } else if(par1Str.equals("smoke")) {
                  if(Config.isAnimatedSmoke()) {
                     var21 = new EntitySmokeFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                  }
               } else if(par1Str.equals("mobSpell")) {
                  if(Config.isPotionParticles()) {
                     var21 = new EntitySpellParticleFX(this.theWorld, par2, par4, par6, 0.0D, 0.0D, 0.0D);
                     ((EntityFX)var21).setRBGColorF((float)par8, (float)par10, (float)par12);
                  }
               } else if(par1Str.equals("mobSpellAmbient")) {
                  if(Config.isPotionParticles()) {
                     var21 = new EntitySpellParticleFX(this.theWorld, par2, par4, par6, 0.0D, 0.0D, 0.0D);
                     ((EntityFX)var21).setAlphaF(0.15F);
                     ((EntityFX)var21).setRBGColorF((float)par8, (float)par10, (float)par12);
                  }
               } else if(par1Str.equals("spell")) {
                  if(Config.isPotionParticles()) {
                     var21 = new EntitySpellParticleFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                  }
               } else if(par1Str.equals("instantSpell")) {
                  if(Config.isPotionParticles()) {
                     var21 = new EntitySpellParticleFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                     ((EntitySpellParticleFX)var21).setBaseSpellTextureIndex(144);
                  }
               } else if(par1Str.equals("witchMagic")) {
                  if(Config.isPotionParticles()) {
                     var21 = new EntitySpellParticleFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                     ((EntitySpellParticleFX)var21).setBaseSpellTextureIndex(144);
                     float var28 = this.theWorld.rand.nextFloat() * 0.5F + 0.35F;
                     ((EntityFX)var21).setRBGColorF(1.0F * var28, 0.0F * var28, 1.0F * var28);
                  }
               } else if(par1Str.equals("note")) {
                  var21 = new EntityNoteFX(this.theWorld, par2, par4, par6, par8, par10, par12);
               } else if(par1Str.equals("portal")) {
                  if(Config.isPortalParticles()) {
                     var21 = new EntityPortalFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                     CustomColorizer.updatePortalFX((EntityFX)var21);
                  }
               } else if(par1Str.equals("enchantmenttable")) {
                  var21 = new EntityEnchantmentTableParticleFX(this.theWorld, par2, par4, par6, par8, par10, par12);
               } else if(par1Str.equals("explode")) {
                  if(Config.isAnimatedExplosion()) {
                     var21 = new EntityExplodeFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                  }
               } else if(par1Str.equals("flame")) {
                  if(Config.isAnimatedFlame()) {
                     var21 = new EntityFlameFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                  }
               } else if(par1Str.equals("lava")) {
                  var21 = new EntityLavaFX(this.theWorld, par2, par4, par6);
               } else if(par1Str.equals("footstep")) {
                  var21 = new EntityFootStepFX(this.renderEngine, this.theWorld, par2, par4, par6);
               } else if(par1Str.equals("splash")) {
                  var21 = new EntitySplashFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                  CustomColorizer.updateWaterFX((EntityFX)var21, this.theWorld);
               } else if(par1Str.equals("largesmoke")) {
                  if(Config.isAnimatedSmoke()) {
                     var21 = new EntitySmokeFX(this.theWorld, par2, par4, par6, par8, par10, par12, 2.5F);
                  }
               } else if(par1Str.equals("cloud")) {
                  var21 = new EntityCloudFX(this.theWorld, par2, par4, par6, par8, par10, par12);
               } else if(par1Str.equals("reddust")) {
                  if(Config.isAnimatedRedstone()) {
                     var21 = new EntityReddustFX(this.theWorld, par2, par4, par6, (float)par8, (float)par10, (float)par12);
                     CustomColorizer.updateReddustFX((EntityFX)var21, this.theWorld, var15, var17, var19);
                  }
               } else if(par1Str.equals("snowballpoof")) {
                  var21 = new EntityBreakingFX(this.theWorld, par2, par4, par6, Item.snowball, this.renderEngine);
               } else if(par1Str.equals("dripWater")) {
                  if(Config.isDrippingWaterLava()) {
                     var21 = new EntityDropParticleFX(this.theWorld, par2, par4, par6, Material.water);
                  }
               } else if(par1Str.equals("dripLava")) {
                  if(Config.isDrippingWaterLava()) {
                     var21 = new EntityDropParticleFX(this.theWorld, par2, par4, par6, Material.lava);
                  }
               } else if(par1Str.equals("snowshovel")) {
                  var21 = new EntitySnowShovelFX(this.theWorld, par2, par4, par6, par8, par10, par12);
               } else if(par1Str.equals("slime")) {
                  var21 = new EntityBreakingFX(this.theWorld, par2, par4, par6, Item.slimeBall, this.renderEngine);
               } else if(par1Str.equals("heart")) {
                  var21 = new EntityHeartFX(this.theWorld, par2, par4, par6, par8, par10, par12);
               } else if(par1Str.equals("angryVillager")) {
                  var21 = new EntityHeartFX(this.theWorld, par2, par4 + 0.5D, par6, par8, par10, par12);
                  ((EntityFX)var21).setParticleTextureIndex(81);
                  ((EntityFX)var21).setRBGColorF(1.0F, 1.0F, 1.0F);
               } else if(par1Str.equals("happyVillager")) {
                  var21 = new EntityAuraFX(this.theWorld, par2, par4, par6, par8, par10, par12);
                  ((EntityFX)var21).setParticleTextureIndex(82);
                  ((EntityFX)var21).setRBGColorF(1.0F, 1.0F, 1.0F);
               } else if(par1Str.startsWith("iconcrack_")) {
                  int var282 = Integer.parseInt(par1Str.substring(par1Str.indexOf("_") + 1));
                  var21 = new EntityBreakingFX(this.theWorld, par2, par4, par6, par8, par10, par12, Item.itemsList[var282], this.renderEngine);
               } else if(par1Str.startsWith("tilecrack_")) {
                  String[] var281 = par1Str.split("_", 3);
                  int var25 = Integer.parseInt(var281[1]);
                  int var26 = Integer.parseInt(var281[2]);
                  var21 = (new EntityDiggingFX(this.theWorld, par2, par4, par6, par8, par10, par12, Block.blocksList[var25], 0, var26, this.renderEngine)).applyRenderColor(var26);
               }

               if(var21 != null) {
                  this.mc.effectRenderer.addEffect((EntityFX)var21);
               }

               return (EntityFX)var21;
            }
         }
      } else {
         return null;
      }
   }

   public void onEntityCreate(Entity par1Entity) {
      par1Entity.updateCloak();
      //pixelsky start
      par1Entity.updateTopHat();
      par1Entity.updateNotchHat();
      par1Entity.updateTail();
      par1Entity.updateEars();
      par1Entity.updateBracelet();
      par1Entity.updateSunglasses();
      par1Entity.updateWings();
      //pixelsky end
      if(par1Entity.skinUrl != null) {
         this.renderEngine.obtainImageData(par1Entity.skinUrl, new ImageBufferDownload());
      }

      if(par1Entity.cloakUrl != null) {
         this.renderEngine.obtainImageData(par1Entity.cloakUrl, new ImageBufferDownload());
         if(par1Entity instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer)par1Entity;
            ThreadDownloadImageData tdid = this.renderEngine.obtainImageData(player.cloakUrl, new ImageBufferDownload());
            this.renderEngine.releaseImageData(player.cloakUrl);
            String urlStr = "http://s.optifine.net/capes/" + StringUtils.stripControlCodes(player.username) + ".png";
            ThreadDownloadImage tdi = new ThreadDownloadImage(tdid, urlStr, new ImageBufferDownload());
            tdi.start();
            if(!Config.isShowCapes()) {
               player.cloakUrl = "";
            }
         }
      }
      //pixelsky start
        if (par1Entity.topHatUrl != null)
        {
            this.renderEngine.obtainImageData(par1Entity.topHatUrl, new ImageBufferDownload());
        }

        if (par1Entity.earsUrl != null)
        {
            this.renderEngine.obtainImageData(par1Entity.earsUrl, new ImageBufferDownload());
        }

        if (par1Entity.braceletUrl != null)
        {
            this.renderEngine.obtainImageData(par1Entity.braceletUrl, new ImageBufferDownload());
        }

        if (par1Entity.sunglassesUrl != null)
        {
            this.renderEngine.obtainImageData(par1Entity.sunglassesUrl, new ImageBufferDownload());
        }

        if (par1Entity.tailUrl != null)
        {
            this.renderEngine.obtainImageData(par1Entity.tailUrl, new ImageBufferDownload());
        }

        if (par1Entity.notchHatUrl != null)
        {
            this.renderEngine.obtainImageData(par1Entity.notchHatUrl, new ImageBufferDownload());
        }

        if (par1Entity.wingsUrl != null)
        {
            this.renderEngine.obtainImageData(par1Entity.wingsUrl, new ImageBufferDownload());
        }
    //pixelsky end
      if(Config.isRandomMobs()) {
         RandomMobs.entityLoaded(par1Entity);
      }

   }

   public void onEntityDestroy(Entity par1Entity) {
      if(par1Entity.skinUrl != null) {
         this.renderEngine.releaseImageData(par1Entity.skinUrl);
      }

      if(par1Entity.cloakUrl != null) {
         this.renderEngine.releaseImageData(par1Entity.cloakUrl);
      }

   }

   public void deleteAllDisplayLists() {
      GLAllocation.deleteDisplayLists(this.glRenderListBase);
   }

   public void broadcastSound(int par1, int par2, int par3, int par4, int par5) {
      Random var6 = this.theWorld.rand;
      switch(par1) {
      case 1013:
      case 1018:
         if(this.mc.renderViewEntity != null) {
            double var7 = (double)par2 - this.mc.renderViewEntity.posX;
            double var9 = (double)par3 - this.mc.renderViewEntity.posY;
            double var11 = (double)par4 - this.mc.renderViewEntity.posZ;
            double var13 = Math.sqrt(var7 * var7 + var9 * var9 + var11 * var11);
            double var15 = this.mc.renderViewEntity.posX;
            double var17 = this.mc.renderViewEntity.posY;
            double var19 = this.mc.renderViewEntity.posZ;
            if(var13 > 0.0D) {
               var15 += var7 / var13 * 2.0D;
               var17 += var9 / var13 * 2.0D;
               var19 += var11 / var13 * 2.0D;
            }

            if(par1 == 1013) {
               this.theWorld.playSound(var15, var17, var19, "mob.wither.spawn", 1.0F, 1.0F, false);
            } else if(par1 == 1018) {
               this.theWorld.playSound(var15, var17, var19, "mob.enderdragon.end", 5.0F, 1.0F, false);
            }
         }
      default:
      }
   }

   public void playAuxSFX(EntityPlayer par1EntityPlayer, int par2, int par3, int par4, int par5, int par6) {
      Random var7 = this.theWorld.rand;
      double var8;
      double var10;
      double var12;
      String var14;
      int var15;
      double var23;
      int var20;
      double var25;
      double var27;
      double var29;
      double var39;
      switch(par2) {
      case 1000:
         this.theWorld.playSound((double)par3, (double)par4, (double)par5, "random.click", 1.0F, 1.0F, false);
         break;
      case 1001:
         this.theWorld.playSound((double)par3, (double)par4, (double)par5, "random.click", 1.0F, 1.2F, false);
         break;
      case 1002:
         this.theWorld.playSound((double)par3, (double)par4, (double)par5, "random.bow", 1.0F, 1.2F, false);
         break;
      case 1003:
         if(Math.random() < 0.5D) {
            this.theWorld.playSound((double)par3 + 0.5D, (double)par4 + 0.5D, (double)par5 + 0.5D, "random.door_open", 1.0F, this.theWorld.rand.nextFloat() * 0.1F + 0.9F, false);
         } else {
            this.theWorld.playSound((double)par3 + 0.5D, (double)par4 + 0.5D, (double)par5 + 0.5D, "random.door_close", 1.0F, this.theWorld.rand.nextFloat() * 0.1F + 0.9F, false);
         }
         break;
      case 1004:
         this.theWorld.playSound((double)((float)par3 + 0.5F), (double)((float)par4 + 0.5F), (double)((float)par5 + 0.5F), "random.fizz", 0.5F, 2.6F + (var7.nextFloat() - var7.nextFloat()) * 0.8F, false);
         break;
      case 1005:
         if(Item.itemsList[par6] instanceof ItemRecord) {
            this.theWorld.playRecord(((ItemRecord)Item.itemsList[par6]).recordName, par3, par4, par5);
         } else {
            this.theWorld.playRecord((String)null, par3, par4, par5);
         }
         break;
      case 1007:
         this.theWorld.playSound((double)par3 + 0.5D, (double)par4 + 0.5D, (double)par5 + 0.5D, "mob.ghast.charge", 10.0F, (var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
         break;
      case 1008:
         this.theWorld.playSound((double)par3 + 0.5D, (double)par4 + 0.5D, (double)par5 + 0.5D, "mob.ghast.fireball", 10.0F, (var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
         break;
      case 1009:
         this.theWorld.playSound((double)par3 + 0.5D, (double)par4 + 0.5D, (double)par5 + 0.5D, "mob.ghast.fireball", 2.0F, (var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
         break;
      case 1010:
         this.theWorld.playSound((double)par3 + 0.5D, (double)par4 + 0.5D, (double)par5 + 0.5D, "mob.zombie.wood", 2.0F, (var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
         break;
      case 1011:
         this.theWorld.playSound((double)par3 + 0.5D, (double)par4 + 0.5D, (double)par5 + 0.5D, "mob.zombie.metal", 2.0F, (var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
         break;
      case 1012:
         this.theWorld.playSound((double)par3 + 0.5D, (double)par4 + 0.5D, (double)par5 + 0.5D, "mob.zombie.woodbreak", 2.0F, (var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
         break;
      case 1014:
         this.theWorld.playSound((double)par3 + 0.5D, (double)par4 + 0.5D, (double)par5 + 0.5D, "mob.wither.shoot", 2.0F, (var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
         break;
      case 1015:
         this.theWorld.playSound((double)par3 + 0.5D, (double)par4 + 0.5D, (double)par5 + 0.5D, "mob.bat.takeoff", 0.05F, (var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
         break;
      case 1016:
         this.theWorld.playSound((double)par3 + 0.5D, (double)par4 + 0.5D, (double)par5 + 0.5D, "mob.zombie.infect", 2.0F, (var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
         break;
      case 1017:
         this.theWorld.playSound((double)par3 + 0.5D, (double)par4 + 0.5D, (double)par5 + 0.5D, "mob.zombie.unfect", 2.0F, (var7.nextFloat() - var7.nextFloat()) * 0.2F + 1.0F, false);
         break;
      case 1020:
         this.theWorld.playSound((double)((float)par3 + 0.5F), (double)((float)par4 + 0.5F), (double)((float)par5 + 0.5F), "random.anvil_break", 1.0F, this.theWorld.rand.nextFloat() * 0.1F + 0.9F, false);
         break;
      case 1021:
         this.theWorld.playSound((double)((float)par3 + 0.5F), (double)((float)par4 + 0.5F), (double)((float)par5 + 0.5F), "random.anvil_use", 1.0F, this.theWorld.rand.nextFloat() * 0.1F + 0.9F, false);
         break;
      case 1022:
         this.theWorld.playSound((double)((float)par3 + 0.5F), (double)((float)par4 + 0.5F), (double)((float)par5 + 0.5F), "random.anvil_land", 0.3F, this.theWorld.rand.nextFloat() * 0.1F + 0.9F, false);
         break;
      case 2000:
         int var33 = par6 % 3 - 1;
         int var9 = par6 / 3 % 3 - 1;
         var10 = (double)par3 + (double)var33 * 0.6D + 0.5D;
         var12 = (double)par4 + 0.5D;
         double var34 = (double)par5 + (double)var9 * 0.6D + 0.5D;

         for(int var44 = 0; var44 < 10; ++var44) {
            double var43 = var7.nextDouble() * 0.2D + 0.01D;
            double var48 = var10 + (double)var33 * 0.01D + (var7.nextDouble() - 0.5D) * (double)var9 * 0.5D;
            var39 = var12 + (var7.nextDouble() - 0.5D) * 0.5D;
            var23 = var34 + (double)var9 * 0.01D + (var7.nextDouble() - 0.5D) * (double)var33 * 0.5D;
            var25 = (double)var33 * var43 + var7.nextGaussian() * 0.01D;
            var27 = -0.03D + var7.nextGaussian() * 0.01D;
            var29 = (double)var9 * var43 + var7.nextGaussian() * 0.01D;
            this.spawnParticle("smoke", var48, var39, var23, var25, var27, var29);
         }

         return;
      case 2001:
         var20 = par6 & 4095;
         if(var20 > 0) {
            Block var42 = Block.blocksList[var20];
            this.mc.sndManager.playSound(var42.stepSound.getBreakSound(), (float)par3 + 0.5F, (float)par4 + 0.5F, (float)par5 + 0.5F, (var42.stepSound.getVolume() + 1.0F) / 2.0F, var42.stepSound.getPitch() * 0.8F);
         }

         this.mc.effectRenderer.addBlockDestroyEffects(par3, par4, par5, par6 & 4095, par6 >> 12 & 255);
         break;
      case 2002:
         var8 = (double)par3;
         var10 = (double)par4;
         var12 = (double)par5;
         var14 = "iconcrack_" + Item.potion.itemID;

         for(var15 = 0; var15 < 8; ++var15) {
            this.spawnParticle(var14, var8, var10, var12, var7.nextGaussian() * 0.15D, var7.nextDouble() * 0.2D, var7.nextGaussian() * 0.15D);
         }

         var15 = Item.potion.getColorFromDamage(par6);
         float var16 = (float)(var15 >> 16 & 255) / 255.0F;
         float var17 = (float)(var15 >> 8 & 255) / 255.0F;
         float var18 = (float)(var15 >> 0 & 255) / 255.0F;
         String var19 = "spell";
         if(Item.potion.isEffectInstant(par6)) {
            var19 = "instantSpell";
         }

         for(var20 = 0; var20 < 100; ++var20) {
            var39 = var7.nextDouble() * 4.0D;
            var23 = var7.nextDouble() * 3.141592653589793D * 2.0D;
            var25 = Math.cos(var23) * var39;
            var27 = 0.01D + var7.nextDouble() * 0.5D;
            var29 = Math.sin(var23) * var39;
            EntityFX var45 = this.doSpawnParticle(var19, var8 + var25 * 0.1D, var10 + 0.3D, var12 + var29 * 0.1D, var25, var27, var29);
            if(var45 != null) {
               float var46 = 0.75F + var7.nextFloat() * 0.25F;
               var45.setRBGColorF(var16 * var46, var17 * var46, var18 * var46);
               var45.multiplyVelocity((float)var39);
            }
         }

         this.theWorld.playSound((double)par3 + 0.5D, (double)par4 + 0.5D, (double)par5 + 0.5D, "random.glass", 1.0F, this.theWorld.rand.nextFloat() * 0.1F + 0.9F, false);
         break;
      case 2003:
         var8 = (double)par3 + 0.5D;
         var10 = (double)par4;
         var12 = (double)par5 + 0.5D;
         var14 = "iconcrack_" + Item.eyeOfEnder.itemID;

         for(var15 = 0; var15 < 8; ++var15) {
            this.spawnParticle(var14, var8, var10, var12, var7.nextGaussian() * 0.15D, var7.nextDouble() * 0.2D, var7.nextGaussian() * 0.15D);
         }

         for(double var47 = 0.0D; var47 < 6.283185307179586D; var47 += 0.15707963267948966D) {
            this.spawnParticle("portal", var8 + Math.cos(var47) * 5.0D, var10 - 0.4D, var12 + Math.sin(var47) * 5.0D, Math.cos(var47) * -5.0D, 0.0D, Math.sin(var47) * -5.0D);
            this.spawnParticle("portal", var8 + Math.cos(var47) * 5.0D, var10 - 0.4D, var12 + Math.sin(var47) * 5.0D, Math.cos(var47) * -7.0D, 0.0D, Math.sin(var47) * -7.0D);
         }

         return;
      case 2004:
         for(int var21 = 0; var21 < 20; ++var21) {
            double var22 = (double)par3 + 0.5D + ((double)this.theWorld.rand.nextFloat() - 0.5D) * 2.0D;
            double var24 = (double)par4 + 0.5D + ((double)this.theWorld.rand.nextFloat() - 0.5D) * 2.0D;
            double var26 = (double)par5 + 0.5D + ((double)this.theWorld.rand.nextFloat() - 0.5D) * 2.0D;
            this.theWorld.spawnParticle("smoke", var22, var24, var26, 0.0D, 0.0D, 0.0D);
            this.theWorld.spawnParticle("flame", var22, var24, var26, 0.0D, 0.0D, 0.0D);
         }

         return;
      case 2005:
         ItemDye.func_96603_a(this.theWorld, par3, par4, par5, par6);
      }

   }

   public void destroyBlockPartially(int par1, int par2, int par3, int par4, int par5) {
      if(par5 >= 0 && par5 < 10) {
         DestroyBlockProgress var6 = (DestroyBlockProgress)this.damagedBlocks.get(Integer.valueOf(par1));
         if(var6 == null || var6.getPartialBlockX() != par2 || var6.getPartialBlockY() != par3 || var6.getPartialBlockZ() != par4) {
            var6 = new DestroyBlockProgress(par1, par2, par3, par4);
            this.damagedBlocks.put(Integer.valueOf(par1), var6);
         }

         var6.setPartialBlockDamage(par5);
         var6.setCloudUpdateTick(this.cloudTickCounter);
      } else {
         this.damagedBlocks.remove(Integer.valueOf(par1));
      }

   }

   public void registerDestroyBlockIcons(IconRegister par1IconRegister) {
      this.destroyBlockIcons = new Icon[10];

      for(int var2 = 0; var2 < this.destroyBlockIcons.length; ++var2) {
         this.destroyBlockIcons[var2] = par1IconRegister.registerIcon("destroy_" + var2);
      }

   }

   public void setAllRenderersVisible() {
      if(this.worldRenderers != null) {
         for(int i = 0; i < this.worldRenderers.length; ++i) {
            this.worldRenderers[i].isVisible = true;
         }

      }
   }

   public boolean isMoving(EntityLiving entityliving) {
      boolean moving = this.isMovingNow(entityliving);
      if(moving) {
         this.lastMovedTime = System.currentTimeMillis();
         return true;
      } else {
         return System.currentTimeMillis() - this.lastMovedTime < 2000L;
      }
   }

   private boolean isMovingNow(EntityLiving entityliving) {
      double maxDiff = 0.001D;
      return entityliving.isJumping?true:(entityliving.isSneaking()?true:((double)entityliving.prevSwingProgress > maxDiff?true:(this.mc.mouseHelper.deltaX != 0?true:(this.mc.mouseHelper.deltaY != 0?true:(Math.abs(entityliving.posX - entityliving.prevPosX) > maxDiff?true:(Math.abs(entityliving.posY - entityliving.prevPosY) > maxDiff?true:Math.abs(entityliving.posZ - entityliving.prevPosZ) > maxDiff))))));
   }

   public boolean isActing() {
      boolean acting = this.isActingNow();
      if(acting) {
         this.lastActionTime = System.currentTimeMillis();
         return true;
      } else {
         return System.currentTimeMillis() - this.lastActionTime < 500L;
      }
   }

   public boolean isActingNow() {
      return Mouse.isButtonDown(0)?true:Mouse.isButtonDown(1);
   }

   public int renderAllSortedRenderers(int renderPass, double partialTicks) {
      return this.renderSortedRenderers(0, this.sortedWorldRenderers.length, renderPass, partialTicks);
   }

   public void updateCapes() {
      if(this.theWorld != null) {
         boolean showCapes = Config.isShowCapes();
         List playerList = this.theWorld.playerEntities;

         for(int i = 0; i < playerList.size(); ++i) {
            Entity entity = (Entity)playerList.get(i);
            if(entity instanceof EntityPlayer) {
               EntityPlayer player = (EntityPlayer)entity;
               if(showCapes) {
                  player.cloakUrl = player.cloakUrl;
               } else {
                  player.cloakUrl = "";
               }
            }
         }

      }
   }
}
