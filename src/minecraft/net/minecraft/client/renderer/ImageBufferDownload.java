package net.minecraft.client.renderer;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.ImageObserver;
import net.minecraft.client.renderer.IImageBuffer;

public class ImageBufferDownload implements IImageBuffer {

   private int[] imageData;
   private int imageWidth;
   private int imageHeight;


   public BufferedImage parseUserSkin(BufferedImage par1BufferedImage) {
      if (par1BufferedImage == null)
        {
            return null;
        }
        else
        {
            imageWidth = par1BufferedImage.getWidth(null);
            imageHeight = par1BufferedImage.getHeight(null);
            BufferedImage bufferedimage = new BufferedImage(imageWidth, imageHeight, 2);
            Graphics g = bufferedimage.getGraphics();
            g.drawImage(par1BufferedImage, 0, 0, null);
            g.dispose();
            imageData = ((DataBufferInt)bufferedimage.getRaster().getDataBuffer()).getData();

            /* Магия. Разобравшемуся отписаться в комемментах. */
            /* Никто не понимает, зачем это нужно */
            this.setAreaOpaque(0, 0, 32, 16);
            this.setAreaTransparent(32, 0, 64, 32);
            this.setAreaOpaque(0, 16, 64, 32);

            boolean var4 = false;
            int var5;
            int var6;
            int var7;

            for (var5 = 32; var5 < 64; ++var5)
            {
                for (var6 = 0; var6 < 16; ++var6)
                {
                    var7 = this.imageData[var5 + var6 * 64];

                    if ((var7 >> 24 & 255) < 128)
                    {
                        var4 = true;
                    }
                }
            }

            if (!var4)
            {
                for (var5 = 32; var5 < 64; ++var5)
                {
                    for (var6 = 0; var6 < 16; ++var6)
                    {
                        var7 = this.imageData[var5 + var6 * 64];

                        if ((var7 >> 24 & 255) < 128)
                        {
                            var4 = true;
                        }
                    }
                }
            }

            return bufferedimage;
        }
   }

   private void setAreaTransparent(int par1, int par2, int par3, int par4) {
      if(!this.hasTransparency(par1, par2, par3, par4)) {
         for(int var5 = par1; var5 < par3; ++var5) {
            for(int var6 = par2; var6 < par4; ++var6) {
               this.imageData[var5 + var6 * this.imageWidth] &= 16777215;
            }
         }
      }

   }

   private void setAreaOpaque(int par1, int par2, int par3, int par4) {
      for(int var5 = par1; var5 < par3; ++var5) {
         for(int var6 = par2; var6 < par4; ++var6) {
            this.imageData[var5 + var6 * this.imageWidth] |= -16777216;
         }
      }

   }

   private boolean hasTransparency(int par1, int par2, int par3, int par4) {
      for(int var5 = par1; var5 < par3; ++var5) {
         for(int var6 = par2; var6 < par4; ++var6) {
            int var7 = this.imageData[var5 + var6 * this.imageWidth];
            if((var7 >> 24 & 255) < 128) {
               return true;
            }
         }
      }

      return false;
   }
}
