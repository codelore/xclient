package net.minecraft.client.gui;

import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.Bidi;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import javax.imageio.ImageIO;
import net.minecraft.client.renderer.RenderEngine;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.texturepacks.ITexturePack;
import net.minecraft.src.Config;
import net.minecraft.util.ChatAllowedCharacters;
import org.lwjgl.opengl.GL11;
import betterfonts.StringCache;
import betterfonts.ConfigParser;

public class FontRenderer {
	
	public static boolean betterFontsEnabled = true;
	public StringCache stringCache;
	public boolean dropShadowEnabled = true;
	
    private float[] charWidth = new float[256];
    public int FONT_HEIGHT = 9;
    public Random fontRandom = new Random();
    private byte[] glyphWidth = new byte[65536];
    private int[] colorCode = new int[32];
    private final String fontTextureName;
    private RenderEngine renderEngine;
    private float posX;
    private float posY;
    public boolean unicodeFlag;
    private boolean bidiFlag;
    private float red;
    private float blue;
    private float green;
    private float alpha;
    private int textColor;
    private boolean randomStyle = false;
    private boolean boldStyle = false;
    private boolean italicStyle = false;
    private boolean underlineStyle = false;
    private boolean strikethroughStyle = false;
    public GameSettings gameSettings;
    public boolean enabled = true;


    FontRenderer() {
        this.renderEngine = null;
        this.fontTextureName = null;
    }

    public FontRenderer(GameSettings par1GameSettings, String par2Str, RenderEngine par3RenderEngine, boolean par4) {
        this.gameSettings = par1GameSettings;
        this.fontTextureName = par2Str;
        this.renderEngine = par3RenderEngine;
        this.unicodeFlag = par4;
        this.readFontData();
        par3RenderEngine.bindTexture(par2Str);

        for(int var5 = 0; var5 < 32; ++var5) {
            int var6 = (var5 >> 3 & 1) * 85;
            int var7 = (var5 >> 2 & 1) * 170 + var6;
            int var8 = (var5 >> 1 & 1) * 170 + var6;
            int var9 = (var5 >> 0 & 1) * 170 + var6;
            if(var5 == 6) {
                var7 += 85;
            }

            if(par1GameSettings.anaglyph) {
                int var10 = (var7 * 30 + var8 * 59 + var9 * 11) / 100;
                int var11 = (var7 * 30 + var8 * 70) / 100;
                int var12 = (var7 * 30 + var9 * 70) / 100;
                var7 = var10;
                var8 = var11;
                var9 = var12;
            }

            if(var5 >= 16) {
                var7 /= 4;
                var8 /= 4;
                var9 /= 4;
            }

            this.colorCode[var5] = (var7 & 255) << 16 | (var8 & 255) << 8 | var9 & 255;
        }
        /*
         * Only use OpenType rendering for the primary FontRenderer and not for the enchantment table Standard Galactic renderer.
         * Also, mcpatcher will call initialize() when switching texture packs to reload the bitmap font, but the StringCache
         * should not be re-created a second time or it will leak OpenGL textures.
         */
        if(par2Str.equals("/font/default.png") && this.stringCache == null)
        {
            this.stringCache = new StringCache(this.colorCode);

            /* Read optional config file to override the default font name/size */
            ConfigParser config = new ConfigParser();
            if(config.loadConfig("/config/BetterFonts.cfg"))
            {
                String fontName = config.getFontName("SansSerif");
               int fontSize = config.getFontSize(18);
               boolean antiAlias = config.getBoolean("font.antialias", false);
               dropShadowEnabled = config.getBoolean("font.dropshadow", true);

                this.stringCache.setDefaultFont(fontName, fontSize, antiAlias);
                System.out.println("BetterFonts configuration loaded");
            }
        }    

    }

    public void readFontData() {
        this.readGlyphSizes();
        this.readFontTexture(this.fontTextureName);
    }

    private void readFontTexture(String par1Str) {
        BufferedImage bufferedimage;
        try {
            par1Str = fixHdFontName(par1Str);
            bufferedimage = ImageIO.read(getFontTexturePack().getResourceAsStream(par1Str));
        } catch (IOException var19) {
            throw new RuntimeException(var19);
        }

        int imgWidth = bufferedimage.getWidth();
        int imgHeight = bufferedimage.getHeight();
        int charW = imgWidth / 16;
        int charH = imgHeight / 16;
        float kx = (float)imgWidth / 128.0F;
        int[] ai = new int[imgWidth * imgHeight];
        bufferedimage.getRGB(0, 0, imgWidth, imgHeight, ai, 0, imgWidth);
        int k = 0;

        while(k < 256) {
            int cx = k % 16;
            int cy = k / 16;
            boolean px = false;
            int var20 = charW - 1;

            while(true) {
                if(var20 >= 0) {
                    int x = cx * charW + var20;
                    boolean flag = true;

                    for(int py = 0; py < charH && flag; ++py) {
                        int ypos = (cy * charH + py) * imgWidth;
                        int col = ai[x + ypos];
                        int al = col >> 24 & 255;
                        if(al > 16) {
                            flag = false;
                        }
                    }

                    if(flag) {
                        --var20;
                        continue;
                    }
                }

                if(k == 65) {
                    k = k;
                }

                if(k == 32) {
                    if(charW <= 8) {
                        var20 = (int)(2.0F * kx);
                    } else {
                        var20 = (int)(1.5F * kx);
                    }
                }

                this.charWidth[k] = (float)(var20 + 1) / kx + 1.0F;
                ++k;
                break;
            }
        }

        this.readCustomCharWidths();
    }

    private void readGlyphSizes() {
        try {
            InputStream var2 = getFontTexturePack().getResourceAsStream("/font/glyph_sizes.bin");
            var2.read(this.glyphWidth);
        } catch (IOException var21) {
            throw new RuntimeException(var21);
        }
    }

    private float renderCharAtPos(int par1, char par2, boolean par3) {
        return par2 == 32?this.charWidth[par2]:(par1 > 0 && !this.unicodeFlag?this.renderDefaultChar(par1 + 32, par3):this.renderUnicodeChar(par2, par3));
    }

    private float renderDefaultChar(int par1, boolean par2) {
        float var3 = (float)(par1 % 16 * 8);
        float var4 = (float)(par1 / 16 * 8);
        float var5 = par2?1.0F:0.0F;
        this.renderEngine.bindTexture(this.fontTextureName);
        float var6 = this.charWidth[par1] - 0.01F;
        GL11.glBegin(5);
        GL11.glTexCoord2f(var3 / 128.0F, var4 / 128.0F);
        GL11.glVertex3f(this.posX + var5, this.posY, 0.0F);
        GL11.glTexCoord2f(var3 / 128.0F, (var4 + 7.99F) / 128.0F);
        GL11.glVertex3f(this.posX - var5, this.posY + 7.99F, 0.0F);
        GL11.glTexCoord2f((var3 + var6) / 128.0F, var4 / 128.0F);
        GL11.glVertex3f(this.posX + var6 + var5, this.posY, 0.0F);
        GL11.glTexCoord2f((var3 + var6) / 128.0F, (var4 + 7.99F) / 128.0F);
        GL11.glVertex3f(this.posX + var6 - var5, this.posY + 7.99F, 0.0F);
        GL11.glEnd();
        return this.charWidth[par1];
    }

    private void loadGlyphTexture(int par1) {
        String var2 = String.format("/font/glyph_%02X.png", new Object[]{Integer.valueOf(par1)});
        this.renderEngine.bindTexture(var2);
    }

    private float renderUnicodeChar(char par1, boolean par2) {
        if(this.glyphWidth[par1] == 0) {
            return 0.0F;
        } else {
            int var3 = par1 / 256;
            this.loadGlyphTexture(var3);
            int var4 = this.glyphWidth[par1] >>> 4;
            int var5 = this.glyphWidth[par1] & 15;
            float var6 = (float)var4;
            float var7 = (float)(var5 + 1);
            float var8 = (float)(par1 % 16 * 16) + var6;
            float var9 = (float)((par1 & 255) / 16 * 16);
            float var10 = var7 - var6 - 0.02F;
            float var11 = par2?1.0F:0.0F;
            GL11.glBegin(5);
            GL11.glTexCoord2f(var8 / 256.0F, var9 / 256.0F);
            GL11.glVertex3f(this.posX + var11, this.posY, 0.0F);
            GL11.glTexCoord2f(var8 / 256.0F, (var9 + 15.98F) / 256.0F);
            GL11.glVertex3f(this.posX - var11, this.posY + 7.99F, 0.0F);
            GL11.glTexCoord2f((var8 + var10) / 256.0F, var9 / 256.0F);
            GL11.glVertex3f(this.posX + var10 / 2.0F + var11, this.posY, 0.0F);
            GL11.glTexCoord2f((var8 + var10) / 256.0F, (var9 + 15.98F) / 256.0F);
            GL11.glVertex3f(this.posX + var10 / 2.0F - var11, this.posY + 7.99F, 0.0F);
            GL11.glEnd();
            return (var7 - var6) / 2.0F + 1.0F;
        }
    }

    public int drawStringWithShadow(String par1Str, int par2, int par3, int par4) {
        return this.drawString(par1Str, par2, par3, par4, true);
    }

    public int drawString(String par1Str, int par2, int par3, int par4) {
        return !this.enabled?0:this.drawString(par1Str, par2, par3, par4, false);
    }

    public int drawString(String par1Str, int par2, int par3, int par4, boolean par5) {
        this.resetStyles();
        if(this.bidiFlag) {
            par1Str = this.bidiReorder(par1Str);
        }

        int var6;
        if (par5 && this.dropShadowEnabled) {
            var6 = this.renderString(par1Str, par2 + 1, par3 + 1, par4, true);
            var6 = Math.max(var6, this.renderString(par1Str, par2, par3, par4, false));
        } else {
            var6 = this.renderString(par1Str, par2, par3, par4, false);
        }

        return var6;
    }

    private String bidiReorder(String par1Str) {
    	if (this.betterFontsEnabled && this.stringCache != null)
    	{
    		            return par1Str;
    		        }
        if(par1Str != null && Bidi.requiresBidi(par1Str.toCharArray(), 0, par1Str.length())) {
            Bidi var2 = new Bidi(par1Str, -2);
            byte[] var3 = new byte[var2.getRunCount()];
            String[] var4 = new String[var3.length];

            int var7;
            for(int var11 = 0; var11 < var3.length; ++var11) {
                int var12 = var2.getRunStart(var11);
                var7 = var2.getRunLimit(var11);
                int var13 = var2.getRunLevel(var11);
                String var14 = par1Str.substring(var12, var7);
                var3[var11] = (byte)var13;
                var4[var11] = var14;
            }

            String[] var111 = (String[])((String[])var4.clone());
            Bidi.reorderVisually(var3, 0, var4, 0, var3.length);
            StringBuilder var121 = new StringBuilder();
            var7 = 0;

            while(var7 < var4.length) {
                byte var131 = var3[var7];
                int var141 = 0;

                while(true) {
                    if(var141 < var111.length) {
                        if(!var111[var141].equals(var4[var7])) {
                            ++var141;
                            continue;
                        }

                        var131 = var3[var141];
                    }

                    if((var131 & 1) == 0) {
                        var121.append(var4[var7]);
                    } else {
                        for(var141 = var4[var7].length() - 1; var141 >= 0; --var141) {
                            char var10 = var4[var7].charAt(var141);
                            if(var10 == 40) {
                                var10 = 41;
                            } else if(var10 == 41) {
                                var10 = 40;
                            }

                            var121.append(var10);
                        }
                    }

                    ++var7;
                    break;
                }
            }

            return var121.toString();
        } else {
            return par1Str;
        }
    }

    private void resetStyles() {
        this.randomStyle = false;
        this.boldStyle = false;
        this.italicStyle = false;
        this.underlineStyle = false;
        this.strikethroughStyle = false;
    }

    /**
     * Added by limito
     *
     * @return color from hex code, -1 if code is invalid
     */
    private static int getColorFromHexCode(String hexCode) {
        try {
            return Integer.parseInt(hexCode, 16);
        } catch (NumberFormatException ex) {
            return -1;
        }
    }

    /**
     * Modified to use HTML-like color codes (limito)
     */
    private void renderStringAtPos(String par1Str, boolean par2)
    {
        for (int var3 = 0; var3 < par1Str.length(); ++var3)
        {
            char var4 = par1Str.charAt(var3);
            int var5;
            int var6;

            boolean isFormatCode = false;
            if ((var4 == 167 && var3 + 1 < par1Str.length()))
            {
                if (var4 == 167) {
                    isFormatCode = true;
                    var5 = "0123456789abcdefklmnor".indexOf(par1Str.toLowerCase().charAt(var3 + 1));

                    if (var5 < 16) {
                        this.randomStyle = false;
                        this.boldStyle = false;
                        this.strikethroughStyle = false;
                        this.underlineStyle = false;
                        this.italicStyle = false;

                        if (var5 < 0 || var5 > 15) {
                            var5 = 15;
                        }

                        if (par2) {
                            var5 += 16;
                        }

                        var6 = this.colorCode[var5];
                        this.textColor = var6;
                        GL11.glColor4f((float) (var6 >> 16) / 255.0F, (float) (var6 >> 8 & 255) / 255.0F, (float) (var6 & 255) / 255.0F, this.alpha);
                    } else if (var5 == 16) {
                        this.randomStyle = true;
                    } else if (var5 == 17) {
                        this.boldStyle = true;
                    } else if (var5 == 18) {
                        this.strikethroughStyle = true;
                    } else if (var5 == 19) {
                        this.underlineStyle = true;
                    } else if (var5 == 20) {
                        this.italicStyle = true;
                    } else if (var5 == 21) {
                        this.randomStyle = false;
                        this.boldStyle = false;
                        this.strikethroughStyle = false;
                        this.underlineStyle = false;
                        this.italicStyle = false;
                        GL11.glColor4f(this.red, this.blue, this.green, this.alpha);
                    }

                    ++var3;
                }
            }  else if (var4 == '#' && var3 + 6 < par1Str.length()) {
                // Hex code
                int color = getColorFromHexCode(par1Str.substring(var3 + 1, var3 + 7));
                if (color >= 0) {
                    isFormatCode = true;
                    int red = color >> 16;
                    int green = (color >> 8) & 0xff;
                    int blue = color & 0xff;
                    if (par2) {
                        red /= 4;
                        green /= 4;
                        blue /= 4;
                    }
                    this.textColor = red << 16 | green << 8 | blue;
                    GL11.glColor4f(red / 255.0F, green / 255.0F, blue / 255.0F, this.alpha);
                    var3 += 6;
                }
            }
            if (!isFormatCode)
            {

                var5 = ChatAllowedCharacters.allowedCharacters.indexOf(var4);

                if (this.randomStyle && var5 > 0)
                {
                    do
                    {
                        var6 = this.fontRandom.nextInt(ChatAllowedCharacters.allowedCharacters.length());
                    }
                    while (this.charWidth[var5 + 32] != this.charWidth[var6 + 32]);

                    var5 = var6;
                }

                float var9 = this.renderCharAtPos(var5, var4, this.italicStyle);

                if (this.boldStyle)
                {
                    ++this.posX;
                    this.renderCharAtPos(var5, var4, this.italicStyle);
                    --this.posX;
                    ++var9;
                }

                Tessellator var7;

                if (this.strikethroughStyle)
                {
                    var7 = Tessellator.instance;
                    GL11.glDisable(GL11.GL_TEXTURE_2D);
                    var7.startDrawingQuads();
                    var7.addVertex((double)this.posX, (double)(this.posY + (float)(this.FONT_HEIGHT / 2)), 0.0D);
                    var7.addVertex((double)(this.posX + var9), (double)(this.posY + (float)(this.FONT_HEIGHT / 2)), 0.0D);
                    var7.addVertex((double)(this.posX + var9), (double)(this.posY + (float)(this.FONT_HEIGHT / 2) - 1.0F), 0.0D);
                    var7.addVertex((double)this.posX, (double)(this.posY + (float)(this.FONT_HEIGHT / 2) - 1.0F), 0.0D);
                    var7.draw();
                    GL11.glEnable(GL11.GL_TEXTURE_2D);
                }

                if (this.underlineStyle)
                {
                    var7 = Tessellator.instance;
                    GL11.glDisable(GL11.GL_TEXTURE_2D);
                    var7.startDrawingQuads();
                    int var8 = this.underlineStyle ? -1 : 0;
                    var7.addVertex((double)(this.posX + (float)var8), (double)(this.posY + (float)this.FONT_HEIGHT), 0.0D);
                    var7.addVertex((double)(this.posX + var9), (double)(this.posY + (float)this.FONT_HEIGHT), 0.0D);
                    var7.addVertex((double)(this.posX + var9), (double)(this.posY + (float)this.FONT_HEIGHT - 1.0F), 0.0D);
                    var7.addVertex((double)(this.posX + (float)var8), (double)(this.posY + (float)this.FONT_HEIGHT - 1.0F), 0.0D);
                    var7.draw();
                    GL11.glEnable(GL11.GL_TEXTURE_2D);
                }

                this.posX += (float)((int)var9);
            }
        }

    }

    private int renderStringAligned(String par1Str, int par2, int par3, int par4, int par5, boolean par6) {
        if(this.bidiFlag) {
            par1Str = this.bidiReorder(par1Str);
            int var7 = this.getStringWidth(par1Str);
            par2 = par2 + par4 - var7;
        }

        return this.renderString(par1Str, par2, par3, par5, par6);
    }

    private int renderString(String par1Str, int par2, int par3, int par4, boolean par5) {
        if(par1Str == null) {
            return 0;
        } else {
        	this.renderEngine.resetBoundTexture();
        	
            if((par4 & -67108864) == 0) {
                par4 |= -16777216;
            }

            if(par5) {
                par4 = (par4 & 16579836) >> 2 | par4 & -16777216;
            }

            this.red = (float)(par4 >> 16 & 255) / 255.0F;
            this.blue = (float)(par4 >> 8 & 255) / 255.0F;
            this.green = (float)(par4 & 255) / 255.0F;
            this.alpha = (float)(par4 >> 24 & 255) / 255.0F;
            GL11.glColor4f(this.red, this.blue, this.green, this.alpha);
            this.posX = (float)par2;
            this.posY = (float)par3;
                        if (this.betterFontsEnabled && this.stringCache != null)
            	            {
            	                this.posX += stringCache.renderString(par1Str, par2, par3, par4, par5);
            	            }
            	            else
            	            {
            	                this.renderStringAtPos(par1Str, par5);
            	            }            return (int)this.posX;
        }
    }

    public int getStringWidth(String par1Str) {
    	        if (this.betterFontsEnabled && this.stringCache != null)
    		        {
    		            return this.stringCache.getStringWidth(par1Str);
    		        }
        if (par1Str == null)
        {
            return 0;
        }
        else
        {
            int var2 = 0;
            boolean var3 = false;

            for (int var4 = 0; var4 < par1Str.length(); ++var4)
            {
                char var5 = par1Str.charAt(var4);
                int var6 = this.getCharWidth(var5);

                if (var6 < 0 && var4 < par1Str.length() - 1)
                {
                    ++var4;
                    var5 = par1Str.charAt(var4);

                    if (var5 != 108 && var5 != 76)
                    {
                        if (var5 == 114 || var5 == 82)
                        {
                            var3 = false;
                        }
                    }
                    else
                    {
                        var3 = true;
                    }

                    var6 = 0;
                }
                if (var5 == '#' && var4 + 6 < par1Str.length() && getColorFromHexCode(par1Str.substring(var4 + 1, var4 + 7)) >= 0) {
                    var4 += 6;
                    var6 = 0;
                }

                var2 += var6;

                if (var3)
                {
                    ++var2;
                }
            }

            return var2;
        }
    }

    public int getCharWidth(char par1) {
        return Math.round(this.getCharWidthFloat(par1));
    }

    private float getCharWidthFloat(char par1) {
        if(par1 == 167) {
            return -1.0F;
        } else if(par1 == 32) {
            return this.charWidth[32];
        } else {
            int var2 = ChatAllowedCharacters.allowedCharacters.indexOf(par1);
            if(var2 >= 0 && !this.unicodeFlag) {
                return this.charWidth[var2 + 32];
            } else if(this.glyphWidth[par1] != 0) {
                int var3 = this.glyphWidth[par1] >>> 4;
                int var4 = this.glyphWidth[par1] & 15;
                if(var4 > 7) {
                    var4 = 15;
                    var3 = 0;
                }

                ++var4;
                return (float)((var4 - var3) / 2 + 1);
            } else {
                return 0.0F;
            }
        }
    }

    public String trimStringToWidth(String par1Str, int par2) {
        return this.trimStringToWidth(par1Str, par2, false);
    }

    public String trimStringToWidth(String par1Str, int par2, boolean par3) {
    	        if (this.betterFontsEnabled && this.stringCache != null)
    		        {
    		            return this.stringCache.trimStringToWidth(par1Str, par2, par3);
    		        }
        StringBuilder var4 = new StringBuilder();
        float var5 = 0.0F;
        int var6 = par3?par1Str.length() - 1:0;
        int var7 = par3?-1:1;
        boolean var8 = false;
        boolean var9 = false;

        for(int var10 = var6; var10 >= 0 && var10 < par1Str.length() && var5 < (float)par2; var10 += var7) {
            char var11 = par1Str.charAt(var10);
            float var12 = this.getCharWidthFloat(var11);
            if(var8) {
                var8 = false;
                if(var11 != 108 && var11 != 76) {
                    if(var11 == 114 || var11 == 82) {
                        var9 = false;
                    }
                } else {
                    var9 = true;
                }
            } else if(var12 < 0.0F) {
                var8 = true;
            } else {
                var5 += var12;
                if(var9) {
                    ++var5;
                }
            }

            if(var5 > (float)par2) {
                break;
            }

            if(par3) {
                var4.insert(0, var11);
            } else {
                var4.append(var11);
            }
        }

        return var4.toString();
    }

    private String trimStringNewline(String par1Str) {
        while(par1Str != null && par1Str.endsWith("\n")) {
            par1Str = par1Str.substring(0, par1Str.length() - 1);
        }

        return par1Str;
    }

    public void drawSplitString(String par1Str, int par2, int par3, int par4, int par5) {
        this.resetStyles();
        this.textColor = par5;
        par1Str = this.trimStringNewline(par1Str);
        this.renderSplitString(par1Str, par2, par3, par4, false);
    }

    private void renderSplitString(String par1Str, int par2, int par3, int par4, boolean par5) {
        List var6 = this.listFormattedStringToWidth(par1Str, par4);

        for(Iterator var7 = var6.iterator(); var7.hasNext(); par3 += this.FONT_HEIGHT) {
            String var8 = (String)var7.next();
            this.renderStringAligned(var8, par2, par3, par4, this.textColor, par5);
        }

    }

    public int splitStringWidth(String par1Str, int par2) {
        return this.FONT_HEIGHT * this.listFormattedStringToWidth(par1Str, par2).size();
    }

    public void setUnicodeFlag(boolean par1) {
        this.unicodeFlag = par1;
    }

    public boolean getUnicodeFlag() {
        return this.unicodeFlag;
    }

    public void setBidiFlag(boolean par1) {
        this.bidiFlag = par1;
    }

    public List listFormattedStringToWidth(String par1Str, int par2) {
        return Arrays.asList(this.wrapFormattedStringToWidth(par1Str, par2).split("\n"));
    }

    String wrapFormattedStringToWidth(String par1Str, int par2) {
        int var3 = this.sizeStringToWidth(par1Str, par2);
        if(par1Str.length() <= var3) {
            return par1Str;
        } else {
            String var4 = par1Str.substring(0, var3);
            char var5 = par1Str.charAt(var3);
            boolean var6 = var5 == 32 || var5 == 10;
            String var7 = getFormatFromString(var4) + par1Str.substring(var3 + (var6?1:0));
            return var4 + "\n" + this.wrapFormattedStringToWidth(var7, par2);
        }
    }

    private int sizeStringToWidth(String par1Str, int par2) {
    	if (this.betterFontsEnabled && this.stringCache != null)
    		        {
    		            return this.stringCache.sizeStringToWidth(par1Str, par2);
    		        }
        int var3 = par1Str.length();
        int var4 = 0;
        int var5 = 0;
        int var6 = -1;

        for (boolean var7 = false; var5 < var3; ++var5)
        {
            char var8 = par1Str.charAt(var5);

            switch (var8)
            {
                case 10:
                    --var5;
                    break;
                case 167:
                    if (var5 < var3 - 1)
                    {
                        ++var5;
                        char var9 = par1Str.charAt(var5);

                        if (var9 != 108 && var9 != 76)
                        {
                            if (var9 == 114 || var9 == 82 || isFormatColor(var9))
                            {
                                var7 = false;
                            }
                        }
                        else
                        {
                            var7 = true;
                        }
                    }

                    break;
                case '#':
                    // Don;t count hex code
                    int hexCode = -1;
                    if (var5 < var3 - 6) {
                        hexCode = getColorFromHexCode(par1Str.substring(var5 + 1, var5 + 7));
                    }
                    if (hexCode >= 0) {
                        var7 = false;
                        var5 += 6;
                    } else {
                        var4 += this.getCharWidth(var8);

                        if (var7)
                        {
                            ++var4;
                        }
                    }
                    break;
                case 32:
                    var6 = var5;
                default:
                    var4 += this.getCharWidth(var8);

                    if (var7)
                    {
                        ++var4;
                    }
            }

            if (var8 == 10)
            {
                ++var5;
                var6 = var5;
                break;
            }

            if (var4 > par2)
            {
                break;
            }
        }

        return var5 != var3 && var6 != -1 && var6 < var5 ? var6 : var5;
    }

    private static boolean isFormatColor(char par0) {
        return par0 >= 48 && par0 <= 57 || par0 >= 97 && par0 <= 102 || par0 >= 65 && par0 <= 70;
    }

    private static boolean isFormatSpecial(char par0) {
        return par0 >= 107 && par0 <= 111 || par0 >= 75 && par0 <= 79 || par0 == 114 || par0 == 82;
    }

    /**
     * Digests a string for nonprinting formatting characters then returns a string containing only that formatting.
     *
     * Rewritten by limito (using HTML-like colorcodes)
     */
    private static String getFormatFromString(String par0Str)
    {
        String colorCode = "";
        String formatCode = "";

        for (int i = 0; i < par0Str.length();i++) {
            if (par0Str.charAt(i) == '#' && i + 6 < par0Str.length()) {
                String hcolorCode = par0Str.substring(i + 1, i + 7);
                int color = getColorFromHexCode(hcolorCode);
                if (color >= 0) {
                    colorCode = "#" + hcolorCode;
                }
            } else if (par0Str.charAt(i) == 167 && i + 1 < par0Str.length()) {
                char var4 = par0Str.charAt(i + 1);

                if (isFormatColor(var4))
                {
                    colorCode = "\u00a7" + var4;
                }
                else if (isFormatSpecial(var4))
                {
                    formatCode = formatCode + "\u00a7" + var4;
                }
            }
        }

        return colorCode + formatCode;
    }

    public boolean getBidiFlag() {
        return this.bidiFlag;
    }

    private void readCustomCharWidths() {
        String fontFileName = fixHdFontName(this.fontTextureName);
        String suffix = ".png";
        if(fontFileName.endsWith(suffix)) {
            String fileName = fontFileName.substring(0, fontFileName.length() - suffix.length()) + ".properties";

            try {
                InputStream e = getFontTexturePack().getResourceAsStream(fileName);
                if(e == null) {
                    return;
                }

                Config.log("Loading " + fileName);
                Properties props = new Properties();
                props.load(e);
                Set keySet = props.keySet();
                Iterator iter = keySet.iterator();

                while(iter.hasNext()) {
                    String key = (String)iter.next();
                    String prefix = "width.";
                    if(key.startsWith(prefix)) {
                        String numStr = key.substring(prefix.length());
                        int num = Config.parseInt(numStr, -1);
                        if(num >= 0 && num < this.charWidth.length) {
                            String value = props.getProperty(key);
                            float width = Config.parseFloat(value, -1.0F);
                            if(width >= 0.0F) {
                                this.charWidth[num] = width;
                            }
                        }
                    }
                }
            } catch (FileNotFoundException var14) {
                ;
            } catch (IOException var15) {
                var15.printStackTrace();
            }

        }
    }

    private static String fixHdFontName(String texName) {
        if(!Config.isCustomFonts()) {
            return texName;
        } else if(texName == null) {
            return texName;
        } else {
            String suffPng = ".png";
            if(!texName.endsWith(suffPng)) {
                return texName;
            } else {
                String baseName = texName.substring(0, texName.length() - suffPng.length());
                String hdName = baseName + "_hd" + suffPng;
                return getFontTexturePack().func_98138_b(hdName, true)?hdName:texName;
            }
        }
    }

    private static ITexturePack getFontTexturePack() {
        return Config.isCustomFonts()?Config.getMinecraft().texturePackList.getSelectedTexturePack():(ITexturePack)Config.getMinecraft().texturePackList.availableTexturePacks().get(0);
    }
}