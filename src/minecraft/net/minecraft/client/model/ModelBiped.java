package net.minecraft.client.model;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.util.MathHelper;
import org.lwjgl.opengl.GL11;

@SideOnly(Side.CLIENT)
public class ModelBiped extends ModelBase
{
    public ModelRenderer bipedHead;
    public ModelRenderer bipedHeadwear;
    public ModelRenderer bipedBody;
    public ModelRenderer bipedRightArm;
    public ModelRenderer bipedLeftArm;
    public ModelRenderer bipedRightLeg;
    public ModelRenderer bipedLeftLeg;
    public ModelRenderer bipedEars;
    public ModelRenderer bipedCloak;
    
    //pixelsky start
    public ModelRenderer bipedEars2;
    public ModelRenderer bipedBottomHat;
    public ModelRenderer bipedTopHat;
    public ModelRenderer bipedFrontBracelet;
    public ModelRenderer bipedLeftBracelet;
    public ModelRenderer bipedBackBracelet;
    public ModelRenderer bipedRightBracelet;
    public ModelRenderer SunglassesFront;
    public ModelRenderer SunglassesFront2;
    public ModelRenderer SunglassesBridge;
    public ModelRenderer RightSunglasses;
    public ModelRenderer LeftSunglasses;
    public ModelRenderer RightSunglassesBridge;
    public ModelRenderer LeftSunglassesBridge;
    public ModelRenderer NotchHatTop;
    public ModelRenderer NotchHatBottom;
    public ModelRenderer Tail;
    ModelRenderer LeftWingPart1;
    ModelRenderer LeftWingPart2;
    ModelRenderer LeftWingPart3;
    ModelRenderer LeftWingPart4;
    ModelRenderer LeftWingPart5;
    ModelRenderer LeftWingPart6;
    ModelRenderer LeftWingPart7;
    ModelRenderer LeftWingPart8;
    ModelRenderer LeftWingPart0;
    ModelRenderer RightWingPart0;
    ModelRenderer RightWingPart1;
    ModelRenderer RightWingPart2;
    ModelRenderer RightWingPart3;
    ModelRenderer RightWingPart4;
    ModelRenderer RightWingPart5;
    ModelRenderer RightWingPart6;
    ModelRenderer RightWingPart7;
    ModelRenderer RightWingPart8;
    //pixelsky end
    
    /**
     * Records whether the model should be rendered holding an item in the left hand, and if that item is a block.
     */
    public int heldItemLeft;

    /**
     * Records whether the model should be rendered holding an item in the right hand, and if that item is a block.
     */
    public int heldItemRight;
    public boolean isSneak;

    /** Records whether the model should be rendered aiming a bow. */
    public boolean aimedBow;

    public ModelBiped()
    {
        this(0.0F);
    }

    public ModelBiped(float par1)
    {
        this(par1, 0.0F, 64, 32);
    }

    public ModelBiped(float par1, float par2, int par3, int par4)
    {
        this.heldItemLeft = 0;
        this.heldItemRight = 0;
        this.isSneak = false;
        this.aimedBow = false;
        this.textureWidth = par3;
        this.textureHeight = par4;
        this.bipedCloak = new ModelRenderer(this, 0, 0);
        this.bipedCloak.addBox(-5.0F, 0.0F, -1.0F, 10, 16, 1, par1);
        this.bipedEars = new ModelRenderer(this, 24, 0);
        this.bipedEars.addBox(-3.0F, -6.0F, -1.0F, 6, 6, 1, par1);
        this.bipedHead = new ModelRenderer(this, 0, 0);
        this.bipedHead.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, par1);
        this.bipedHead.setRotationPoint(0.0F, 0.0F + par2, 0.0F);
        this.bipedHeadwear = new ModelRenderer(this, 32, 0);
        this.bipedHeadwear.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, par1 + 0.5F);
        this.bipedHeadwear.setRotationPoint(0.0F, 0.0F + par2, 0.0F);
        this.bipedBody = new ModelRenderer(this, 16, 16);
        this.bipedBody.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, par1);
        this.bipedBody.setRotationPoint(0.0F, 0.0F + par2, 0.0F);
        this.bipedRightArm = new ModelRenderer(this, 40, 16);
        this.bipedRightArm.addBox(-3.0F, -2.0F, -2.0F, 4, 12, 4, par1);
        this.bipedRightArm.setRotationPoint(-5.0F, 2.0F + par2, 0.0F);
        this.bipedLeftArm = new ModelRenderer(this, 40, 16);
        this.bipedLeftArm.mirror = true;
        this.bipedLeftArm.addBox(-1.0F, -2.0F, -2.0F, 4, 12, 4, par1);
        this.bipedLeftArm.setRotationPoint(5.0F, 2.0F + par2, 0.0F);
        this.bipedRightLeg = new ModelRenderer(this, 0, 16);
        this.bipedRightLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, par1);
        this.bipedRightLeg.setRotationPoint(-1.9F, 12.0F + par2, 0.0F);
        this.bipedLeftLeg = new ModelRenderer(this, 0, 16);
        this.bipedLeftLeg.mirror = true;
        this.bipedLeftLeg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, par1);
        this.bipedLeftLeg.setRotationPoint(1.9F, 12.0F + par2, 0.0F);
        //pixelsky start
        this.bipedBottomHat = new ModelRenderer(this, 0, 0);
        this.bipedBottomHat.addBox(-5.5F, -9.0F, -5.5F, 11, 2, 11);
        this.bipedTopHat = new ModelRenderer(this, 0, 13);
        this.bipedTopHat.addBox(-3.5F, -17.0F, -3.5F, 7, 8, 7);
        this.bipedFrontBracelet = new ModelRenderer(this, 0, 0);
        this.bipedFrontBracelet.addBox(4.0F, 9.0F, -3.0F, 4, 1, 1);
        this.bipedLeftBracelet = new ModelRenderer(this, 10, 0);
        this.bipedLeftBracelet.addBox(3.0F, 9.0F, -3.0F, 1, 1, 6);
        this.bipedBackBracelet = new ModelRenderer(this, 0, 2);
        this.bipedBackBracelet.addBox(4.0F, 9.0F, 2.0F, 4, 1, 1);
        this.bipedBackBracelet.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.bipedRightBracelet = new ModelRenderer(this, 0, 4);
        this.bipedRightBracelet.addBox(8.0F, 9.0F, -3.0F, 1, 1, 6);
        this.bipedRightBracelet.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.bipedEars2 = new ModelRenderer(this, 0, 0);
        this.bipedEars2.addBox(-3.0F, -6.0F, -1.0F, 6, 6, 1);
        this.SunglassesFront = new ModelRenderer(this, 0, 0);
        this.SunglassesFront.addBox(-3.0F, -4.0F, -5.0F, 2, 2, 1);
        this.SunglassesFront2 = new ModelRenderer(this, 6, 0);
        this.SunglassesFront2.addBox(1.0F, -4.0F, -5.0F, 2, 2, 1);
        this.SunglassesBridge = new ModelRenderer(this, 0, 4);
        this.SunglassesBridge.addBox(-1.0F, -4.0F, -5.0F, 2, 1, 1);
        this.RightSunglasses = new ModelRenderer(this, 0, 6);
        this.RightSunglasses.addBox(4.0F, -4.0F, -4.0F, 1, 1, 4);
        this.LeftSunglasses = new ModelRenderer(this, 12, 0);
        this.LeftSunglasses.addBox(-5.0F, -4.0F, -4.0F, 1, 1, 4);
        this.LeftSunglassesBridge = new ModelRenderer(this, 6, 5);
        this.LeftSunglassesBridge.addBox(-5.0F, -4.0F, -5.0F, 2, 1, 1);
        this.RightSunglassesBridge = new ModelRenderer(this, 6, 7);
        this.RightSunglassesBridge.addBox(3.0F, -4.0F, -5.0F, 2, 1, 1);
        this.Tail = new ModelRenderer(this, 0, 0);
        this.Tail.addBox(0.0F, 0.0F, 0.0F, 1, 10, 1);
        this.Tail.setRotationPoint(-0.5F, 0.0F, -1.0F);
        this.NotchHatTop = new ModelRenderer(this, 0, 0);
        this.NotchHatTop.addBox(-5.0F, -9.0F, -5.0F, 10, 1, 10);
        this.NotchHatBottom = new ModelRenderer(this, 0, 11);
        this.NotchHatBottom.addBox(-4.0F, -13.0F, -4.0F, 8, 4, 8);
        this.LeftWingPart1 = new ModelRenderer(this, 56, 0);
        this.LeftWingPart1.addBox(-1.0F, 1.0F, 3.0F, 1, 10, 1);
        this.LeftWingPart1.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.LeftWingPart1.setTextureSize(64, 32);
        this.LeftWingPart1.mirror = true;
        this.setRotation(this.LeftWingPart1, 0.0F, 0.5007752F, 0.0174533F);
        this.LeftWingPart2 = new ModelRenderer(this, 50, 0);
        this.LeftWingPart2.addBox(-1.0F, 0.0F, 4.0F, 1, 10, 2);
        this.LeftWingPart2.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.LeftWingPart2.setTextureSize(64, 32);
        this.LeftWingPart2.mirror = true;
        this.setRotation(this.LeftWingPart2, 0.0F, 0.5182285F, 0.0349066F);
        this.LeftWingPart3 = new ModelRenderer(this, 46, 0);
        this.LeftWingPart3.addBox(-1.0F, -1.0F, 6.0F, 1, 10, 1);
        this.LeftWingPart3.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.LeftWingPart3.setTextureSize(64, 32);
        this.LeftWingPart3.mirror = true;
        this.setRotation(this.LeftWingPart3, 0.0F, 0.5356818F, 0.0523599F);
        this.LeftWingPart4 = new ModelRenderer(this, 38, 0);
        this.LeftWingPart4.addBox(-1.0F, -2.0F, 7.0F, 1, 10, 3);
        this.LeftWingPart4.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.LeftWingPart4.setTextureSize(64, 32);
        this.LeftWingPart4.mirror = true;
        this.setRotation(this.LeftWingPart4, 0.0F, 0.5531351F, 0.0698132F);
        this.LeftWingPart5 = new ModelRenderer(this, 34, 0);
        this.LeftWingPart5.addBox(-1.0F, -1.0F, 10.0F, 1, 10, 1);
        this.LeftWingPart5.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.LeftWingPart5.setTextureSize(64, 32);
        this.LeftWingPart5.mirror = true;
        this.setRotation(this.LeftWingPart5, 0.0F, 0.5531351F, 0.0523599F);
        this.LeftWingPart6 = new ModelRenderer(this, 30, 0);
        this.LeftWingPart6.addBox(-1.0F, 0.0F, 11.0F, 1, 10, 1);
        this.LeftWingPart6.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.LeftWingPart6.setTextureSize(64, 32);
        this.LeftWingPart6.mirror = true;
        this.setRotation(this.LeftWingPart6, 0.0F, 0.5705884F, 0.0349066F);
        this.LeftWingPart7 = new ModelRenderer(this, 26, 0);
        this.LeftWingPart7.addBox(-1.0F, 1.0F, 12.0F, 1, 10, 1);
        this.LeftWingPart7.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.LeftWingPart7.setTextureSize(64, 32);
        this.LeftWingPart7.mirror = true;
        this.setRotation(this.LeftWingPart7, 0.0F, 0.5880417F, 0.0174533F);
        this.LeftWingPart8 = new ModelRenderer(this, 22, 0);
        this.LeftWingPart8.addBox(-1.0F, 3.0F, 13.0F, 1, 10, 1);
        this.LeftWingPart8.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.LeftWingPart8.setTextureSize(64, 32);
        this.LeftWingPart8.mirror = true;
        this.setRotation(this.LeftWingPart8, 0.0F, 0.5880417F, 0.0F);
        this.LeftWingPart0 = new ModelRenderer(this, 60, 0);
        this.LeftWingPart0.addBox(-1.0F, 2.0F, 2.0F, 1, 10, 1);
        this.LeftWingPart0.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.LeftWingPart0.setTextureSize(64, 32);
        this.LeftWingPart0.mirror = true;
        this.setRotation(this.LeftWingPart0, 0.0F, 0.4833219F, 0.0F);
        this.RightWingPart0 = new ModelRenderer(this, 60, 21);
        this.RightWingPart0.addBox(0.0F, 2.0F, 2.0F, 1, 10, 1);
        this.RightWingPart0.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.RightWingPart0.setTextureSize(64, 32);
        this.RightWingPart0.mirror = true;
        this.setRotation(this.RightWingPart0, 0.0F, -0.4833166F, 0.0F);
        this.RightWingPart1 = new ModelRenderer(this, 56, 21);
        this.RightWingPart1.addBox(0.0F, 1.0F, 3.0F, 1, 10, 1);
        this.RightWingPart1.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.RightWingPart1.setTextureSize(64, 32);
        this.RightWingPart1.mirror = true;
        this.setRotation(this.RightWingPart1, 0.0F, -0.5007699F, -0.0174533F);
        this.RightWingPart2 = new ModelRenderer(this, 50, 20);
        this.RightWingPart2.addBox(0.0F, 0.0F, 4.0F, 1, 10, 2);
        this.RightWingPart2.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.RightWingPart2.setTextureSize(64, 32);
        this.RightWingPart2.mirror = true;
        this.setRotation(this.RightWingPart2, 0.0F, -0.5182232F, -0.0349066F);
        this.RightWingPart3 = new ModelRenderer(this, 46, 21);
        this.RightWingPart3.addBox(0.0F, -1.0F, 6.0F, 1, 10, 1);
        this.RightWingPart3.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.RightWingPart3.setTextureSize(64, 32);
        this.RightWingPart3.mirror = true;
        this.setRotation(this.RightWingPart3, 0.0174533F, -0.5356765F, -0.0523599F);
        this.RightWingPart4 = new ModelRenderer(this, 38, 19);
        this.RightWingPart4.addBox(0.0F, -2.0F, 7.0F, 1, 10, 3);
        this.RightWingPart4.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.RightWingPart4.setTextureSize(64, 32);
        this.RightWingPart4.mirror = true;
        this.setRotation(this.RightWingPart4, 0.0174533F, -0.5531297F, -0.0698132F);
        this.RightWingPart5 = new ModelRenderer(this, 34, 21);
        this.RightWingPart5.addBox(0.0F, -1.0F, 10.0F, 1, 10, 1);
        this.RightWingPart5.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.RightWingPart5.setTextureSize(64, 32);
        this.RightWingPart5.mirror = true;
        this.setRotation(this.RightWingPart5, 0.0174533F, -0.570583F, -0.0523599F);
        this.RightWingPart6 = new ModelRenderer(this, 30, 21);
        this.RightWingPart6.addBox(0.0F, 0.0F, 11.0F, 1, 10, 1);
        this.RightWingPart6.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.RightWingPart6.setTextureSize(64, 32);
        this.RightWingPart6.mirror = true;
        this.setRotation(this.RightWingPart6, 0.0174533F, -0.5880363F, -0.0349066F);
        this.RightWingPart7 = new ModelRenderer(this, 26, 21);
        this.RightWingPart7.addBox(0.0F, 1.0F, 12.0F, 1, 10, 1);
        this.RightWingPart7.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.RightWingPart7.setTextureSize(64, 32);
        this.RightWingPart7.mirror = true;
        this.setRotation(this.RightWingPart7, 0.0174533F, -0.6054896F, -0.0174533F);
        this.RightWingPart8 = new ModelRenderer(this, 22, 21);
        this.RightWingPart8.addBox(0.0F, 3.0F, 13.0F, 1, 10, 1);
        this.RightWingPart8.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.RightWingPart8.setTextureSize(64, 32);
        this.RightWingPart8.mirror = true;
        this.setRotation(this.RightWingPart8, 0.0174533F, -0.6229429F, 0.0F);
        
        //pixelsky end
    }
    /*
     * Pixelsky start
     */
    private void setRotation(ModelRenderer var1, float var2, float var3, float var4)
    {
        var1.rotateAngleX = var2;
        var1.rotateAngleY = var3;
        var1.rotateAngleZ = var4;
    }
    
    /**
     * Sets the models various rotation angles then renders the model.
     */
    public void render(Entity par1Entity, float par2, float par3, float par4, float par5, float par6, float par7)
    {
        this.setRotationAngles(par2, par3, par4, par5, par6, par7, par1Entity);

        if (this.isChild)
        {
            float f6 = 2.0F;
            GL11.glPushMatrix();
            GL11.glScalef(1.5F / f6, 1.5F / f6, 1.5F / f6);
            GL11.glTranslatef(0.0F, 16.0F * par7, 0.0F);
            this.bipedHead.render(par7);
            GL11.glPopMatrix();
            GL11.glPushMatrix();
            GL11.glScalef(1.0F / f6, 1.0F / f6, 1.0F / f6);
            GL11.glTranslatef(0.0F, 24.0F * par7, 0.0F);
            this.bipedBody.render(par7);
            this.bipedRightArm.render(par7);
            this.bipedLeftArm.render(par7);
            this.bipedRightLeg.render(par7);
            this.bipedLeftLeg.render(par7);
            this.bipedHeadwear.render(par7);
            GL11.glPopMatrix();
        }
        else
        {
            this.bipedHead.render(par7);
            this.bipedBody.render(par7);
            this.bipedRightArm.render(par7);
            this.bipedLeftArm.render(par7);
            this.bipedRightLeg.render(par7);
            this.bipedLeftLeg.render(par7);
            this.bipedHeadwear.render(par7);
        }
    }

    /**
     * Sets the model's various rotation angles. For bipeds, par1 and par2 are used for animating the movement of arms
     * and legs, where par1 represents the time(so that arms and legs swing back and forth) and par2 represents how
     * "far" arms and legs can swing at most.
     */
    public void setRotationAngles(float par1, float par2, float par3, float par4, float par5, float par6, Entity par7Entity)
    {
        this.bipedHead.rotateAngleY = par4 / (180F / (float)Math.PI);
        this.bipedHead.rotateAngleX = par5 / (180F / (float)Math.PI);
        this.bipedHeadwear.rotateAngleY = this.bipedHead.rotateAngleY;
        this.bipedHeadwear.rotateAngleX = this.bipedHead.rotateAngleX;
        this.bipedRightArm.rotateAngleX = MathHelper.cos(par1 * 0.6662F + (float)Math.PI) * 2.0F * par2 * 0.5F;
        this.bipedLeftArm.rotateAngleX = MathHelper.cos(par1 * 0.6662F) * 2.0F * par2 * 0.5F;
        this.bipedRightArm.rotateAngleZ = 0.0F;
        this.bipedLeftArm.rotateAngleZ = 0.0F;
        this.bipedRightLeg.rotateAngleX = MathHelper.cos(par1 * 0.6662F) * 1.4F * par2;
        this.bipedLeftLeg.rotateAngleX = MathHelper.cos(par1 * 0.6662F + (float)Math.PI) * 1.4F * par2;
        this.bipedRightLeg.rotateAngleY = 0.0F;
        this.bipedLeftLeg.rotateAngleY = 0.0F;

        if (this.isRiding)
        {
            this.bipedRightArm.rotateAngleX += -((float)Math.PI / 5F);
            this.bipedLeftArm.rotateAngleX += -((float)Math.PI / 5F);
            this.bipedRightLeg.rotateAngleX = -((float)Math.PI * 2F / 5F);
            this.bipedLeftLeg.rotateAngleX = -((float)Math.PI * 2F / 5F);
            this.bipedRightLeg.rotateAngleY = ((float)Math.PI / 10F);
            this.bipedLeftLeg.rotateAngleY = -((float)Math.PI / 10F);
        }

        if (this.heldItemLeft != 0)
        {
            this.bipedLeftArm.rotateAngleX = this.bipedLeftArm.rotateAngleX * 0.5F - ((float)Math.PI / 10F) * (float)this.heldItemLeft;
        }

        if (this.heldItemRight != 0)
        {
            this.bipedRightArm.rotateAngleX = this.bipedRightArm.rotateAngleX * 0.5F - ((float)Math.PI / 10F) * (float)this.heldItemRight;
        }

        this.bipedRightArm.rotateAngleY = 0.0F;
        this.bipedLeftArm.rotateAngleY = 0.0F;
        float f6;
        float f7;

        if (this.onGround > -9990.0F)
        {
            f6 = this.onGround;
            this.bipedBody.rotateAngleY = MathHelper.sin(MathHelper.sqrt_float(f6) * (float)Math.PI * 2.0F) * 0.2F;
            this.bipedRightArm.rotationPointZ = MathHelper.sin(this.bipedBody.rotateAngleY) * 5.0F;
            this.bipedRightArm.rotationPointX = -MathHelper.cos(this.bipedBody.rotateAngleY) * 5.0F;
            this.bipedLeftArm.rotationPointZ = -MathHelper.sin(this.bipedBody.rotateAngleY) * 5.0F;
            this.bipedLeftArm.rotationPointX = MathHelper.cos(this.bipedBody.rotateAngleY) * 5.0F;
            this.bipedRightArm.rotateAngleY += this.bipedBody.rotateAngleY;
            this.bipedLeftArm.rotateAngleY += this.bipedBody.rotateAngleY;
            this.bipedLeftArm.rotateAngleX += this.bipedBody.rotateAngleY;
            f6 = 1.0F - this.onGround;
            f6 *= f6;
            f6 *= f6;
            f6 = 1.0F - f6;
            f7 = MathHelper.sin(f6 * (float)Math.PI);
            float f8 = MathHelper.sin(this.onGround * (float)Math.PI) * -(this.bipedHead.rotateAngleX - 0.7F) * 0.75F;
            this.bipedRightArm.rotateAngleX = (float)((double)this.bipedRightArm.rotateAngleX - ((double)f7 * 1.2D + (double)f8));
            this.bipedRightArm.rotateAngleY += this.bipedBody.rotateAngleY * 2.0F;
            this.bipedRightArm.rotateAngleZ = MathHelper.sin(this.onGround * (float)Math.PI) * -0.4F;
        }

        if (this.isSneak)
        {
            this.bipedBody.rotateAngleX = 0.5F;
            this.bipedRightArm.rotateAngleX += 0.4F;
            this.bipedLeftArm.rotateAngleX += 0.4F;
            this.bipedRightLeg.rotationPointZ = 4.0F;
            this.bipedLeftLeg.rotationPointZ = 4.0F;
            this.bipedRightLeg.rotationPointY = 9.0F;
            this.bipedLeftLeg.rotationPointY = 9.0F;
            this.bipedHead.rotationPointY = 1.0F;
            this.bipedHeadwear.rotationPointY = 1.0F;
        }
        else
        {
            this.bipedBody.rotateAngleX = 0.0F;
            this.bipedRightLeg.rotationPointZ = 0.1F;
            this.bipedLeftLeg.rotationPointZ = 0.1F;
            this.bipedRightLeg.rotationPointY = 12.0F;
            this.bipedLeftLeg.rotationPointY = 12.0F;
            this.bipedHead.rotationPointY = 0.0F;
            this.bipedHeadwear.rotationPointY = 0.0F;
        }

        this.bipedRightArm.rotateAngleZ += MathHelper.cos(par3 * 0.09F) * 0.05F + 0.05F;
        this.bipedLeftArm.rotateAngleZ -= MathHelper.cos(par3 * 0.09F) * 0.05F + 0.05F;
        this.bipedRightArm.rotateAngleX += MathHelper.sin(par3 * 0.067F) * 0.05F;
        this.bipedLeftArm.rotateAngleX -= MathHelper.sin(par3 * 0.067F) * 0.05F;

        if (this.aimedBow)
        {
            f6 = 0.0F;
            f7 = 0.0F;
            this.bipedRightArm.rotateAngleZ = 0.0F;
            this.bipedLeftArm.rotateAngleZ = 0.0F;
            this.bipedRightArm.rotateAngleY = -(0.1F - f6 * 0.6F) + this.bipedHead.rotateAngleY;
            this.bipedLeftArm.rotateAngleY = 0.1F - f6 * 0.6F + this.bipedHead.rotateAngleY + 0.4F;
            this.bipedRightArm.rotateAngleX = -((float)Math.PI / 2F) + this.bipedHead.rotateAngleX;
            this.bipedLeftArm.rotateAngleX = -((float)Math.PI / 2F) + this.bipedHead.rotateAngleX;
            this.bipedRightArm.rotateAngleX -= f6 * 1.2F - f7 * 0.4F;
            this.bipedLeftArm.rotateAngleX -= f6 * 1.2F - f7 * 0.4F;
            this.bipedRightArm.rotateAngleZ += MathHelper.cos(par3 * 0.09F) * 0.05F + 0.05F;
            this.bipedLeftArm.rotateAngleZ -= MathHelper.cos(par3 * 0.09F) * 0.05F + 0.05F;
            this.bipedRightArm.rotateAngleX += MathHelper.sin(par3 * 0.067F) * 0.05F;
            this.bipedLeftArm.rotateAngleX -= MathHelper.sin(par3 * 0.067F) * 0.05F;
        }
    }

    /**
     * renders the ears (specifically, deadmau5's)
     */
    public void renderEars(float par1)
    {
        this.bipedEars.rotateAngleY = this.bipedHead.rotateAngleY;
        this.bipedEars.rotateAngleX = this.bipedHead.rotateAngleX;
        this.bipedEars.rotationPointX = 0.0F;
        this.bipedEars.rotationPointY = 0.0F;
        this.bipedEars.render(par1);
    }

    /**
     * Renders the cloak of the current biped (in most cases, it's a player)
     */
    public void renderCloak(float par1)
    {
        this.bipedCloak.render(par1);
    }
    
    //pixelsky start
    public void renderEars2(float var1)
    {
        this.bipedEars2.rotateAngleY = this.bipedHead.rotateAngleY;
        this.bipedEars2.rotateAngleX = this.bipedHead.rotateAngleX;
        this.bipedEars2.rotationPointX = 0.0F;
        this.bipedEars2.rotationPointY = 0.0F;
        this.bipedEars2.render(var1);
    }

    public void renderTopHat(float var1)
    {
        this.bipedBottomHat.rotateAngleY = this.bipedHead.rotateAngleY;
        this.bipedBottomHat.rotateAngleX = this.bipedHead.rotateAngleX;
        this.bipedBottomHat.rotationPointX = 0.0F;
        this.bipedBottomHat.rotationPointY = 0.0F;
        this.bipedBottomHat.render(var1);
        this.bipedTopHat.rotateAngleY = this.bipedHead.rotateAngleY;
        this.bipedTopHat.rotateAngleX = this.bipedHead.rotateAngleX;
        this.bipedTopHat.rotationPointX = 0.0F;
        this.bipedTopHat.rotationPointY = 0.0F;
        this.bipedTopHat.render(var1);
    }

    public void renderNotchHat(float var1)
    {
        this.NotchHatTop.rotateAngleY = this.bipedHead.rotateAngleY;
        this.NotchHatTop.rotateAngleX = this.bipedHead.rotateAngleX;
        this.NotchHatTop.rotationPointX = 0.0F;
        this.NotchHatTop.rotationPointY = 0.0F;
        this.NotchHatTop.render(var1);
        this.NotchHatBottom.rotateAngleY = this.bipedHead.rotateAngleY;
        this.NotchHatBottom.rotateAngleX = this.bipedHead.rotateAngleX;
        this.NotchHatBottom.rotationPointX = 0.0F;
        this.NotchHatBottom.rotationPointY = 0.0F;
        this.NotchHatBottom.render(var1);
    }

    public void renderBracelet(float var1)
    {
        this.bipedFrontBracelet.rotateAngleY = this.bipedLeftArm.rotateAngleY;
        this.bipedFrontBracelet.rotateAngleX = this.bipedLeftArm.rotateAngleX;
        this.bipedFrontBracelet.rotationPointX = 0.0F;
        this.bipedFrontBracelet.rotationPointY = 0.0F;
        this.bipedFrontBracelet.render(var1);
        this.bipedLeftBracelet.rotateAngleY = this.bipedLeftArm.rotateAngleY;
        this.bipedLeftBracelet.rotateAngleX = this.bipedLeftArm.rotateAngleX;
        this.bipedLeftBracelet.rotationPointX = 0.0F;
        this.bipedLeftBracelet.rotationPointY = 0.0F;
        this.bipedLeftBracelet.render(var1);
        this.bipedBackBracelet.rotateAngleY = this.bipedLeftArm.rotateAngleY;
        this.bipedBackBracelet.rotateAngleX = this.bipedLeftArm.rotateAngleX;
        this.bipedBackBracelet.rotationPointX = 0.0F;
        this.bipedBackBracelet.rotationPointY = 0.0F;
        this.bipedBackBracelet.render(var1);
        this.bipedRightBracelet.rotateAngleY = this.bipedLeftArm.rotateAngleY;
        this.bipedRightBracelet.rotateAngleX = this.bipedLeftArm.rotateAngleX;
        this.bipedRightBracelet.rotationPointX = 0.0F;
        this.bipedRightBracelet.rotationPointY = 0.0F;
        this.bipedRightBracelet.render(var1);
    }

    public void renderSunglasses(float var1)
    {
        this.SunglassesFront.rotateAngleY = this.bipedHead.rotateAngleY;
        this.SunglassesFront.rotateAngleX = this.bipedHead.rotateAngleX;
        this.SunglassesFront.render(var1);
        this.SunglassesFront2.rotateAngleY = this.bipedHead.rotateAngleY;
        this.SunglassesFront2.rotateAngleX = this.bipedHead.rotateAngleX;
        this.SunglassesFront2.render(var1);
        this.SunglassesBridge.rotateAngleY = this.bipedHead.rotateAngleY;
        this.SunglassesBridge.rotateAngleX = this.bipedHead.rotateAngleX;
        this.SunglassesBridge.render(var1);
        this.RightSunglasses.rotateAngleY = this.bipedHead.rotateAngleY;
        this.RightSunglasses.rotateAngleX = this.bipedHead.rotateAngleX;
        this.RightSunglasses.render(var1);
        this.LeftSunglasses.rotateAngleY = this.bipedHead.rotateAngleY;
        this.LeftSunglasses.rotateAngleX = this.bipedHead.rotateAngleX;
        this.LeftSunglasses.render(var1);
        this.LeftSunglassesBridge.rotateAngleY = this.bipedHead.rotateAngleY;
        this.LeftSunglassesBridge.rotateAngleX = this.bipedHead.rotateAngleX;
        this.LeftSunglassesBridge.render(var1);
        this.RightSunglassesBridge.rotateAngleY = this.bipedHead.rotateAngleY;
        this.RightSunglassesBridge.rotateAngleX = this.bipedHead.rotateAngleX;
        this.RightSunglassesBridge.render(var1);
    }

    public void renderTail(float var1)
    {
        this.Tail.render(var1);
    }

    public void renderWings(float var1)
    {
        this.LeftWingPart1.render(var1);
        this.LeftWingPart2.render(var1);
        this.LeftWingPart3.render(var1);
        this.LeftWingPart4.render(var1);
        this.LeftWingPart5.render(var1);
        this.LeftWingPart6.render(var1);
        this.LeftWingPart7.render(var1);
        this.LeftWingPart8.render(var1);
        this.LeftWingPart0.render(var1);
        this.RightWingPart0.render(var1);
        this.RightWingPart1.render(var1);
        this.RightWingPart2.render(var1);
        this.RightWingPart3.render(var1);
        this.RightWingPart4.render(var1);
        this.RightWingPart5.render(var1);
        this.RightWingPart6.render(var1);
        this.RightWingPart7.render(var1);
        this.RightWingPart8.render(var1);
    }
    //pixelsky end
}
