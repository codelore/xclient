import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import net.minecraft.client.Minecraft;

public class Start
{
    public static void main(String[] args)
    {
        try
        {
            Field f = Minecraft.class.getDeclaredField("minecraftDir");
            Field.setAccessible(new Field[] { f }, true);
            f.set(null, new File("."));
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return;
        }

        Minecraft.main(args);
    }

    private static String openUrl(String addr)
    {
        try
        {
            URL url = new URL(addr);
            java.io.InputStream is;
            is = url.openConnection().getInputStream();
            java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.InputStreamReader(is));
            String buf = "";
            String line = null;

            while ((line = reader.readLine()) != null)
            {
                buf += "\n" + line;
            }

            reader.close();
            return buf;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }
}