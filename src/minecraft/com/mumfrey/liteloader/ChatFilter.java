package com.mumfrey.liteloader;

import net.minecraft.network.packet.Packet3Chat;

public abstract interface ChatFilter extends LiteMod
{
  public abstract boolean onChat(Packet3Chat paramPacket3Chat);
}