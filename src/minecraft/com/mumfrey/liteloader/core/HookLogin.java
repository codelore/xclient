 package com.mumfrey.liteloader.core;
 
 import net.minecraft.network.packet.NetHandler;
 import net.minecraft.network.packet.Packet1Login;
 import net.minecraft.world.EnumGameType;
 import net.minecraft.world.WorldType;
 
 public class HookLogin extends Packet1Login
 {
   public static LiteLoader loader;
 
   public HookLogin()
   {
   }
 
   public HookLogin(int par1, WorldType par2WorldType, EnumGameType par3EnumGameType, boolean par4, int par5, int par6, int par7, int par8)
   {
     super(par1, par2WorldType, par3EnumGameType, par4, par5, par6, par7, par8);
   }
 
   public void processPacket(NetHandler par1NetHandler)
   {
     if ((loader == null) || (loader.onPreLogin(par1NetHandler, this)))
     {
       super.processPacket(par1NetHandler);
 
       if (loader != null) loader.onConnectToServer(par1NetHandler, this);
     }
   }
 }