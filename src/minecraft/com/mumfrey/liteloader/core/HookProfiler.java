 package com.mumfrey.liteloader.core;
 
 import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.profiler.Profiler;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.logging.Logger;
 
 public class HookProfiler extends Profiler
 {
   private Logger logger;
   private LiteLoader loader;
   private LinkedList sectionStack = new LinkedList();
 
   private boolean initDone = false;
   private boolean tick;
   private Field ofProfiler;
   private Minecraft mc;
 
   public HookProfiler(LiteLoader core, Logger logger)
   {
     this.mc = Minecraft.getMinecraft();
 
     this.loader = core;
     this.logger = logger;
 
     detectOptifine();
   }
 
   private void detectOptifine()
   {
     try
     {
       this.ofProfiler = GameSettings.class.getDeclaredField("ofProfiler");
     }
     catch (SecurityException ex) {
     }
     catch (NoSuchFieldException ex) {
       this.logger.info("Optifine not detected");
     }
     finally
     {
       if (this.ofProfiler != null)
       {
         this.logger.info(String.format("Optifine version %s detected, enabling compatibility check", new Object[] { getOptifineVersion() }));
       }
     }
   }
 
   private String getOptifineVersion()
   {
     try
     {
       Class config = Class.forName("Config");
 
       if (config != null)
       {
         Method getVersion = config.getDeclaredMethod("getVersion", new Class[0]);
 
         if (getVersion != null)
         {
           return (String)getVersion.invoke(null, new Object[0]);
         }
       }
     }
     catch (Exception ex) {
     }
     return "Unknown";
   }
 
   public void startSection(String sectionName)
   {
     if (!this.initDone)
     {
       this.initDone = true;
       this.loader.onInit();
     }
 
     if (("gameRenderer".equals(sectionName)) && ("root".equals(this.sectionStack.getLast())))
     {
       this.loader.onRender();
     }
 
     if (("frustrum".equals(sectionName)) && ("level".equals(this.sectionStack.getLast())))
     {
       this.loader.onSetupCameraTransform();
     }
 
     if ("litParticles".equals(sectionName))
     {
       this.loader.postRenderEntities();
     }
 
     if (("tick".equals(sectionName)) && ("root".equals(this.sectionStack.getLast())))
     {
       this.loader.onTimerUpdate();
     }
 
     if ("chat".equals(sectionName))
     {
       this.loader.onBeforeChatRender();
     }
 
     if ("animateTick".equals(sectionName)) this.tick = true;
     this.sectionStack.add(sectionName);
     super.startSection(sectionName);
 
     if (this.ofProfiler != null)
     {
       try
       {
         this.ofProfiler.set(this.mc.gameSettings, Boolean.valueOf(true));
       }
       catch (IllegalArgumentException ex)
       {
         this.ofProfiler = null;
       }
       catch (IllegalAccessException ex)
       {
         this.ofProfiler = null;
       }
     }
   }
 
   public void endSection()
   {
     super.endSection();
     try
     {
       String endingSection = this.sectionStack.size() > 0 ? (String)this.sectionStack.removeLast() : null;
       String nextSection = this.sectionStack.size() > 0 ? (String)this.sectionStack.getLast() : null;
 
       if (("gameRenderer".equals(endingSection)) && ("root".equals(this.sectionStack.getLast())))
       {
         super.startSection("litetick");
 
         this.loader.onTick(this, this.tick);
         this.tick = false;
 
         super.endSection();
       }
       else if ((("mouse".equals(endingSection)) && ("gameRenderer".equals(nextSection)) && ((this.mc.skipRenderWorld) || (this.mc.theWorld == null))) || (("gui".equals(endingSection)) && ("gameRenderer".equals(nextSection)) && (this.mc.theWorld != null)))
       {
         this.loader.onBeforeGuiRender();
       }
       else if (("hand".equals(endingSection)) && ("level".equals(this.sectionStack.getLast())))
       {
         this.loader.postRender();
       }
       else if ("chat".equals(endingSection))
       {
         this.loader.onAfterChatRender();
       }
     }
     catch (NoSuchElementException ex)
     {
       this.logger.severe("Corrupted Profiler stack detected, this indicates an error with one of your mods.");
       throw new ProfilerStackCorruptionException("Corrupted Profiler stack detected", ex);
     }
   }
 }