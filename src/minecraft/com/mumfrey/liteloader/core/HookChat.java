 package com.mumfrey.liteloader.core;

 import com.mumfrey.liteloader.util.PrivateFields;
 import net.minecraft.network.packet.NetHandler;
 import net.minecraft.network.packet.Packet;
 import net.minecraft.network.packet.Packet3Chat;
 import net.minecraft.util.IntHashMap;

 import java.io.DataInputStream;
 import java.io.DataOutputStream;
 import java.io.IOException;
 import java.util.Map;
 
 public class HookChat extends Packet3Chat
 {
   private static boolean registered = false;
   private static LiteLoader packetHandler;
   private static Class proxyClass;
   private Packet proxyPacket;
 
   public HookChat()
   {
     try
     {
       if (proxyClass != null)
       {
         this.proxyPacket = ((Packet)proxyClass.newInstance());
       }
     }
     catch (Exception ex)
     {
     }
   }
 
   public HookChat(String message)
   {
     super(message);
     try
     {
       if (proxyClass != null)
       {
         this.proxyPacket = ((Packet)proxyClass.newInstance());
 
         if ((this.proxyPacket instanceof Packet3Chat))
         {
           ((Packet3Chat)this.proxyPacket).message = this.message;
         }
       }
     }
     catch (Exception ex)
     {
     }
   }
 
   public void readPacketData(DataInputStream datainputstream) throws IOException {
     if (this.proxyPacket != null)
     {
       this.proxyPacket.readPacketData(datainputstream);
       this.message = ((Packet3Chat)this.proxyPacket).message;
     }
     else {
       super.readPacketData(datainputstream);
     }
   }
 
   public void writePacketData(DataOutputStream dataoutputstream) throws IOException
   {
     if (this.proxyPacket != null)
       this.proxyPacket.writePacketData(dataoutputstream);
     else
       super.writePacketData(dataoutputstream);
   }
 
   public void processPacket(NetHandler nethandler)
   {
     if ((packetHandler == null) || (packetHandler.onChat(this)))
     {
       if (this.proxyPacket != null)
         this.proxyPacket.processPacket(nethandler);
       else
         super.processPacket(nethandler);
     }
   }
 
   public int getPacketSize()
   {
     if (this.proxyPacket != null) {
       return this.proxyPacket.getPacketSize();
     }
     return super.getPacketSize();
   }
 
   public static void registerPacketHandler(LiteLoader handler)
   {
     packetHandler = handler;
   }
 
   public static void register()
   {
     register(false);
   }
 
   public static void register(boolean force)
   {
     if ((!registered) || (force))
     {
       try
       {
         IntHashMap packetIdToClassMap = Packet.packetIdToClassMap;
         proxyClass = (Class)packetIdToClassMap.lookup(3);
 
         if (proxyClass.equals(Packet3Chat.class))
         {
           proxyClass = null;
         }
 
         packetIdToClassMap.removeObject(3);
         packetIdToClassMap.addKey(3, HookChat.class);
 
         Map packetClassToIdMap = (Map) PrivateFields.StaticFields.packetClassToIdMap.get();
         packetClassToIdMap.put(HookChat.class, Integer.valueOf(3));
 
         registered = true;
       }
       catch (Exception ex)
       {
         ex.printStackTrace();
       }
     }
   }
 }