 package com.mumfrey.liteloader.core;
 
 import net.minecraft.crash.CrashReport;

import java.util.concurrent.Callable;
 
 public class CallableLiteLoaderBrand
   implements Callable
 {
   final CrashReport crashReport;
 
   public CallableLiteLoaderBrand(CrashReport report)
   {
     this.crashReport = report;
   }
 
   public String call()
     throws Exception
   {
     String brand = LiteLoader.getInstance().getBranding();
     return brand == null ? "Unknown / None" : brand;
   }
 }