 package com.mumfrey.liteloader.core;
 
 import net.minecraft.crash.CrashReport;

import java.util.concurrent.Callable;
 
 public class CallableLiteLoaderMods
   implements Callable
 {
   final CrashReport crashReport;
 
   public CallableLiteLoaderMods(CrashReport report)
   {
     this.crashReport = report;
   }
 
   public String call()
     throws Exception
   {
     return LiteLoader.getInstance().getLoadedModsList();
   }
 }