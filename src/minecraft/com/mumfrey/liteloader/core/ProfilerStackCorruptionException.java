 package com.mumfrey.liteloader.core;
 
 public class ProfilerStackCorruptionException extends RuntimeException
 {
   private static final long serialVersionUID = -7745831270297368169L;
 
   public ProfilerStackCorruptionException(String message)
   {
     super(message);
   }

     public ProfilerStackCorruptionException(String message, Throwable cause) {
         super(message, cause);
     }
 }