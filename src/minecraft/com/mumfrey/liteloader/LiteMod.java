package com.mumfrey.liteloader;

public abstract interface LiteMod
{
  public abstract String getName();

  public abstract String getVersion();

  public abstract void init();
}