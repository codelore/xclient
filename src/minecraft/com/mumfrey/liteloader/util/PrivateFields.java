 package com.mumfrey.liteloader.util;
 
 import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiControls;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.network.packet.Packet;
import net.minecraft.profiler.PlayerUsageSnooper;
import net.minecraft.tileentity.TileEntity;

import java.lang.reflect.Field;
 
 public class PrivateFields
 {
   public final Class parentClass;
   public final String mcpName;
   public final String name;
   public final String seargeName;
   private final String fieldName;
   public static final PrivateFields minecraftTimer = new PrivateFields(Minecraft.class, "timer", "V", "field_71428_T");
   public static final PrivateFields minecraftProfiler = new PrivateFields(Minecraft.class, "mcProfiler", "J", "field_71424_I");
   public static final PrivateFields entityRenderMap = new PrivateFields(RenderManager.class, "entityRenderMap", "q", "field_78729_o");
   public static final PrivateFields guiControlsParentScreen = new PrivateFields(GuiControls.class, "parentScreen", "b", "field_73909_b");
   public static final PrivateFields playerStatsCollector = new PrivateFields(PlayerUsageSnooper.class, "playerStatsCollector", "d", "field_76478_d");
 
   private PrivateFields(Class owner, String mcpName, String name, String seargeName)
   {
     this.parentClass = owner;
     this.mcpName = mcpName;
     this.name = name;
     this.seargeName = seargeName;
 
     this.fieldName = ModUtilities.getObfuscatedFieldName(mcpName, name, seargeName);
   }
 
   public Object get(Object instance)
   {
     try
     {
       Field field = this.parentClass.getDeclaredField(this.fieldName);
       field.setAccessible(true);
       return field.get(instance);
     }
     catch (Exception ex) {
     }
     return null;
   }
 
   public Object set(Object instance, Object value)
   {
     try
     {
       Field field = this.parentClass.getDeclaredField(this.fieldName);
       field.setAccessible(true);
       field.set(instance, value);
     }
     catch (Exception ex) {
     }
     return value;
   }
 
   public Object setFinal(Object instance, Object value)
   {
     try
     {
       Field modifiers = Field.class.getDeclaredField("modifiers");
       modifiers.setAccessible(true);
 
       Field field = this.parentClass.getDeclaredField(this.fieldName);
       modifiers.setInt(field, field.getModifiers() & 0xFFFFFFEF);
       field.setAccessible(true);
       field.set(instance, value);
     }
     catch (Exception ex) {
     }
     return value;
   }
 
   public static final class StaticFields extends PrivateFields
   {
     public static final StaticFields packetClassToIdMap = new StaticFields(Packet.class, "packetClassToIdMap", "a", "field_73291_a");
     public static final StaticFields tileEntityNameToClassMap = new StaticFields(TileEntity.class, "nameToClassMap", "a", "field_70326_a");
 
     public StaticFields(Class owner, String mcpName, String name, String fmlName)
     {
       super(owner, mcpName, name, fmlName); }
     public Object get() { return get(null); } 
     public void set(Object value) { set(null, value);
     }
   }
 }