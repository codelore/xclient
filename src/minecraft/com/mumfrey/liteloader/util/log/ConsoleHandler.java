 package com.mumfrey.liteloader.util.log;
 
 import java.util.logging.LogRecord;
 import java.util.logging.StreamHandler;
 
 public class ConsoleHandler extends StreamHandler
 {
   public ConsoleHandler()
   {
     setOutputStream(System.out);
   }
 
   public synchronized void publish(LogRecord record)
   {
     super.publish(record);
     flush();
   }
 
   public synchronized void close()
   {
     flush();
   }
 }