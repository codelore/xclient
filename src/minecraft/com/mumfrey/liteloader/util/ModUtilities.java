 package com.mumfrey.liteloader.util;
 
 import com.mumfrey.liteloader.core.LiteLoader;
import net.minecraft.client.ClientBrandRetriever;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.network.packet.Packet;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.util.IntHashMap;

import java.util.*;
 
 public abstract class ModUtilities
 {
   private static Set overriddenPackets = new HashSet();
 
   private static boolean forgeModLoader = false;
 
   public static void addRenderer(Class entityClass, Render renderer)
   {
     Map entityRenderMap = (Map)PrivateFields.entityRenderMap.get(RenderManager.instance);
     entityRenderMap.put(entityClass, renderer);
     renderer.setRenderManager(RenderManager.instance);
   }
 
   public static boolean registerPacketOverride(int packetId, Class newPacket)
   {
     if (overriddenPackets.contains(Integer.valueOf(packetId)))
     {
       LiteLoader.getLogger().warning(String.format("Packet with ID %s was already overridden by another mod, one or mods may not function correctly", new Object[] { Integer.valueOf(packetId) }));
     }
 
     try
     {
       IntHashMap packetIdToClassMap = Packet.packetIdToClassMap;
       PrivateFields.StaticFields.packetClassToIdMap.get();
       Map packetClassToIdMap = (Map)PrivateFields.StaticFields.packetClassToIdMap.get();
 
       packetIdToClassMap.removeObject(packetId);
       packetIdToClassMap.addKey(packetId, newPacket);
       packetClassToIdMap.put(newPacket, Integer.valueOf(packetId));
 
       return true;
     }
     catch (Exception ex)
     {
       LiteLoader.logger.warning("Error registering packet override for packet id " + packetId + ": " + ex.getMessage());
     }return false;
   }
 
   public static void sendPluginChannelMessage(String channel, byte[] data)
   {
     if ((channel == null) || (channel.length() > 16)) {
       throw new RuntimeException("Invalid channel name specified");
     }
     try
     {
       Minecraft minecraft = Minecraft.getMinecraft();
 
       if (minecraft.thePlayer != null)
       {
         Packet250CustomPayload payload = new Packet250CustomPayload(channel, data);
         minecraft.thePlayer.sendQueue.addToSendQueue(payload);
       }
     }
     catch (Exception ex)
     {
     }
   }
 
   public static String getObfuscatedFieldName(String fieldName, String obfuscatedFieldName, String seargeFieldName)
   {
     if (forgeModLoader) return seargeFieldName;
     return !Tessellator.instance.getClass().getSimpleName().equals("Tessellator") ? obfuscatedFieldName : fieldName;
   }
 
   public static void registerKey(KeyBinding newBinding)
   {
     Minecraft mc = Minecraft.getMinecraft();
 
     if ((mc == null) || (mc.gameSettings == null)) return;
 
     LinkedList keyBindings = new LinkedList();
     keyBindings.addAll(Arrays.asList(mc.gameSettings.keyBindings));
 
     if (!keyBindings.contains(newBinding))
     {
       keyBindings.add(newBinding);
       mc.gameSettings.keyBindings = ((KeyBinding[])keyBindings.toArray(new KeyBinding[0]));
       mc.gameSettings.loadOptions();
     }
   }
 
   public static void unRegisterKey(KeyBinding removeBinding)
   {
     Minecraft mc = Minecraft.getMinecraft();
 
     if ((mc == null) || (mc.gameSettings == null)) return;
 
     LinkedList keyBindings = new LinkedList();
     keyBindings.addAll(Arrays.asList(mc.gameSettings.keyBindings));
 
     if (keyBindings.contains(removeBinding))
     {
       keyBindings.remove(removeBinding);
       mc.gameSettings.keyBindings = ((KeyBinding[])keyBindings.toArray(new KeyBinding[0]));
     }
   }
 
   static
   {
     forgeModLoader = ClientBrandRetriever.getClientModName().contains("fml");
   }
 }