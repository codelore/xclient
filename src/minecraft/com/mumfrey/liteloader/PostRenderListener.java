package com.mumfrey.liteloader;

public abstract interface PostRenderListener extends LiteMod
{
  public abstract void onPostRenderEntities(float paramFloat);

  public abstract void onPostRender(float paramFloat);
}