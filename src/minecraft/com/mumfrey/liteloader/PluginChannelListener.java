package com.mumfrey.liteloader;

import java.util.List;

public abstract interface PluginChannelListener extends LoginListener
{
  public abstract List getChannels();

  public abstract void onCustomPayload(String paramString, int paramInt, byte[] paramArrayOfByte);
}