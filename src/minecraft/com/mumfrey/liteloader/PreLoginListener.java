package com.mumfrey.liteloader;

import net.minecraft.network.packet.NetHandler;
import net.minecraft.network.packet.Packet1Login;

public abstract interface PreLoginListener extends LiteMod
{
  public abstract boolean onPreLogin(NetHandler paramNetHandler, Packet1Login paramPacket1Login);
}