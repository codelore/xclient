@echo off
title Build script
cls
%~dp0Utils\rm.exe -rf build\classes
mkdir build\classes
echo // Unpack minecraft.jar only with Forge //
%~dp0Utils\unzip.exe build\minecraft.jar -d build\classes

echo // Copy mod files //
xcopy /Y /E %~dp0reobf\minecraft\* %~dp0build\classes\

echo // Copy resource files //
xcopy /E /Y %~dp0res\* build\classes\

echo // Zip to minecraft.jar //
cd build\classes
%~dp0Utils\rm.exe new_minecraft.jar
%~dp0Utils\zip.exe -r -9 ..\new_minecraft.jar .

echo // Delete dir //
%~dp0Utils\rm.exe -rf build\classes

echo // Done. //
pause